#pragma once

#include "config.h"
#include "uGraph.h"

template<class T>
class Operation
{
    public:
        enum OpType{MSG, MSG_T,DIFFUSE,HANDSHAKE,MPLP,FRACTION,NONE};
        int base;
        std::vector<edgeHalf> nbrs;
		T frac;
		OpType type;

        Operation(): base(-1), type(NONE),frac(1)
        {

        }

        Operation(const int B, const int To, 
				  const int E_ID, OpType op):
            	  base(B), type(op),frac(1)
        {
			edgeHalf eH;
			eH.eID=E_ID;
			eH.ndID=To;
			nbrs.push_back(eH);
        }

        Operation(const int B, const int To,
 				  const int E_ID, OpType op, T fraction):
            	  base(B), type(op),frac(fraction)
        {
			edgeHalf eH;
			eH.eID=E_ID;
			eH.ndID=To;
			nbrs.push_back(eH);
        }


    	/*!
    		\brief Copy Assignment Operator
	    */
	    Operation& operator=(const Operation& op)
	    {
 		    base=(op.base);
 		    nbrs=(op.nbrs);
 		    type=(op.type);
     		frac=(op.frac);
	    }

		friend std::ostream& operator<<(std::ostream& os, Operation<T>& op)
		{
			if(op.type==MSG)
			{
				os << "Sending Msg From: " <<  op.base << " To: " <<  op.nbrs[0].ndID << " Via: "  << op.nbrs[0].eID;
			}
			if(op.type==MSG_T)
			{
				os << "Sending MsgT From: " << op.base << " To: " << op.nbrs[0].ndID << " via: " << op.nbrs[0].eID;
			}
			else if(op.type==DIFFUSE)
			{
				os << "Diffusion from: " << op.base <<" to: ";
			    for(int i=0;i<op.nbrs.size();++i)
			    {
					os << "(" << op.nbrs[i].ndID <<" via " << op.nbrs[i].eID << ") ";
			    }
			}
			else if(op.type==HANDSHAKE)
		    {
				os << "Handshake Between " << op.base << " and " << op.nbrs[0].ndID << " via " << op.nbrs[0].eID;
		    }
			else if(op.type==MPLP)
		    {
				os << "MPLP Between " << op.base << " and " << op.nbrs[0].ndID << " via " << op.nbrs[0].eID;
		    }
		    else if(op.type==FRACTION)
		    {
				os << "Fraction between " << op.base << " and " << op.nbrs[0].ndID << " via " << op.nbrs[0].eID << ", frac: " << op.frac ;
		    }
			return os;
		}


        /*!
    		\brief Equality Operator
	    */
	    bool operator==(const Operation& op) const
        {
		    bool is_equal=true;
		    if(base!=op.base)   is_equal=false;
		    if(type!=op.type)   is_equal=false;

		    for(int i=0;i<nbrs.size();++i)
		    {
			    const std::pair<int,int>& pr1=nbrs[i];
			    const std::pair<int,int>& pr2=op.nbrs[i];
			    if((pr1.first!=pr2.first)||(pr1.second!=pr2.second))
			    {
				    is_equal=false;
    			}
		    }
    		return is_equal;
	    }

};