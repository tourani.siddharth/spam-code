#pragma once

/*!
    @brief This class should take as input chains and return as output the reparameterization
           operations that should be performed on the chain in a vector<vector<Operation>> container.
           The first index corresponding to the chain to be reparameterizated, 
           second index the operation to be performed.

           It should take two options, one is a TRWS option.
           The other is a HM option.
           
    @author Siddharth Tourani
    @date 
 */

#include "config.h"
#include "operations.h"
#include "uGraph.h"
#include "chain.h"

namespace Solver
{
    template<class vecType>
    class Chain2Ops
    {
        using vectorizer=typename  vecType::alignedVec;     //	
        using T=typename  vecType::scalar;	                //	
        enum OPT{TRWS,HM};
        public:
            std::shared_ptr<std::vector<chain<vecType>>> chainsPtr;
            std::vector<Operation<T>> operations;
            Chain2Ops(std::vector<Solver::chain<vecType>>& chains, OPT ops=HM);
            
            std::vector<Operation<T>> computeChainOperation(const size_t idx)
            {
                using operationSequence=std::vector<Operation<T>>;

                assert(idx>=0 && idx<chainsPtr->size());
                chain<vecType>& ch=(*chainsPtr)[idx];
                ch.fixChain();

                std::vector<chainEnds> prevLevel;
                std::vector<chainEnds> currLevel;

                operationSequence ops;
                std::vector<std::vector<operationSequence>> operationsHierarchy;

                bool uninitialized=true;
                size_t numEdgesUnshook=ch.edges.size();
                // std::cout << "numEdgesUnshook: " << numEdgesUnshook << "\n";

                bool done=false;
                chainEnds cE;
                cE.m_start=0;
                cE.m_stop=ch.nodes.size()-1;
                currLevel.push_back(cE);

                size_t out=0;
                while(!done)
                {
                    std::vector<chainEnds> nextLevel;
                    std::vector<operationSequence> opsAtCurrLevel;

                    for(size_t e=0;e<currLevel.size();++e)
                    {
                        std::vector<Operation<T>> chainOperations;
                        chainEnds& cE=currLevel[e];
                        chainEnds leftChild;
                        chainEnds rightChild;

                        size_t currStart=cE.m_start;
                        size_t currStop=cE.m_stop;

                        size_t currMidLeft= static_cast<size_t>(floor((static_cast<float>(currStart)+static_cast<float>(currStop))/2));
                        size_t currMidRight=currMidLeft+1;

                        if(currStart==currStop)
                        {
                            continue;
                        }

                        //! first initialization
                        if(prevLevel.size()==0)
                        {
                            //  Push from the left to the centre left
                            for(size_t i=currStart;i<currMidLeft;++i)
                            {
                                assert(ch.edges[i].s==ch.nodes[i].id);
                                assert(ch.edges[i].t==ch.nodes[i+1].id);

                                if(ch.nodes[i].id<ch.nodes[i+1].id)
                                {
                                    Operation<T> op(ch.nodes[i].id,
                                                ch.nodes[i+1].id,
                                                ch.edges[i].id,
                                                Operation<T>::MSG);
                                    chainOperations.push_back(op);
                                }
                                else
                                {
                                    Operation<T> op(ch.nodes[i].id,
                                                ch.nodes[i+1].id,
                                                ch.edges[i].id,
                                                Operation<T>::MSG_T);
                                    chainOperations.push_back(op);
                                }

                                
                            }
 
                            //  Push from the right to the centre right
                            for(int i=currStop;i>currMidRight;--i)
                            {
                                assert(ch.edges[i-1].s==ch.nodes[i-1].id);
                                assert(ch.edges[i-1].t=ch.nodes[i].id);

                                if(ch.nodes[i].id<ch.nodes[i-1].id)
                                {
                                    Operation<T> op(ch.nodes[i].id,
                                                ch.nodes[i-1].id, 
                                                ch.edges[i-1].id,
                                                Operation<T>::MSG);
                                    chainOperations.push_back(op);
                                }
                                else
                                {
                                    Operation<T> op(ch.nodes[i].id,
                                                ch.nodes[i-1].id, 
                                                ch.edges[i-1].id,
                                                Operation<T>::MSG_T);

                                    chainOperations.push_back(op);
                                }
                                
                                    
                                
                            }


                            //  Handshake
                            // Operation<T> op(ch.nodes[currMidLeft].id,
                            //                 ch.nodes[currMidRight].id,
                            //                 ch.edges[currMidLeft].id,
                            //                 Operation<T>::HANDSHAKE);

                            if(ch.nodes[currMidLeft].id<ch.nodes[currMidRight].id)
                            {
                                Operation<T> op(ch.nodes[currMidLeft].id,ch.nodes[currMidRight].id,
                                            ch.edges[currMidLeft].id,Operation<T>::HANDSHAKE);
                                chainOperations.push_back(op);
                            }
                            else
                            {
                                Operation<T> op(ch.nodes[currMidRight].id,ch.nodes[currMidLeft].id,
                                            ch.edges[currMidLeft].id,Operation<T>::HANDSHAKE);
                                chainOperations.push_back(op);
                            }



                            

                            //!	stop the left child
                            leftChild.m_start=currStart;
                            leftChild.m_stop=currMidLeft;

                            //!	stop the right child
                            rightChild.m_start=currMidRight;
                            rightChild.m_stop=currStop;

                            nextLevel.push_back(leftChild);
                            nextLevel.push_back(rightChild);

                            numEdgesUnshook--;
                            if(numEdgesUnshook==0)
                            {
                                done=true;
                                // break;
                            }

                        }
                        else 
                        {
                            size_t prevIdx=static_cast<int>(std::floor(e/2));

                            size_t prevStart=prevLevel[prevIdx].m_start;
                            size_t prevStop=prevLevel[prevIdx].m_stop;

                            //  Left Chain
                            if(prevStart==currStart)
                            {
                                // std::cout << BOLDRED << "Left Chain\n" << RESET;
                                // if(currStop-currMidRight>1)
                                for(size_t i=currStop;i>currMidRight;--i)
                                {
                                    if(ch.nodes[i].id<ch.nodes[i-1].id)
                                    {
                                        Operation<T> op(ch.nodes[i].id,ch.nodes[i-1].id,
                                                        ch.edges[i-1].id,Operation<T>::MSG);
                                        chainOperations.push_back(op);
                                    }
                                    else
                                    {
                                        Operation<T> op(ch.nodes[i].id,ch.nodes[i-1].id,
                                                        ch.edges[i-1].id,Operation<T>::MSG_T);
                                        chainOperations.push_back(op);
                                    }
                                }
                            }

                            //  Right Chain
                            if(prevStop==currStop)
                            {
                                // std::cout << BOLDRED << "Right Chain\n" << RESET;
                                // if(currMidRight-currStart>1)
                                {
                                    for(size_t i=currStart;i<currMidLeft;++i)
                                    {
                                        if(ch.nodes[i].id<ch.nodes[i+1].id)
                                        {
                                            Operation<T> op(ch.nodes[i].id,ch.nodes[i+1].id,
                                                        ch.edges[i].id,Operation<T>::MSG);
                                            chainOperations.push_back(op);
                                        }
                                        else
                                        {
                                            Operation<T> op(ch.nodes[i].id,ch.nodes[i+1].id,
                                                        ch.edges[i].id,Operation<T>::MSG_T);
                                            chainOperations.push_back(op);
                                        }   
                                    }
                                }
                            }

                            if(ch.nodes[currMidLeft].id<ch.nodes[currMidRight].id)
                            {
                                Operation<T> op(ch.nodes[currMidLeft].id,ch.nodes[currMidRight].id,
                                            ch.edges[currMidLeft].id,Operation<T>::HANDSHAKE);
                                chainOperations.push_back(op);
                            }
                            else
                            {
                                Operation<T> op(ch.nodes[currMidRight].id,ch.nodes[currMidLeft].id,
                                            ch.edges[currMidLeft].id,Operation<T>::HANDSHAKE);
                                chainOperations.push_back(op);
                            }

                            //! Stop the left child
                            leftChild.m_start=currStart;
                            leftChild.m_stop=currMidLeft;

                            //! Stop the right child
                            rightChild.m_start=currMidRight;
                            rightChild.m_stop=currStop;

                            nextLevel.push_back(leftChild);
                            nextLevel.push_back(rightChild);

                            numEdgesUnshook--;
                            if(numEdgesUnshook==0)
                            {
                                done=true;
                            }
                        }
                        
                        opsAtCurrLevel.push_back(chainOperations);
                    }

                    operationsHierarchy.push_back(opsAtCurrLevel);
                    out++;

                    prevLevel=currLevel;
                    currLevel=nextLevel;
                }

                operationSequence operations;
                for(size_t i=0;i<operationsHierarchy.size();++i)
                {
                    for(size_t j=0;j<operationsHierarchy[i].size();++j)
                    {
                        for(size_t k=0;k<operationsHierarchy[i][j].size();++k)
                        {
                            // std::cout << operationsHierarchy[i][j][k] << "\n";
                            operations.push_back(operationsHierarchy[i][j][k]);
                        }
                    }
                }
                return operations;


            }

            std::vector<std::vector<Operation<T>>> computeOperationsForAllChains()
            {
                std::vector<std::vector<Operation<T>>> chainOps;
                for(size_t i=0;i<chainsPtr->size();++i)
                {
                    std::vector<Operation<T>> chOps=computeChainOperation(i);
                    chainOps.push_back(chOps);
                }
                return chainOps;
            }

            std::vector<std::vector<Operation<T>>> flattenOperationsHierarchy()
            {

            }

            void computeOperations()
            {
                if(opts==TRWS)
                {

                }
                else if(opts==HM)
                {
                    size_t i=0;
                    // for(size_t i=0;i<chainsPtr->size();++i)
                    {
                        std::vector<Operation<T>> ChOps=computeChainOperation(i);
                    }   
                    // operations.push_back(ChOps);
                }
                else
                {
                    throw std::runtime_error("Not sure what ...");
                }
            }

            OPT opts;
    };


    template<class vecType>
    Chain2Ops<vecType>::Chain2Ops(std::vector<chain<vecType>>& chains, OPT ops): opts(ops)
    {
        chainsPtr=std::make_shared< std::vector<chain<vecType>> >(chains);
    }

};