#pragma once

#include "config.h"
#include "uGraph.h"
#include "testGraphs2.h"


namespace IO
{

// template<typename T>
void readFile(const std::string& fname,
                Graph::graphStorage& gS)
{
    // std::cout << BOLDRED << "Read File\n" << RESET;
    // Graph::graphStorage<T,Graph::uGraph> gS;
    
    std::ifstream fid(fname);
    if(fid.is_open())
    {
        // std::cout << "in here\n";
        int nV, nE, nF;
        fid >> nV >> nF;
        nE=nF-nV;
        // std::cout << "nV: " << nV << " nE: " << nE << " nF: " << nF << "\n";
        gS.G.m_nV=nV;   gS.G.m_nE=nE;
        gS.G.m_E.resize(nE);
        // gS.G.m_nbrs.resize(nV);
        gS.unaries.resize(nV);
        gS.pairwise.resize(nE);
        gS.pwSz.resize(nE);

        // std::cout << "reached here\n";
        // std::cout << gS.unaries.size() << "\n";

        int blockID;

        int unID, l1;
        int from, to, l2;
        int edgeID=0;
        int nodeID=0;

        for(size_t f=0;f<nF;++f)
        {
            // std::cout << "f: " << f << "\n";
            fid >> blockID;

            // std::cout << nodeID << " " << edgeID << " "<< blockID << "\n";
            if(blockID==1)
            {
                fid >> unID;
                fid >> l1;
                // std::cout << "unID: " << unID << " l1: " << l1 << "\n";
                gS.unaries[unID].resize(l1);
                for(size_t j=0;j<l1;++j)
                {
                    fid >> gS.unaries[unID][j];
                }
            }
            else if(blockID==2)
            {
                fid >> from >> to;
                fid >> l1 >>  l2;
                // std::cout << "s: " << from << " t: " << to << "\n";
                // std::cout << "l1: " << l1 << " l2: " << l2 << "\n";
                gS.G.m_E[edgeID].s=from;
                gS.G.m_E[edgeID].t=to;
                // std::cout << "HERE G.m_E\n";
                gS.pwSz[edgeID].l1=l1;
                gS.pwSz[edgeID].l2=l2;
                // std::cout << "HERE pwSz\n";
                gS.pairwise[edgeID].resize(l1*l2);
                // std::cout << "HERE pairwise\n";
                double val;
                for(size_t j=0;j<l1;++j)
                {
                    for(size_t k=0;k<l2;++k)
                    {
                        fid >> gS.pairwise[edgeID][j*l2+k];
                    }
                }
                edgeID++;
                // getchar();
            }
            
        }

        //  fill up the adjacency list
        // for(size_t e=0;e<gS.G.m_E.size();++e)
        // {
        //     int from=gS.G.m_E[e].s;
        //     int to=gS.G.m_E[e].t;
        //     edgeHalf eH1;
        //     eH1.eID=e;  eH1.ndID=to;
        //     edgeHalf eH2;
        //     eH2.eID=e;  eH2.ndID=from;
        //     gS.G.m_nbrs[from].push_back(eH1);
        //     gS.G.m_nbrs[to].push_back(eH2);
        // }
    }
    else
    {
        throw std::runtime_error("unable to read file.");
    }
    // std::cout << BOLDRED << "Done\n" << RESET;
}


};