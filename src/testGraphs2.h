#pragma once
/*!
	@file testGraphs.h
	@brief Creates test graphs for debugging purposes. 
	       Mainly gridGraphs, denseGraphs, randomGraphs.
	@author Siddharth Tourani
*/

#include "uGraph.h"
#include "config.h"

namespace Graph
{
	class StorageBase
	{
	public:
		std::vector<std::vector<float>> unaries;
		std::vector<std::vector<float>> pairwise;
		std::vector<pwSize> pwSz;
		// graphType G;
	};

	//	Maybe no template specialization is required here. Only a class
	class graphStorage: public StorageBase
	{
		public:
		uGraph G;
		void printGraphType()
		{
		
		}

		friend std::ostream& operator<<(std::ostream& os, graphStorage& gT)
		{
			os << "nV: " << gT.unaries.size() << " nE: " << gT.pairwise.size() << "\n";
			// for(auto& nd: gT.unaries)
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				os << "UNARY[" << i << "]: ";
				for(size_t k=0;k<gT.unaries[i].size();++k)
				{
					os << gT.unaries[i][k] << " ";
				}
				os << "\n";
			}
			
			for(size_t e=0;e<gT.G.m_E.size();++e)
			{
				std::cout << gT.G.m_E[e].s << " ----- " << gT.G.m_E[e].t << "\n";
				size_t k1=gT.pwSz[e].l1;
				size_t k2=gT.pwSz[e].l2;
				for(size_t k=0;k<k1;++k)
				{
					for(size_t l=0;l<k2;++l)
					{
						os << gT.pairwise[e][k*k2+l] << " ";
					}
					os << "\n";
				}
				os << "\n";
			}


			return os;
		}
	};

	void allocateMemory(const size_t R, 
						const size_t C, 
						const size_t K,
						const bool labelsFixed,
						graphStorage& gT)
	{
		gT.unaries.resize(R*C);
		gT.pairwise.resize(R*(C-1)+C*(R-1));
		gT.pwSz.resize(R*(C-1)+C*(R-1));
		if(labelsFixed)
		{
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				gT.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				gT.unaries[i].resize(L);
			}
		}
	}

	void allocateMemory(const size_t N,
						const size_t K,
						const bool labelsFixed,
						graphStorage& gT)
	{
		size_t nE=(N*(N-1))/2;
		gT.unaries.resize(N);
		gT.pairwise.resize(nE);
		gT.pwSz.resize(nE);
		if(labelsFixed)
		{
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				gT.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				gT.unaries[i].resize(L);
			}
		}
	}


	void fillUpUnaries(std::vector<std::vector<float>>& unaries)
	{
		for(size_t i=0;i<unaries.size();++i)
		{
			for(size_t j=0;j<unaries[i].size();++j)
			{
				unaries[i][j]=rand()%10;		//	add the unary potentials
			}
		}
	}



	void createGridGraph(const size_t R, 
                         const size_t C,
						 const size_t K, 
                         const bool labelsFixed,
						 graphStorage& gS)
	{
		std::cout << "Create a uGraph\n";
		std::vector<edgeEnds> graphEdges(R*(C-1)+C*(R-1));
		allocateMemory(R,C,K,labelsFixed,gS);
		fillUpUnaries(gS.unaries);

		size_t pwID=0;
		//	rows
		for(size_t r=0;r<R;++r)
		{
			for(size_t c=0;c<C-1;++c)
			{
				size_t ind1=r*C+c;						//	
				size_t ind2=r*C+c+1;					//	
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges

				size_t l1=gS.unaries[ind1].size();		//	
				size_t l2=gS.unaries[ind2].size();		//	
				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		//	cols
		for(size_t c=0;c<C;++c)
		{
			for(size_t r=0;r<R-1;++r)	
			{
				size_t ind1=r*C+c;
				size_t ind2=(r+1)*C+c;
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges
				size_t l1=gS.unaries[ind1].size();
				size_t l2=gS.unaries[ind2].size();
				gS.pairwise[pwID].resize(l1*l2);
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;
					}
				}
				pwID++;
			}
		}
		
		gS.G.init(R*C,graphEdges);
	}

	void createCompleteGraph(const size_t N, 
                             const size_t K, 
							 const bool labelsFixed,
						     graphStorage& gS)
	{
		std::cout << "Create a uGraph\n";
		size_t nE=(N*(N-1))/2;
		std::vector<edgeEnds> graphEdges(nE);
		allocateMemory(N,K,labelsFixed,gS);
		fillUpUnaries(gS.unaries);

		size_t pwID=0;
		for(size_t i=0;i<N;++i)
		{
			for(size_t j=i+1;j<N;++j)
			{
				graphEdges[pwID].s=i;				//	
				graphEdges[pwID].t=j;				//	add the graph edges
				size_t l1=gS.unaries[i].size();		//	
				size_t l2=gS.unaries[j].size();		//	
				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		gS.G.init(N,graphEdges);
	}
    

};