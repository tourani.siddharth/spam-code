#pragma once

#include <filesystem>
#include <iostream>
#include <cstdio>
#include <vector>
#include <memory>
#include <utility>
#include <algorithm>
#include <stack>
#include <queue>
#include <sstream>
#include <random>
#include <map>
#include <cassert>

#include <fstream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <typeinfo>
#include <xmmintrin.h> //SSE
#include <emmintrin.h> //SSE2


#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#define EPSILON 1e-6

enum graphType{GRID,COMPLETE,UNSTRUCTURED};
enum repType{DIR,UN,HSH};
enum minorantType{UNIFORM,HIERARCHICAL};

namespace Solver
{
enum sampType{SPANNING_TREES, 
                ARC_INCONSISTENT, 
					 MAX_MONO_CHAINS, EDGE,
                     INDUCED_CHAINS, ROW_COL, ROW_EDGE, SHORTEST_PATH};

enum GraphType{GRID,COMPLETE};
enum RoundingType{FUSIONMOVES,NORMAL,GREEDYBEST};

};


void printSampling(const Solver::sampType st);

// using bGraph=boost::adjacency_list<boost::vecS, boost::vecS, 
// 							boost::undirectedS, boost::no_property,
//                             boost::property<boost::edge_weight_t, int> >;

// using bEdge=boost::graph_traits<bGraph>::edge_descriptor;

// using bVertex=boost::graph_traits<bGraph>::vertex_descriptor;

constexpr size_t uintInf=std::numeric_limits<size_t>::max();
constexpr size_t sizetInf=std::numeric_limits<size_t>::max();
constexpr int intInf=std::numeric_limits<int>::max();

template<typename T>
using dVec=std::vector<std::vector<T>>;	//	double vector

struct file_parts
{
    std::string path;
    std::string name;
    std::string ext;
};

file_parts ExtractFileParts(std::string filename);

class subchainElement
{
    public:
    size_t locationInNode;
    size_t chainID;
};  

class dirEdgeHalf
{
    public:
        size_t dir;
        size_t eID;
        size_t ndID;

    dirEdgeHalf(): eID(std::numeric_limits<size_t>::max()),ndID(std::numeric_limits<size_t>::max())
    {};

    dirEdgeHalf(const size_t Dir, const size_t EID, const size_t NDID):dir(Dir) , eID(EID), ndID(NDID)
    {};
};

class edgeHalf
{
    public:
        size_t eID;
        size_t ndID;

    edgeHalf():eID(std::numeric_limits<size_t>::max()),
                ndID(std::numeric_limits<size_t>::max())
    {};

    edgeHalf(const size_t eid, 
            const size_t ndid):
            eID(eid),ndID(ndid)
    {}


    edgeHalf(const edgeHalf& eH)
    {
        eID=eH.eID;
        ndID=eH.ndID;
    }

    edgeHalf& operator=(const edgeHalf& eH)
    {
        this->eID=eH.eID;
        this->ndID=eH.ndID;
        return *this;
    }


    friend std::ostream& operator <<(std::ostream& os, edgeHalf& eH)
    {
        std::cout << "edgeHalf eID: " << eH.eID << " ndID: " << eH.ndID << "\n";
    }
};

struct chainEnds
{
    public:
    size_t m_start;
    size_t m_stop;

    friend std::ostream& operator <<(std::ostream& os, chainEnds& cE)
    {
        std::cout << "chainEnd start: " << cE.m_start << " stop: " << cE.m_stop << "\n";
    }
};

/*  We use int instead of size_t.   */
class edgeEnds
{
    public:
    size_t s;  
    size_t t;

    edgeEnds(){};

    edgeEnds(const size_t S, 
             const size_t T): 
             s(S),t(T)
    {
    }

    edgeEnds(const edgeEnds& eE)
    {
        s=eE.s;
        t=eE.t;
    }

    edgeEnds& operator=(const edgeEnds& eE)
    {
        this->s=eE.s;
        this->t=eE.t;
        return *this;
    }


    friend std::ostream& operator <<(std::ostream& os, edgeEnds& eE)
    {
        std::cout << "edgeEnd s: " << eE.s << " t: " << eE.t << "\n";
    }
};

struct pwSize
{
    public:
    size_t l1;
    size_t l2;

    friend std::ostream& operator <<(std::ostream& os, pwSize& pS)
    {
        std::cout << "pwSize l1: " << pS.l1 << " l2: " << pS.l2 << "\n";
    }
};

using adjacencyVector=std::vector<edgeHalf>;
using adjacencyList=std::vector<adjacencyVector>;

using dirAdjVec=std::vector<dirEdgeHalf>;
using dirAdjList=std::vector<dirAdjVec>;

template<typename T>
void printVectorBasic(std::vector<T>& vec)
{
    for(auto i:vec)
    {
        std::cout << i << " ";
    }
    std::cout << "\n";
}


template< typename T >
bool is_aligned(T* p)
{
    return !(reinterpret_cast<uintptr_t>(p) % std::alignment_of<T>::value);
}

template<typename T>
void printPtrAddress(T* ip)
{
    printf("Address of x is %p\n", (void *)ip);  
}


using intPair=std::pair<int,int>;
typedef std::pair<int,int> edgePair;
typedef std::vector<edgePair> edgeList;
typedef std::map<int,edgeList> adjList;
typedef std::map<int,edgePair> edgeMatrix;
typedef std::map<size_t,std::vector<size_t>> mapList;    //  MAP list 

/*
    //	TODO: Move to utility.
    //  \brief Check if the map list is empty.
    // bool is_map_list_empty(map_list& ml)
    // {
    //     map_list::iterator it;
    // 	bool flag=true;

    //     if(ml.size()==0)
    //     {
    //         return true;
    //     }

    //     for(it=ml.begin();it!=ml.end();++it)
    //     {
    //         if((it->second).size()!=0)  //  there is still an element left
    //         {
    //             flag=false;
    //         }
    //     }
    //     return flag;
    // }
*/