#pragma once

/*!
	@file uGraph.h
	@author Siddharth Tourani
	@brief hashed Edges vs mGraph edges.
*/

#include "config.h"
#include <vector>
#include "edgeHash.h"

namespace Graph
{

	class GraphBase
	{
		public:
		using edgeVector=std::vector<edgeEnds>;

		size_t m_nV;
		size_t m_nE;
		edgeVector m_E;

		/*! Graph Constructor */
		GraphBase(): m_nV(0),m_nE(0){};

		/*!	nV(): number of vertices	*/
		size_t nV() const {return m_nV;};

		/*!	nE(): number of edges 		*/
		size_t nE() const {return m_nE;};

		/*!	get the getEdgeEnds			*/
		edgeEnds& getEdgeEnds(const size_t s) {return m_E[s];};

		/*! initialize the graph.	*/
		virtual void init(const size_t nV, const std::vector<edgeEnds>& E){};

		/*! get the number of neighbors. */
		virtual size_t nNbrs(const size_t s) const {};

		/*!	edge: returns true if edge s-t exists.	*/
		virtual size_t getEdgeID(const size_t s, const size_t t){};

		/*!	Print the list of edges in the graph.	*/
		virtual void print()
		{
			for(int i=0;i<m_E.size();++i)	std::cout << "("<< m_E[i].s << "," << m_E[i].t << ")\n";
		}
	};

/*!
	Maybe have another class called undirected graph.
	And another called directed graph.
*/

	class uGraph: public Graph::GraphBase
	{
		public:
		
		adjacencyList m_nbrs;

		uGraph():GraphBase(){};

		uGraph(const uGraph& gr)
		{
			m_nV=gr.m_nV;
			m_nE=gr.m_nE;
			m_E=gr.m_E;
			init(m_nV,m_E);
		}

		uGraph& operator=(const uGraph& gr)
		{
			if(this==&gr)
			{
				return * this;
			}
			m_nV=gr.m_nV;
			m_nE=gr.m_nE;
			m_E=gr.m_E;
			init(m_nV,m_E);
			return *this;
		}

		/*!	init(): Initialize the graph. Computes m_E.	*/
		void init(const size_t nV, const std::vector<edgeEnds>& E);

		/*! Return number of neighbors for node s	*/
		size_t nNbrs(const size_t s) const {return m_nbrs[s].size();};
 
		/*!	edge: returns true if edge s-t exists.	*/
		size_t getEdgeID(const size_t s, const size_t t);


		/*! get the edges neighboring ndID	*/ 
		adjacencyVector& getNbrsToNode(const size_t ndID);

		/*! Remove the node with node id ndID.	*/ 
		void removeNode(const size_t ndID);

		//!	Remove the edge with edge id eID.
		void removeEdge(const size_t eID);	



		friend std::ostream& operator<<(std::ostream& os, uGraph& uG)
		{
			for(int i=0;i<uG.m_E.size();++i)
			{
				os << "(" << uG.m_E[i].s << " " << uG.m_E[i].t << ")\n";
			}
			return os;
		}

	};

	
};