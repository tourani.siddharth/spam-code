#pragma once

/*!
	@file spam.h
	@author Siddharth Tourani
	@brief The ICHM class is derived from the msgAlgo class.
*/

#include "msgAlgo.h"
#include "alignedAllocator.h"
// #include "solverOptions.h"
#include "testGraphs.h"
#include "simd.h"
#include "graphSampling.h"
#include "chain2Ops.h"
#include "shortestPathSampler.h"

namespace Solver
{
    
		// // using msgAlgo<vecType,graphType> mAlgo;
		// typedef typename Solver::msgAlgo<vecType,graphType> parent;
		// typedef typename  vecType::alignedVec vectorizer;	//	
		// typedef typename  vecType::scalar	   T;	//	

        template<typename vecType>
        struct nodeCore;

        template<typename vecType>
        struct edgeCore;

        // template<typename vecType>
        // struct chainNodeCore;

        // template<typename vecType>
        // struct chainEdgeCore;


		/*!	nodeCore */
        template<class vecType>
        struct nodeCore
        {
            using vectorizer=typename  vecType::alignedVec;     //	
            using T=typename  vecType::scalar;	                //	

        	vectorizer* data;	//	size of the unary
            int K;
        	int vK;				//	size in vectorizers
            // int nNbrs;
        	// edgeCore<vecType>* nbrEdges;	//	Should these be here? ? ?
        	// nodeCore<vecType>* n;			//	pointer to the node
        };


        /*! edgeCore */
        template<class vecType>
        struct edgeCore
        {
            using vectorizer=typename  vecType::alignedVec;     //	
            using T=typename  vecType::scalar;	                //	

            int s;
            int t;

            int K1;         //  num rows in pw term
            int K2;         //  num cols in pw term 
            int vK2;        //  vectorized num cols
            vectorizer* data;   //  where the pw term begins
        };

        /*!  */
        template<class vecType>
        struct chainNodeCore
        {
            using vectorizer=typename  vecType::alignedVec;     //	
            using T=typename  vecType::scalar;	                //	

            int id;
            int K;
            int vK;
            vectorizer* data;
        };

        /*! */
        template<class vecType>
        struct chainEdgeCore
        {
            using vectorizer=typename  vecType::alignedVec;     //	
            using T=typename  vecType::scalar;	                //	

            int id;
            int s;
            int t;
            int K1;
            int K2;
            int vK2;
            vectorizer* data;
        };

        template<class vecType>
        struct subgraphChain
        {
            std::vector<chainNodeCore<vecType>> nodes;
            std::vector<chainEdgeCore<vecType>> edges;
        };

		/*! nodeInfo */
        // template<class vecType>
        // struct nodeInfo
        // {
        //     typedef typename  vecType::alignedVec vectorizer;	            //	
        //     typedef typename  vecType::scalar	   T;	                    //	

        //     int K;
        //     nodeCore* core;
        //     std::vector<edgeInfo*> nbrs;
        // };

		/*! edgeInfo */
        // template<class vecType>
        // struct edgeInfo
        // {
        //     typedef typename  vecType::alignedVec vectorizer;	           //	
        //     typedef typename  vecType::scalar	   T;	                   //	

        // 	edgeCore<vecType>* core;
        // 	nodeInfo<vecType>* head;
        // 	nodeInfo<vecType>* tail;
        // 	edgeInfo<vecType>(): core(0),head(0),tail(0){};
        // 	//	get the data out
        // };

        // template<class vecType>
        // struct chainNodeCore
        // {
        //     typedef typename vecType::alignedVec vectorizer;
        //     typedef typename vecType::scalar T;

        //     vectorizer* data;
        //     int vK;
            
        //     chainEdgeCore<vecType>* inEdge;
        //     chainNodeCore<vecType>* inNode;

        //     chainEdgeCore<vecType>* outEdge;
        //     chainNodeCore<vecType>* outNode;

        //     //  do i need any connection to 
        //     chainNodeCore(): data(0), vK(0){};
        // };

        // template<class vecType>
        // struct chainEdgeCore
        // {
        //     typedef typename vecType::alignedVec vectorizer;
        //     typedef typename vecType::scalar T;

        //     vectorizer* data;
        //     nodeCore<vecType>* nHead;
        //     nodeCore<vecType>* nTail;
        //     //	Let K1 and K2 be the lengths of the pairwise term.
        //     int vK1;		//	vectorized K1
        //     int vK2;		//	vectorized K2
        //     int K1;			//	
        //     int K2;			//

        //     edgeCore(): data(0),nHead(0),nTail(0){};
        //     const nodeCore* tail() const
        //     {
        //         return nTail;
        //     }

        //     const nodeCore* head() const
        //     {
        //         return nHead;
        //     }




        //     chainEdgeCore<vecType>* inEdge;
        //     chainNodeCore<vecType>* inNode;

        //     chainEdgeCore<vecType>* outEdge;
        //     chainNodeCore<vecType>* outNode;

        //     //  do i need any connection to 

        //     chainNodeCore(): data(0), vK(0){};
        // };
};