#pragma once

#include "config.h"
#include "tree.h"
#include "chain.h"
#include "utils.h"

namespace Solver
{
	template<class graphType>
	class spanningTreeSampler
	{
	public:
		graphType* gr;

		std::vector<tree> m_trees;

		spanningTreeSampler(graphType& gr);
		void sample();
	};

	template<class graphType>
	spanningTreeSampler<graphType>::spanningTreeSampler(graphType& g)
	{
		gr=&g;
	}

	template<class graphType>
	void spanningTreeSampler<graphType>::sample()
	{
		std::cout << BOLDRED << "inside sample\n" << RESET;

		std::vector<std::pair<size_t,size_t>> edgeVec(gr->m_nE);
		for(size_t e=0;e<gr->m_nE;++e)
		{
			edgeVec[e]=std::pair<size_t,size_t>(gr->m_E[e].s,gr->m_E[e].t);
		}

		adjacencyList adjMap=computeAdjMap<graphType>(*gr);
					
		std::vector<tree> trees;
		std::vector<bool> edgesDone(gr->m_nE,false);
		std::vector<int> edgeWeights(gr->m_nE,1);
		std::cout << "Starting while loop\n";
		bool graphCovered=false;
		int treeID=0;	//	index on the tree.
		std::vector<size_t> numTimesEdgeSampled(gr->m_E.size(),0);

		while(!graphCovered)
		{
			/*!
				Set up the graph with inputs as the edge vecs, 
				the weights and the num of vertices.
			*/
			bGraph g(edgeVec.begin(),edgeVec.begin()+gr->m_nE,
				edgeWeights.begin(),gr->m_nV);

			//!	The tree is represented by a vector of edge pairs.
			boost::property_map<bGraph,boost::edge_weight_t>::type 
			weight_map=get(boost::edge_weight,g);

			//!	The tree is represented by a vector of edge pairs.
			std::vector<bEdge> spanTr;
			
			//!	Compute the min weight spanning tree of graph.
			boost::kruskal_minimum_spanning_tree(g,std::back_inserter(spanTr));
			
			std::vector<size_t> edgeInds;
			std::vector<edgeEnds> treeEdges;                 //  tree_edges
			std::vector<bEdge>::const_iterator eIt;

			for(eIt=spanTr.begin();eIt!=spanTr.end();++eIt)
			{
				const size_t from=source(*eIt,g);
				const size_t to=target(*eIt,g);

				edgeEnds eE(from,to);
				treeEdges.push_back(eE);

				size_t eID=findInAdjMap(adjMap,from,to);
				// std::cout << "eID: " << eID << "\n";

				edgesDone[eID]=true;
				edgeInds.push_back(eID);

				edgeWeights[eID]++;
				numTimesEdgeSampled[eID]++;
			}

			tree spanTree(treeEdges,
						  edgeInds,
						  gr->m_nV,
						  gr->m_nV,
						  gr->m_nE);

			trees.push_back(spanTree);
			treeID++;
			size_t edgeCounter=0;
			for(size_t i=0;i<edgesDone.size();++i)
			{
				if(edgesDone[i]) edgeCounter++;
			}

			if(edgeCounter==gr->m_nE)
			{
				graphCovered=true;
			}
		}
			std::cout << "# of trees: " << treeID << "\n";
			std::cout << BOLDRED << "Done computing spanning trees computer\n" << RESET;
	}

	

};