#pragma once

#include "config.h"
#include "uGraph.h"


template<class graphType>
adjacencyList computeAdjMap(graphType& gr)//	
{
	adjacencyList aL(gr.m_nV);				//	

	std::cout << "aL size: " << aL.size() << "\n";
	size_t e=0;
	for(size_t e=0;e<gr.m_E.size();++e)	//	
	{
		const size_t fr=gr.m_E[e].s;		//	
		const size_t to=gr.m_E[e].t;		//	
		const size_t eID=e;					//	
		edgeHalf e1(eID,to);				//	
		edgeHalf e2(eID,fr);				//
		adjacencyVector& fr_vec=aL[fr];		//	
		aL[fr].push_back(e1);				//	
		aL[to].push_back(e2);				//	
	}
	return aL;
}

size_t findInAdjMap(adjacencyList& adjList,
            const size_t from,
            const size_t to)
{
    size_t eID=std::numeric_limits<size_t>::infinity();

    for(size_t i=0;i<adjList[from].size();++i)
    {
        if(adjList[from][i].ndID==to)
        {
            eID=adjList[from][i].eID;
            return eID;
        }
    }
    throw std::runtime_error("Unable to find element in adjacency list");
}


void line()
{
	std::cout << BOLDRED << "--------------------------------------------------------------------\n" << RESET;
}

/*
    Try to determine if the graph is a grid graph 
    or not.
*/
bool isGridGraph(const Graph::uGraph& gr)
{
    int nV=gr.m_nV;
    int nR, nC;
    bool isGG=true;
    if(gr.m_nbrs[0].size()==2)
    {
        int nbr1=gr.m_nbrs[0][0].ndID;
        int nbr2=gr.m_nbrs[0][1].ndID;

        if(nbr1==1)
        {
            nC=nbr2;    nR=nV/nC;
        }
        else
        {
            nC=nbr1;    nR=nV/nC;
        }

        if(nR*nC==nV)
        {
            isGG=true;
        }
    }
    else
    {
        isGG=false;
    }
    return isGG;
}

/*
    Try to determine if the graph is a grid graph 
    or not.
*/
bool isCompleteGraph(const Graph::uGraph& gr)
{
	bool isComplete=true;
	int nV=gr.m_nV;
	int nE=gr.m_nE;
	std::map<edgePair, bool> allPossibleEdges;
	for(size_t i=0;i<nV;++i)
	{
		for(size_t j=i+1;j<nV;++j)
		{
			edgePair eP(i,j);
			
		}
	}
}

/*!

*/
void printPQ(std::priority_queue<intPair,
                    std::vector<intPair>,
                    std::greater<intPair>> pq)
{
    std::cout << BOLDRED << "Priority Queue: " << RESET;
    while(!pq.empty())
    {
        intPair iP=pq.top();
        pq.pop();
        std::cout << iP.second << " ";
    }
    std::cout << "\n";
}


template<typename T>
 void printArrayInGrid(const std::vector<T>& vec, 
                        const size_t R, const size_t C)
 {
        for(size_t r=0;r<R;++r)
     {
         for(size_t c=0;c<C;++c)
         {
             std::cout << vec[r*C+c] << " ";
         }
         std::cout << "\n";
     }
     std::cout << "\n";
 }

template<typename T>
T generateRandomIntFromUniformDistribution(T intervalStart,T intervalStop, T seed=1235352)
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(intervalStart,intervalStop-1); // distribution in range [1, 6]
    return dist6(rng);
}
