#pragma once

/*!
	@file msgAlgo.h
	@author Siddharth Tourani
	@brief The following generic functionality is required 
			by all the algorithms
		compute the dual.
		compute the primal.
*/

// #include "hGraph.h"
	#include "uGraph.h"
// #include "dGraph.h"
#include "config.h"

namespace Solver
{
	template<class vecType, class graphType>
	class msgAlgo
	{
		public:
		using alignedVec=typename  vecType::alignedVec;	//	
		using scalar=typename  vecType::scalar;	//			

		public:										//	
		
			graphType* mG;

		public:										//	

		/*! 	*/
		msgAlgo() {};
		msgAlgo(graphType& g) {mG=&g;};

		virtual void testFunction()
		{
			std::cout << "msgAlgo Class\n";
		}
	};
	
};