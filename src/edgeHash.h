#pragma once
#include "config.h"

size_t hashEdge(const size_t s, const size_t t, const size_t nE);
edgeEnds unhashEdge(const size_t hashKey, const size_t nE);
