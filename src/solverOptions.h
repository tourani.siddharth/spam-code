#pragma once
#include "config.h"
// #include "msgAlgo.h"

namespace Solver
{
	/*!
		Possible alternative options: 
			Logfile for debugging.
			Algo Type. TRWS vs Minorant vs TBCA
	*/
	/*!	There may be other options, but for the moment, this is enough.	*/
	class algoOptions
	{
		public:
		size_t maxIters;
		sampType sampling;
		graphType grType;
		double tolerance;

		algoOptions(const size_t mI=200, const sampType st=SHORTEST_PATH, 
					const double tol=1e-6,const graphType grT=UNSTRUCTURED):
					maxIters(mI), sampling(st),
					grType(grT),  tolerance(tol)
		{}

		/*! 		*/
		algoOptions(const algoOptions& opts)
		{
			maxIters=opts.maxIters;		//	
			sampling=opts.sampling;		//	
			grType=opts.grType;			//	
			tolerance=opts.tolerance;	//	
		}

		algoOptions(algoOptions&& opts)
		{
			maxIters=opts.maxIters;		//	
			sampling=opts.sampling;		//	
			grType=opts.grType;			//	
			tolerance=opts.tolerance;	//	
		}

		algoOptions& operator=(const algoOptions& opts)
		{
			maxIters=opts.maxIters;
			sampling=opts.sampling;
			grType=opts.grType;
			tolerance=opts.tolerance;
			return *this;
		}

		algoOptions& operator=(algoOptions&& opts)
		{
			this->maxIters=opts.maxIters;
			this->sampling=opts.sampling;
			this->grType=opts.grType;
			this->tolerance=opts.tolerance;
			return *this;
		}

		bool checkGraphAndSamplingCompatibility()
		{
			bool isCompatible=true;
			if(grType!=GRID && (sampling==(ROW_COL||ROW_EDGE)))
			{
				isCompatible=false;
				return isCompatible;
			}
			return isCompatible;
		}

		friend std::ostream& operator<<(std::ostream& os, const algoOptions& aO);
	};

	std::ostream& operator<<(std::ostream& os, const algoOptions& aO)
	{
		os << "----------- Program Options ------------\n";
	    os << "Algorithm Options: " << "maxIters: " << aO.maxIters << "\n";
	    os << "Dual tolerance: " << aO.tolerance << "\n";
	    os << "graph type: ";

	    switch(aO.grType)
	    {
	    	case UNSTRUCTURED:
	    		os << "unstructured ";
	    		break;
	    	case GRID:
	    		os << "grid ";
	    		break;
	    	case COMPLETE:
	    		os << "complete ";
	    		break;
	    	default:
	    		os << "unknown";
	    		break;
	    };
	    os << "\n";

	    os << "sampling type: ";
	    switch(aO.sampling)
	    {
	    	case SPANNING_TREES:
	    		os << "spanning trees ";
	    		break;
	    	case ARC_INCONSISTENT:
	    		os << "arc-inconsistent spanning trees ";
	    		break;
	    	case INDUCED_CHAINS:
	    		os << "induced chains";
	    		break;
	    	case MAX_MONO_CHAINS:
	    		os << "maximal monotonic chains ";
	    		break;
	    	case ROW_COL:
	    		os << "row-column chains";
	    		break;
	    	case ROW_EDGE:
	    		os << "row-edge chains ";
	    		break;
	    	default:
	    		os << "unknown ";
	    		break;
	    };
	    os <<"\n";
	    os << "----------------------------------------\n";
	    return os;
	}


	class progOptions
	{
		public:
			algoOptions algoOpts;
			std::string fName;

		friend std::ostream& operator<<(std::ostream& os, const progOptions& pO);
	};
	
	std::ostream& operator<<(std::ostream& os, const progOptions& pO)
	{
		std::cout << "filename: ";
		if(!pO.fName.empty())
		{
			 std::cout << pO.fName << "\n";	
		}
		else
		{
			std::cout << "N.A.\n";
		}
		std::cout << pO.algoOpts;
	}

	progOptions parseCommandLineOptions(int argc, char** argv)
	{
		progOptions opts;		
		try 
		{ 
			size_t numIters;
			sampType st;
			std::string filename;
			double tol;
	    	/** Define and parse the program options */ 
    		namespace po = boost::program_options; 
    		po::options_description desc("Options"); 
    		desc.add_options() 
			("help,h", "Print help messages")
			("iters,i",po::value<size_t>(&numIters),"Maximum iterations")
			// ("sampling,s",po::value<samp_type>(&st),"Graph Sampling Strategy")
			("tolerance,t",po::value<double>(&tol),"Dual Tolerance")
			("file,f",po::value<std::string>(&filename)->required(),"Filename");

    		po::variables_map vm; 
    		try 
    		{
    			/*!	
    				Parse the command line and store the parameters in the variable map.
					Both commands are used together. 
    			*/ 
    			po::store(po::parse_command_line(argc, argv, desc),  	vm); // can throw 

    			//	options now has the filename
      			if(vm.count("file"))	
      			{
      				opts.fName=filename;
      			}
      			else
      			{
      				throw std::runtime_error("No graph file given to solve\n");
      			}

    			//	print out the description of the options
      			if(vm.count("help"))			
      			{
      				std::cout << "The program runs the Induced Chain Hierarchical Minorant (ICHM) Algorithm\n";
	      			std::cout << desc << "\n";	//		
      			}
      		
      			//	set the iters in algoOptions
      			if(vm.count("iters"))
      			{
	      			opts.algoOpts.maxIters=numIters;
      			}

      			// //	sets the sampling in algoOptions
      			// if(vm.count("sampling"))
      			// {
	      		// 	opts.algoOpts.sampling=st;
      			// }

      			if(vm.count("tolerance"))
      			{
      				opts.algoOpts.tolerance=tol;
      			}

      			//	This is responsible for throwing the error
      			po::notify(vm);		// throws on error, so do after help in case 
								// there are any problems 
    		} 
    		catch(po::error& e) 
    		{ 
      			std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      			std::cerr << desc << std::endl; 
	      		// return ERROR_IN_COMMAND_LINE; 
    		}  
  		} 
  		catch(std::exception& e) 
  		{ 
	    	std::cerr << "Unhandled Exception reached the top of main: " 
              		<< e.what() << ", application will now exit" << std::endl; 
    		// return ERROR_UNHANDLED_EXCEPTION;  
 	 	}
		return opts;
	};

};