#pragma once

/*!
	@file graphSampling.h
	@author Siddharth Tourani
	@brief Return chains, trees. Types of sampling:
	1) Spanning tree, 	
	2) Local-PD, 		
	3) Induced Chains,	
	4) Edge,			
	5) Row-Col,			
	6) Row-Edge,		
	7) Maximal Monotonic Chains.

	Possible UTs:
	1) Grid Graphs
		MMC Long-to-short. 
		IC  Long-to-short.
		RC  Fixed.
		RE  Fixed.
		ST  Fixed.
		LPD Fixed.
		E   Doesn't work so well.

	2) Complete Graphs
		MMC Long-to-short.
		IC  Long-to-short.
		RC  Not possible.
		RE  Fixed.
		ST  Fixed.

	============================================================
	//	MMC
	vector<bool> nodesDone;
	vector<bool> edgesDone;
	vector<int> edgesRemainingForNode;
	MMC start with node 0.
		Go through neighboring edges find one that is free and 
		Add edge to chain
		Add neighboring node to chain
		Until growth is not posisble
		terminate then
	Keep continuing until all nodes are covered.
	=============================================================

	=============================================================
	//	ST
	Weigh edge array as 1.
	Compute the min weight spanning tree
	Construct the tree, treeNodes, treeEdges.
		i.e. Add nodes to the tree. 
		i.e. Add edges to the tree.
		i.e. Construct the tree.
	Reweigh the edges.
	=============================================================

	=============================================================
	//	Induced Chain
	Add a single edge. Add incident nodes.
	Check the neighbors of the neighbors of the head or the tail.
	If the NofN exists in the chain, then don't add it to the chain.
	=============================================================

	=============================================================
	//	Edge Decomposition
	Only Edges.
	=============================================================

	=============================================================
	//	Parallel Induced Chains
	Grow Multiple Chains Simultaneously
	=============================================================

	Possible design for nodes, 
*/

#include "config.h"
#include "chain.h"
#include "tree.h"
#include "shortestPathSampler.h"
#include "shortestPathSampler2.h"
#include "shortestPathSampler3.h"

namespace Solver
{
	template<class graphType, class vecType>
	class graphSampling
	{
		public:
		//	there has to be some template specialization for the different types of graphs	

		graphType* gr;
		Solver::sampType sampling;
		// std::vector<subGraph> subGraphs;						//!	

		graphSampling(graphType& g)
		{
			// sampling=samp;
			gr=(&g);
		}

		graphSampling(graphType& g,
					  Solver::sampType st):
					  sampling(st)
		{
			sampling=st;
			gr=(&g);
		}

		//!	Row-Col Decomposition
		chain<vecType> growRowColChain(const size_t id, const size_t diff, const size_t chLen);

		// private:
		//!	row-col
		edgeHalf& findEdgeHalf(const size_t id, const size_t diff);
		//!	MMC
		bool findNextEdge(size_t id, 
							// const std::vector<bool>& nodesDone, 
							// const std::vector<int>& edgesReamining,
							const std::vector<bool>& edgesDone, 
							edgeHalf& eH);

		//!	inducedChain
		bool findNoNinChain();										//!	
		bool checkIfGraphIsGrid();									//!		
		void sample();												//!		

		std::vector<tree> spanningTreeDecomposition();			//!		
		std::vector<tree> arcInconsistentDecomposition();		//!		

		std::vector<chain<vecType>> shortestPathDecomposition();
		std::vector<chain<vecType>> rowColDecomposition();				//!		
		std::vector<chain<vecType>> rowEdgeDecomposition();				//!		
		std::vector<chain<vecType>> MMCDecomposition();					//!		
		std::vector<chain<vecType>> inducedChainDecomposition();			//!		
		std::vector<chain<vecType>> edgeDecomposition();					//!	
		std::vector<chain<vecType>> shortestPathDecomposition2();
		std::vector<chain<vecType>> shortestPathDecomposition3();
	};

	template<class graphType,class vecType>
	void graphSampling<graphType,vecType>::sample()
	{
		switch(sampling)
		{
			case Solver::SPANNING_TREES:
				spanningTreeDecomposition();
				std::cout << "Spanning Trees\n";
				break;

			case Solver::ARC_INCONSISTENT:
				arcInconsistentDecomposition();
				std::cout << "Arc Inconsistent\n";
				break;

			case Solver::INDUCED_CHAINS:
				// inducedChainDecomposition();
				std::cout << "Induced Chains\n";
				break;

			case Solver::SHORTEST_PATH:
				shortestPathDecomposition();
				std::cout << "Shortest Path Decomposition\n";
				break;

			case Solver::ROW_COL:
				rowColDecomposition();
				std::cout << "Row Col\n";
				break;

			case Solver::ROW_EDGE:
				rowEdgeDecomposition();
				std::cout << "Row Edge\n";
				break;

			case Solver::EDGE:
				edgeDecomposition();
				std::cout << "Edge Decomposition\n";
				break;

			case Solver::MAX_MONO_CHAINS:
				MMCDecomposition();
				std::cout << "Maximal Monotonic Chains\n";
				break;

			default:
				std::cout << "Unknown Graph Type. Throw Exception Here.\n";
				break;
		}
	}

	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::shortestPathDecomposition()
	{
		Solver::shortestPathSampler<graphType,vecType> spSampler(*gr);		//	
		std::vector<chain<vecType>> chains=spSampler.sampleGraph();			//	
		return chains;												//	
	}


	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::shortestPathDecomposition2()
	{
		Solver::shortestPathSampler2<graphType,vecType> spSampler(*gr);		//	
		std::vector<chain<vecType>> chains=spSampler.sampleGraph();			//	
		return chains;														//	
	}

	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::shortestPathDecomposition3()
	{
		Solver::shortestPathSampler3<graphType,vecType> spSampler(*gr);
		std::vector<chain<vecType>> chains=spSampler.sampleGraph();
		return chains;
	}

	template<class graphType, class vecType>
	std::vector<tree> graphSampling<graphType,vecType>::spanningTreeDecomposition()
	{
		std::vector<tree> trees;

		std::vector<bool> edgesDone(gr->m_nE,false);
		size_t numEdgesDone=0;

		edgePair* edges=new edgePair[gr->m_nE];
		for(size_t e=0;e<gr->m_E.size();++e)
		{
			edges[e]=edgePair(gr->m_E[e].s,gr->m_E[e].t);
		}

		int* weights=new int[gr->m_nE];
		for(size_t i=0;i<gr->m_nE;++i)	weights[i]=1;
		while(numEdgesDone<gr->m_nE)
		{
			bGraph g(edges, edges + gr->m_E.size(), weights,gr->m_nV);
			boost::property_map < bGraph, boost::edge_weight_t >::type weight = boost::get(boost::edge_weight, g);
			std::vector < bEdge > spanning_tree;
			boost::kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));

			std::vector<edgeEnds> edgesInTree;

			for (std::vector < bEdge >::iterator ei = spanning_tree.begin();
				ei != spanning_tree.end(); ++ei) 
			{
				edgeEnds eE;
				eE.s=boost::source(*ei,g);
				eE.t=boost::target(*ei,g);
				edgesInTree.push_back(eE);
			}

			std::vector<size_t> eInds;
			for(size_t i=0;i<edgesInTree.size();++i)
			{
				edgeEnds& pr=edgesInTree[i];
				for(size_t e=0;e<gr->m_nE;++e)
				{
					if( (edges[e].first==pr.s)
					 && (edges[e].second==pr.t))
					{
						weights[e]++;
						eInds.push_back(e);
						if(!edgesDone[e])
						{
							edgesDone[e]=true;
							numEdgesDone++;
						}
					}
				}
			}

			Solver::tree tr(edgesInTree,eInds,gr->m_nV,gr->m_nV,gr->m_nE);

			tr.completeConnectivity();
			trees.push_back(tr);
		}
		return trees;
	}

	template<class graphType, class vecType>
	std::vector<tree> graphSampling<graphType,vecType>::arcInconsistentDecomposition()
	{
		std::cout << BOLDRED << "Arc-Inconsistent\n" << RESET;
	}

	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::edgeDecomposition()
	{
		std::vector<chain<vecType>> edgeChains;
		for(size_t i=0;i<gr->m_E.size();++i)
		{
			chain<vecType> ch;							//	
			size_t s=gr->m_E[i].s;						//	
			size_t t=gr->m_E[i].t;						//	
			chainNode<vecType> cN1(s);					//	
			chainNode<vecType> cN2(t);					//	
			chainEdge<vecType> cE(i,s,t);				//	
			ch.addNode(cN1);							//	
			ch.addNode(cN2);							//	
			ch.addEdge(cE);								//	
			edgeChains.push_back(ch);					//	
		}
		return edgeChains;								//	
	}

	//! Assuming a row-major-ordering
	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::rowColDecomposition()
	{
		std::vector<chain<vecType>> chains;						//	
		size_t nRows, nCols;							//	
		size_t rowDiff;									//	

		assert(gr->getNbrsToNode(0).size()>0);			//	
		adjacencyVector& aV=gr->getNbrsToNode(0);		//	

		size_t ndID1=aV[0].ndID;						//	
		size_t ndID2=aV[1].ndID;						//	
	
		if(ndID1==1)									//	
		{
			nCols=ndID2;								//	
			nRows=(gr->m_nV)/nCols;						//	
		}
		else
		{
			nCols=ndID2;								//	
			nRows=(gr->m_nV)/nCols;						//	
		}

		assert((nCols*nRows)==gr->m_nV);				//	
 		size_t id=0;									//	
 		size_t count=0;									//	
 		size_t id2,eID;									//	

		//	Grow Row Chains
 		for(size_t r=0;r<nRows;++r)						//	
 		{
 			id=nCols*r;									//	
 			chain<vecType> ch=growRowColChain(id,1,nCols);		//	
			chains.push_back(ch);						//	
 		}
		//	Grow column chains
 		for(size_t c=0;c<nCols;++c)						//	
 		{
 			id=c;										//	
 			chain<vecType> ch=growRowColChain(c,nCols,nRows);	//	
			chains.push_back(ch);						//	
 		}
		return chains;									//	
	}

	//	This is the row-col chain
	template<class graphType, class vecType>
	chain<vecType> graphSampling<graphType,vecType>::growRowColChain(
									const size_t id, 
									const size_t diff, 
									const size_t chLen)
	{
		chain<vecType> ch;
		chainNode<vecType> cN(id);
		ch.setHead(id);

		size_t curID=id;
		size_t i=0;
		for(size_t i=0;i<chLen-1;++i)
		{
			edgeHalf& eH=findEdgeHalf(curID,diff);
			// std::cout << "Adding ndID: " << eH.ndID << " eID: " << eH.eID << "\n";
			ch.addToTail(eH.ndID,eH.eID);
		
			// std::cout << ch << "\n";
			// size_t curID2=eH.ndID;
			// chainNode cN2(curID2);
			// ch.addToTail(eH.ndID,eH.eID);
			// std::cout << "head: " << ch.head << " tail: " << ch.tail << "\n";
			// std::cout << ch << "\n";
			// chainEdge cE;
			// cE.id=eH.eID;
			// if(curID2>curID)
			// {
			// 	cE.s=curID;		cE.t=curID2;
			// }
			// else
			// {
			// 	cE.s=curID2;	cE.t=curID;
			// }
			// std::cout << cE << "\n";
			// ch.addEdge(cE);

			curID=eH.ndID;
		}
		// std::cout << ch << "\n";
		return ch;
	}

	//!	
	template<typename graphType, class vecType>
	inline edgeHalf& graphSampling<graphType,vecType>::findEdgeHalf(const size_t id, const size_t diff)
	{
		// std::cout << BOLDRED << "Starting Find edge half\n" << RESET;
		adjacencyVector&  aV=gr->getNbrsToNode(id);
		
		// for(size_t i=0;i<aV.size();++i)
		// {
		// 	std::cout << "ndID: " << aV[i].ndID << " eID: " << aV[i].eID << "\n";
		// }

		size_t i=0;
		for(;i<aV.size();++i)
		{
			size_t ndID=aV[i].ndID;
			size_t eID=aV[i].eID;
			if( ((ndID-id)==diff || (ndID-id)==-diff) && (ndID>id) )
			{
				break;
			}
		}

		if(i==aV.size())
		{
			throw std::runtime_error("Couldn't find elment.\n");
		}
		// std::cout << "winning ndID: " << aV[i].ndID << " eID: " << aV[i].eID << "\n";
		// std::cout << BOLDRED << "Stopping Find edge half\n" << RESET;
		return aV[i];
	}

	//!	This is going to be great.
	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::rowEdgeDecomposition()
	{
		std::vector<chain<vecType>> chains;

		size_t nRows, nCols;
		size_t rowDiff;

		assert(gr->getNbrsToNode(0).size()>0);
		adjacencyVector& aV=gr->getNbrsToNode(0);

		size_t ndID1=aV[0].ndID;
		size_t ndID2=aV[1].ndID;
	
		// std::cout << "id1: " << ndID1 << " id2: " << ndID2 << "\n";

		if(ndID1==1)
		{
			nCols=ndID2;
			nRows=(gr->m_nV)/nCols;
		}
		else
		{
			nCols=ndID2;
			nRows=(gr->m_nV)/nCols;
		}

		// std::cout << "nRows: " << nRows << " nCols: " << nCols << "\n";

		assert((nCols*nRows)==gr->m_nV);
 
 		// std::cout << "nR: " << nRows << " nC: " << nCols << "\n";
 		
 		// std::vector<chain> rowChains;
 		// std::vector<chain> edgeChains;

		// std::vector<chain> chains;
	

 		size_t id=0;	//	this is the starting node.
 		size_t count=0;
 		size_t id2,eID;
 		for(size_t r=0;r<nRows;++r)
 		{
 			id=nCols*r;
 			chain<vecType> ch=growRowColChain(id,1,nCols);
			chains.push_back(ch);
 		}

 		for(size_t c=0;c<nCols;++c)
 		{
 			for(size_t r=0;r<nRows-1;++r)	
 			{
 				size_t id1=r*nCols+c;
 				size_t id2=(r+1)*nCols+c;

 				chain<vecType> ch;
				
 				// chainNode cN1(id1);
 				// ch.addNode(cN1);
 				edgeHalf& eH=findEdgeHalf(id1,nCols);
 				// chainNode cN2(eH.ndID);
				// ch.addNode(cN2);
				// chainEdge cE(eH.eID);
				// ch.addEdge(cE);
				
				ch.setHead(id1);
				ch.addToTail(eH.ndID,eH.eID);

				chains.push_back(ch);
 			}
 		}
		return chains;
	}

	//!	
	template<class graphType,class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::MMCDecomposition()
	{
		std::vector<chain<vecType>> chains;

		std::vector<int> edgesRemaining(gr->m_nV);
		for(size_t i=0;i<edgesRemaining.size();++i)
		{
			edgesRemaining[i]=gr->getNbrsToNode(i).size();
		}

		std::vector<bool> edgesDone(gr->m_nE);
		size_t num_edges_done=0;
		size_t num_nodes_done=0;
		size_t currentNode=0;
		size_t id=currentNode;

		//	until the edges are all done,
		while(num_edges_done<gr->m_nE)
		{
			for(size_t i=currentNode;i<edgesRemaining.size();++i)
			{
				if(edgesRemaining[i]>0)
				{
					id=i;
					break;
				}
			}

			//	MMC Chain is grown here.
			chain<vecType> ch;
			chainNode<vecType> cN(id);
			// ch.addNode(cN);
			ch.setHead(id);
			// std::cout << ch;
			
			bool growthPossible=true;
			while(1)
			{
				edgeHalf eH;
				growthPossible=findNextEdge(id,
										// nodesDone,
										// edgesRemaining,
										edgesDone,eH);

				if(growthPossible)
				{
					chainNode<vecType> cN2(eH.ndID);
					chainEdge<vecType> cE(eH.eID,id,eH.ndID);
					ch.addToTail(eH.ndID,eH.eID);
					// ch.addNode(cN2);
					// ch.addEdge(cE);
					edgesDone[eH.eID]=true;
					edgesRemaining[eH.ndID]--;
					edgesRemaining[id]--;
					num_edges_done++;
					id=eH.ndID;
				}
				else
				{
					break;
				}
			}
			chains.push_back(ch);
		}
		return chains;
	}

	//!	Make sure there is a valid edge before 
	template<class graphType, class vecType>
	bool graphSampling<graphType,vecType>::findNextEdge(size_t id,
								// const std::vector<bool>& nodesDone, 
								// const std::vector<int>& edgesReamining,
								const std::vector<bool>& edgesDone, 
								edgeHalf& eH)
	{
		//!	get the neighboring edges
		adjacencyVector& aV=gr->getNbrsToNode(id);
		
		// for(size_t i=0;i<aV.size();++i)
		// {
		// 	std::cout << "ndID: " << aV[i].ndID << " eID: " << aV[i].eID << "\n";
		// }

		for(size_t i=0;i<aV.size();++i)
		{
			size_t eID=aV[i].eID;	//	eID
			size_t ndID=aV[i].ndID;	//	ndID
			if(edgesDone[eID])		//	if the edge is done,
			{	
				continue;
			}
			else
			{
				if(ndID>id)			//	the edge is not done,
				{					//	if the nodeID is 
					eH=aV[i];		//	
					return true;	//	
				}					//	
			} 
		}
		return false;

	}

	/*!
		Pick one edge and two nodes.
		head and tails.
		try to grow the head.
		try to grow the tail.
	*/
	template<class graphType, class vecType>
	std::vector<chain<vecType>> graphSampling<graphType,vecType>::inducedChainDecomposition()
	{
		// std::cout << BOLDRED << "Induced Chain Decomposition\n" << RESET;	
		// size_t eID,nd1,nd2;
		// size_t head,tail;

		// size_t numEdgesDone=0;
		// while(numEdgesDone<gr->m_nE)
		// {
		// 	//	pick the start egde
		// }

	}

	/*!
		@brief IS THIS A DEBUG FUNCTION
	*/
	template<class graphType, class vecType>
	bool graphSampling<graphType,vecType>::findNoNinChain()
	{

	}

	/*!
		@brief The comfort of normality.
	*/
	template<class graphType,class vecType>
	bool graphSampling<graphType,vecType>::checkIfGraphIsGrid()
	{

	}

};