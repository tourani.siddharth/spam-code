#pragma once
/*!
	@file testGraphs.h
	@brief Creates test graphs for debugging purposes. 
	       Mainly gridGraphs, denseGraphs, randomGraphs.
	@author Siddharth Tourani
*/

// #include "hGraph.h"
// #include "dGraph.h"
#include "uGraph.h"
#include "config.h"
namespace Graph
{
	template<class T>
	class StorageBase
	{
	public:
		std::vector<std::vector<T>> unaries;
		std::vector<std::vector<T>> pairwise;
		std::vector<pwSize> pwSz;
		// graphType G;
	};

	//	Maybe no template specialization is required here. Only a class
	template<class T, class graphType>
	class graphStorage: public StorageBase<T>
	{
		public:
		graphType G;
		void printGraphType()
		{
			std::cout << "graph is of type: ";
			if(typeid(uGraph)==typeid(G))
			{
				std::cout << "uGraph";
			}
			else if(typeid(hGraph)==typeid(G))
			{
				std::cout << "hGraph";
			}
			else if(typeid(dGraph)==typeid(G))
			{
				std::cout << "dGraph";
			}
			else
			{
				std::cout << "unkown";
			}
			std::cout << "\n";
		}

		friend std::ostream& operator<<(std::ostream& os, graphStorage<T,graphType>& gT)
		{
			os << "graph is of type: ";
			if(typeid(uGraph)==typeid(gT.G))
			{
				os << "uGraph";
			}
			else if(typeid(hGraph)==typeid(gT.G))
			{
				os << "hGraph";
			}
			else if(typeid(dGraph)==typeid(gT.G))
			{
				os << "dGraph";
			}
			else
			{
				os << "unkown";
			}
			os << "\n";
			
			os << "num unaries: " << gT.unaries.size() << " | ";
			os << "num pairwise: " << gT.pairwise.size() << "\n";
			os << "pwSz: " << gT.pwSz.size() << "\n";
			os << "\n";

			os << "Unaries\n";
			if(gT.unaries.empty())
			{
				std::cout << BOLDRED << "No unaries\n" << RESET;
			}
			else
			{
				for(size_t i=0;i<gT.unaries.size();++i)
				{
					os << "un[" << i << "]: (";
					for(size_t j=0;j<gT.unaries[i].size()-1;++j)
					{
						os << gT.unaries[i][j] << ", ";
					}
					os << gT.unaries[i][gT.unaries[i].size()-1];
					os << ")\n";
				}
				os <<"\n";
			}

			os << "Pairwise\n";
			if(gT.pairwise.empty())
			{
				os << BOLDRED << "No pairwise yet\n" << RESET;
			}
			else
			{
				for(size_t e=0;e<gT.pairwise.size();++e)
				{
					size_t l2=gT.pwSz[e].l2;
					//	Can I maybe get rid of the 
					os << gT.G.m_E[e].s << "---" << gT.G.m_E[e].t << "\n";
					for(size_t i=0;i<gT.pwSz[e].l1;++i)
					{
						for(size_t j=0;j<l2;++j)
						{
							os << gT.pairwise[e][i*l2+j] << " ";
						}	
						os << "\n";
					}
					os << "\n";
				}	
				os <<"\n";
			}

			return os;
		}
	};

/*
	template<class T>
	struct graphStorage<T,uGraph> : public StorageBase<T>
	{
		uGraph G;
		void printGraphType()
		{
			std::cout << "uGraph\n";
		}		
	};

	template<class T>
	struct graphStorage<T,hGraph> : public StorageBase<T>
	{
		hGraph G;
		void printGraphType()
		{
			std::cout << "hGraph\n";
		}
	};

	template<class T>
	struct graphStorage<T,dGraph> : public StorageBase<T>
	{
		dGraph G;
		void printGraphType()
		{
			std::cout << "dGraph\n";
		}
	};
*/

	template<typename T,class graphType>
	void AllocateMemory(const size_t R, 
						const size_t C, 
						const size_t K,
						const bool labelsFixed,
						graphStorage<T,graphType>& gT)
	{
		gT.unaries.resize(R*C);
		gT.pairwise.resize(R*(C-1)+C*(R-1));
		gT.pwSz.resize(R*(C-1)+C*(R-1));
		if(labelsFixed)
		{
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				gT.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				gT.unaries[i].resize(L);
			}
		}
	}

	template<typename T,class graphType>
	void AllocateMemory(const size_t N,
						const size_t K,
						const bool labelsFixed,
						graphStorage<T,graphType>& gT)
	{
		size_t nE=(N*(N-1))/2;
		gT.unaries.resize(N);
		gT.pairwise.resize(nE);
		gT.pwSz.resize(nE);
		if(labelsFixed)
		{
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				gT.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<gT.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				gT.unaries[i].resize(L);
			}
		}
	}


	template<typename T>
	void FillUpUnaries(std::vector<std::vector<T>>& unaries)
	{
		for(size_t i=0;i<unaries.size();++i)
		{
			for(size_t j=0;j<unaries[i].size();++j)
			{
				unaries[i][j]=rand()%10;		//	add the unary potentials
			}
		}
	}


	template<class T,class graphType>
	void CreateGridGraph(const size_t R, 
			   		     const size_t C, 
		 				 const size_t K, 
						 const bool labelsFixed,
						 graphStorage<T,graphType>& gT)
	{
		std::cout << "Unknown graph type\n";
	}

	template<class T>
	void CreateGridGraph(const size_t R, const size_t C,
						 const size_t K, const bool labelsFixed,
						 graphStorage<T,Graph::uGraph>& gS)
	{
		std::cout << "Create a uGraph\n";
		std::vector<edgeEnds> graphEdges(R*(C-1)+C*(R-1));
		AllocateMemory(R,C,K,labelsFixed,gS);
		FillUpUnaries(gS.unaries);

		size_t pwID=0;
		//	rows
		for(size_t r=0;r<R;++r)
		{
			for(size_t c=0;c<C-1;++c)
			{
				size_t ind1=r*C+c;						//	
				size_t ind2=r*C+c+1;					//	
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges

				size_t l1=gS.unaries[ind1].size();		//	
				size_t l2=gS.unaries[ind2].size();		//	
				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		//	cols
		for(size_t c=0;c<C;++c)
		{
			for(size_t r=0;r<R-1;++r)	
			{
				size_t ind1=r*C+c;
				size_t ind2=(r+1)*C+c;
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges
				size_t l1=gS.unaries[ind1].size();
				size_t l2=gS.unaries[ind2].size();
				gS.pairwise[pwID].resize(l1*l2);
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;
					}
				}
				pwID++;
			}
		}

		gS.G.init(R*C,graphEdges);
	}

	template<class T>
	void CreateCompleteGraph(const size_t N, const size_t K, 
							 const bool labelsFixed,
						     graphStorage<T,Graph::uGraph>& gS)
	{
		std::cout << "Create a uGraph\n";
		size_t nE=(N*(N-1))/2;
		std::vector<edgeEnds> graphEdges(nE);
		AllocateMemory(N,K,labelsFixed,gS);
		FillUpUnaries(gS.unaries);

		size_t pwID=0;
		for(size_t i=0;i<N;++i)
		{
			for(size_t j=i+1;j<N;++j)
			{
				graphEdges[pwID].s=i;				//	
				graphEdges[pwID].t=j;				//	add the graph edges
				size_t l1=gS.unaries[i].size();		//	
				size_t l2=gS.unaries[j].size();		//	
				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		gS.G.init(N,graphEdges);
	}

	template<class T>
	void CreateGridGraph(const size_t R, const size_t C, 
						const size_t K, const bool labelsFixed,
						graphStorage<T,Graph::hGraph>& gS)
	{
		std::cout << "Create a hGraph\n";
		std::vector<edgeEnds> graphEdges(R*(C-1)+C*(R-1));
		AllocateMemory(R,C,K,labelsFixed,gS);
		FillUpUnaries(gS.unaries);

		size_t pwID=0;
		//	rows
		for(size_t r=0;r<R;++r)
		{
			for(size_t c=0;c<C-1;++c)
			{
				size_t ind1=r*C+c;						//	
				size_t ind2=r*C+c+1;					//	
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges

				size_t l1=gS.unaries[ind1].size();		//	
				size_t l2=gS.unaries[ind2].size();		//	
				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		//	cols
		for(size_t c=0;c<C;++c)
		{
			for(size_t r=0;r<R-1;++r)	
			{
				size_t ind1=r*C+c;
				size_t ind2=(r+1)*C+c;
				graphEdges[pwID].s=ind1;				//	
				graphEdges[pwID].t=ind2;				//	add the graph edges

				size_t l1=gS.unaries[ind1].size();
				size_t l2=gS.unaries[ind2].size();
				gS.pairwise[pwID].resize(l1*l2);
				gS.pwSz[pwID].l1=l1;
				gS.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						gS.pairwise[pwID][ii*l2+jj]=rand()%10;
					}
				}
				pwID++;
			}
		}
		gS.G.init(R*C,graphEdges);
	}


/*

// 	template<class T>
// 	void CreateGridGraph(const size_t R, const size_t C, 
// 						const size_t K, const bool labelsFixed,
// 						graphStorage<T,Graph::dGraph>& gS)
// 	{
// 		std::cout << "Create a dGraph\n";
// 		std::vector<edgeEnds> graphEdges(R*(C-1)+C*(R-1));
// 		AllocateMemory(R,C,K,labelsFixed,gS);
// 		FillUpUnaries(gS.unaries);

// 		size_t pwID=0;
// 		//	rows
// 		for(size_t r=0;r<R;++r)
// 		{
// 			for(size_t c=0;c<C-1;++c)
// 			{
// 				size_t ind1=r*C+c;						//	
// 				size_t ind2=r*C+c+1;					//	
// 				graphEdges[pwID].s=ind1;				//	
// 				graphEdges[pwID].t=ind2;				//	add the graph edges

// 				size_t l1=gS.unaries[ind1].size();		//	
// 				size_t l2=gS.unaries[ind2].size();		//	
// 				gS.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
// 				gS.pwSz[pwID].l1=l1;
// 				gS.pwSz[pwID].l2=l2;

// 				for(size_t ii=0;ii<l1;++ii)
// 				{
// 					for(size_t jj=0;jj<l2;++jj)
// 					{
// 						gS.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
// 					}
// 				}
// 				pwID++;
// 			}
// 		}

// 		//	cols
// 		for(size_t c=0;c<C;++c)
// 		{
// 			for(size_t r=0;r<R-1;++r)	
// 			{
// 				size_t ind1=r*C+c;
// 				size_t ind2=(r+1)*C+c;
// 				graphEdges[pwID].s=ind1;				//	
// 				graphEdges[pwID].t=ind2;				//	add the graph edges

// 				size_t l1=gS.unaries[ind1].size();
// 				size_t l2=gS.unaries[ind2].size();
// 				gS.pairwise[pwID].resize(l1*l2);
// 				gS.pwSz[pwID].l1=l1;
// 				gS.pwSz[pwID].l2=l2;


// 				for(size_t ii=0;ii<l1;++ii)
// 				{
// 					for(size_t jj=0;jj<l2;++jj)
// 					{
// 						gS.pairwise[pwID][ii*l2+jj]=rand()%10;
// 					}
// 				}
// 				pwID++;
// 			}
// 		}

// 		gS.G.init(R*C,graphEdges);
// 		// return gS;	
// 	}

	template<typename T>
	dGraphStorage<T> CreateCompleteDGraph(const size_t nV, const size_t K,	const bool labelsFixed)
	{
		// assert(K>1);
		dGraphStorage<T> dG;
		dG.unaries.resize(nV);
		dG.pairwise.resize(((nV*(nV-1))/2));
		dG.pwSz.resize(((nV*(nV-1))/2));
		std::vector<edgeEnds> graphEdges(((nV*(nV-1))/2));

		if(labelsFixed)
		{
			for(size_t i=0;i<dG.unaries.size();++i)
			{
				dG.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<dG.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				dG.unaries[i].resize(L);
			}
		}

		for(size_t i=0;i<dG.unaries.size();++i)
		{
			for(size_t j=0;j<dG.unaries[i].size();++j)
			{
				dG.unaries[i][j]=rand()%10;		//	add the unary potentials
			}
		}

		size_t pwID=0;
		//	rows
		for(size_t i=0;i<nV;++i)
		{
			for(size_t j=i+1;j<nV;++j)
			{
				graphEdges[pwID].s=i;				//	
				graphEdges[pwID].t=j;				//	add the graph edges

				size_t l1=dG.unaries[i].size();		//	
				size_t l2=dG.unaries[j].size();		//	
				dG.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				dG.pwSz[pwID].l1=l1;
				dG.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						dG.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		dG.dG.init(nV,graphEdges);
		return dG;
	}

	template<typename T>
	hGraphStorage<T> CreateGridHGraph(const size_t R, const size_t C, 	const size_t K,	const bool labelsFixed)
	{
		hGraphStorage<T> hG;
		hG.unaries.resize(R*C);
		hG.pairwise.resize(R*(C-1)+C*(R-1));
		hG.pwSz.resize(R*(C-1)+C*(R-1));
		std::vector<edgeEnds> graphEdges(R*(C-1)+C*(R-1));

		if(labelsFixed)
		{
			for(size_t i=0;i<hG.unaries.size();++i)
			{
				hG.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<hG.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				hG.unaries[i].resize(L);
			}
		}

		for(size_t i=0;i<hG.unaries.size();++i)
		{
			for(size_t j=0;j<hG.unaries[i].size();++j)
			{
				hG.unaries[i][j]=rand()%10;		//	add the unary potentials
			}
		}


		return hG;
	}

	template<typename T>
	hGraphStorage<T> CreateCompleteHGraph(const size_t nV, 	const size_t K,	const bool labelsFixed)
	{
		hGraphStorage<T> hG;
		hG.unaries.resize(nV);
		hG.pairwise.resize(((nV*(nV-1))/2));
		hG.pwSz.resize(((nV*(nV-1))/2));
		std::vector<edgeEnds> graphEdges(((nV*(nV-1))/2));

		if(labelsFixed)
		{
			for(size_t i=0;i<hG.unaries.size();++i)
			{
				hG.unaries[i].resize(K);
			}
		}
		else
		{
			assert(K>2);
			for(size_t i=0;i<hG.unaries.size();++i)
			{
				size_t L=(rand()%(K-2))+2;
				hG.unaries[i].resize(L);
			}
		}

		for(size_t i=0;i<hG.unaries.size();++i)
		{
			for(size_t j=0;j<hG.unaries[i].size();++j)
			{
				hG.unaries[i][j]=rand()%10;		//	add the unary potentials
			}
		}

		size_t pwID=0;
		//	rows
		for(size_t i=0;i<nV;++i)
		{
			for(size_t j=i+1;j<nV;++j)
			{
				graphEdges[pwID].s=i;				//	
				graphEdges[pwID].t=j;				//	add the graph edges

				size_t l1=hG.unaries[i].size();		//	
				size_t l2=hG.unaries[j].size();		//	
				hG.pairwise[pwID].resize(l1*l2);		//	add the pairwise length
				hG.pwSz[pwID].l1=l1;
				hG.pwSz[pwID].l2=l2;

				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						hG.pairwise[pwID][ii*l2+jj]=rand()%10;	//	add the pairwise potentials
					}
				}
				pwID++;
			}
		}

		hG.hG.init(nV,graphEdges);
		return hG;
	}
*/

};