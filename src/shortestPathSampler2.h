#pragma once
/*!
    @brief An animated embedding. 
    @author 
    @date
*/

#include "config.h"
#include "chain.h"

namespace Solver
{
    template<class graphType, class vecType>
    class shortestPathSampler2
    {
        public:
            shortestPathSampler2(graphType& gr);
            std::vector<chain<vecType>> sampleGraph();        
            std::map<size_t,std::vector<edgeHalf>> createAdjMap();
            size_t getNextID(const size_t id,std::vector<int>& numNbrsOfNodes);
            void computeDistanceGraph(std::vector<int>& distances,const int nodeID);

            void printPQ(std::priority_queue<edgePair, 
                        std::vector<edgePair>,
                        std::greater<edgePair> >& pq);

            //  Member Variables
            std::map<size_t,std::vector<edgeHalf>> adjMap;
            std::shared_ptr<graphType> grPtr;
    };

    template<class graphType, class vecType>
    shortestPathSampler2<graphType,vecType>::shortestPathSampler2(graphType& gr)
    {
        grPtr=std::make_shared<graphType>(gr);
        adjMap=createAdjMap();
    }

    template<class graphType, class vecType>
    std::map<size_t,std::vector<edgeHalf>> shortestPathSampler2<graphType,vecType>::createAdjMap()
    {
        std::map<size_t,std::vector<edgeHalf>> adjMap;
        for(size_t i=0;i<grPtr->m_nV;++i)
        {
            std::vector<edgeHalf> eVector;
            adjMap[i]=grPtr->getNbrsToNode(i);
        }
        return adjMap;
    }

    /*!
        Starting from first principles. 
    */
    template<class graphType, class vecType>
    std::vector<chain<vecType>> shortestPathSampler2<graphType,vecType>::sampleGraph()
    {
        // std::vector<bool> edgesDone(grPtr->m_nE,false);     // 
        size_t numEdgesDone=0;                              //  
        std::vector<chain<vecType>> chains;                 //  
        bool graphSamplingComplete=false;                   //  
        std::vector<int> distances(grPtr->m_nV,intInf);     //  
        bool nodeFresh=false;

        std::vector<bool> blockedNodes(grPtr->m_nV,false);  //  
        std::vector<int> numNbrOfNodes(grPtr->m_nV,0);      //  

        for(size_t i=0;i<numNbrOfNodes.size();++i)          //  
        {                                                   //  
            numNbrOfNodes[i]=grPtr->getNbrsToNode(i).size();//  
        }                                                   //  

        std::cout << "distances: ";                         //  
        for(size_t i=0;i<distances.size();++i)              //  
        {                                                   //  
            std::cout << distances[i] << " ";               //  
        }                                                   //  
        std::cout << "\n\n";                                //  

        std::cout << "num nbr of nodes: ";                  //  
        for(size_t i=0;i<numNbrOfNodes.size();++i)          //  
        {                                                   //  
            std::cout << numNbrOfNodes[i] << " ";           //  
        }                                                   //  
        std::cout << "\n\n";                                //  

        std::vector<int> edgeDone(grPtr->m_nE,0);           //  
        size_t currentID=0;                                 //  
        nodeFresh=true;
        std::cout << "currentID: " << currentID << "\n";

        // while(!graphSamplingComplete)                    //   
        {                                                   //  
            if(numNbrOfNodes[currentID]==0)
            {
                size_t currentID=getNextID(currentID,numNbrOfNodes);
                nodeFresh=true;
            }
            if(nodeFresh)
            {
                distances[currentID]=0;                             //  
                std::priority_queue<edgePair,
                        std::vector<edgePair>,
                        std::greater<edgePair> > pq;                //  
                pq.push(std::make_pair(0,currentID));               //  

                while(!pq.empty())
                {
                    size_t nd=pq.top().second;
                    pq.pop();

                    std::cout << nd << " ";

                    for(size_t e=0;e<adjMap[nd].size();++e)
                    {
                        size_t ndID=adjMap[nd][e].ndID;
                        size_t eID=adjMap[nd][e].eID;

                        if(distances[ndID]>distances[nd]+1)
                        {
                            distances[ndID]=distances[nd]+1;
                            pq.push(std::make_pair(distances[ndID],ndID));
                        }
                        else if(distances[ndID]==distances[nd]+1)
                        {
                            blockedNodes[ndID]=true;
                        }
                    }
                }
            }
            std::cout << "\n";
            for(size_t r=0;r<5;++r)
            {
                for(size_t c=0;c<5;++c)
                {
                    std::cout << distances[r*5+c] << " ";
                }
                std::cout << "\n";
            }
            std::cout << "\n";
            
            // if(all edges done)
            graphSamplingComplete=true;                     //  
        }

        return chains;
    }
/*
    template<class graphType, class vecType>
    size_t shortestPathSampler2<graphType,vecType>::getCurrentNodeID(
                                        std::vector<int>& numNbrsOfNodes) 
    {
        size_t winningID=0;
        for(size_t i=0;i<numNbrsOfNodes.size();++i)
        {
            if(numNbrsOfNodes[i]>0) 
            {
                winningID=i;
                break;
            }
        }
        return winningID;
    }
*/
    template<class graphType, class vecType>
    size_t shortestPathSampler2<graphType,vecType>::getNextID(
                                                    const size_t id,
                                                    std::vector<int>& numNbrsOfNodes)
    {
        for(size_t i=id+1;i<numNbrsOfNodes.size();++i)
        {
            if(numNbrsOfNodes[i]>0)
            {
                return i;
            }
        }
    }

    template<class graphType, class vecType>
    void shortestPathSampler2<graphType,vecType>::computeDistanceGraph(
                                                    std::vector<int>& distances,
                                                    const int nodeID)
    {
        distances[nodeID]=0;
        std::priority_queue<edgePair, std::vector<edgePair>, std::greater<edgePair> > pq; 
        
        pq.push(std::make_pair(0,nodeID));

        // while(!pq.empty())
        {
            size_t currID=pq.top().first;
            pq.pop();

            std::cout << "currID[" << currID << "]: {";
            for(size_t e=0;e<adjMap[nodeID].size();++e)
            {
                std::cout << adjMap[nodeID][e].ndID << " ";
            }
            std::cout << "}\n";
        }
    }

    template<class graphType, class vecType>
    void shortestPathSampler2<graphType,vecType>::printPQ(
                                std::priority_queue<edgePair, 
                                std::vector<edgePair>, 
                                std::greater<edgePair> >& pq)
    {
        std::cout << "print pq: ";
        while(!pq.empty())
        {
            edgePair eP=pq.top();
            pq.pop();
            std::cout << eP.second << " ";
        }
        std::cout << "\n";
    }
};