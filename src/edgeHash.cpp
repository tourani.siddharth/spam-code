
#include "edgeHash.h"

size_t hashEdge(const size_t s, const size_t t, const size_t nE)
{
    return s*nE+t;
}

edgeEnds unhashEdge(const size_t hashKey, const size_t nE)
{
    edgeEnds key;
    key.s=hashKey/nE;
    key.t=hashKey%nE;
    return key;
}