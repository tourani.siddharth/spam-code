#include "testGraphs.h"

namespace Graph
{
	template<typename T>
	dGraphStorage<T> CreateGridGraph<T>(const size_t R, 
													const size_t C, 
													const size_t K,
													const bool labelsFixed=true)
	{
		size_t pwID=0;
		//	rows
		for(size_t r=0;r<R;++r)
		{
			for(size_t c=0;c<C-1;++c)
			{
				size_t ind1=r*C+c;
				size_t ind2=r*C+c+1;
				size_t l1=unaries[ind1].size();
				size_t l2=unaries[ind2].size();
				mG.pairwise[pwID].resize(l1*l2);
						
				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						mG.pairwise[pwID][ii*l2+jj]=rand()%10;
					}
				}
				pwID++;
			}
		}

		//	cols
		for(size_t c=0;c<C;++c)
		{
			for(size_t r=0;r<R-1;++r)	
			{
				size_t ind1=r*C+c;
				size_t ind2=(r+1)*C+c;
				size_t l1=unaries[ind1].size();
				size_t l2=unaries[ind2].size();
				mG.pairwise[pwID].resize(l1*l2);
						
				for(size_t ii=0;ii<l1;++ii)
				{
					for(size_t jj=0;jj<l2;++jj)
					{
						mG.pairwise[pwID][ii*l2+jj]=rand()%10;
					}
				}
				pwID++;
			}
		}		
	};
	
};