#pragma once

/*!
    @brief This class should take as input spanning trees and return as output the reparameterization
           operations that should be performed on the spanning trees in a vector<vector<Operation>> container.

           The first index corresponding to the spanning trees to be reparameterizated, 
           second index the operation to be performed.

           It should take two options, one is a TRWS option.
           The other is a HM option.
           
    @author Siddharth Tourani
    @date 


    TODO: 1) Compute tree centroid.
          2) Find unvisited node.
          3) Compute Operations for Edge Pair
          4) Remove Node From Adj. List
          5) Compute Operations.
          6) Split Trees.
          7) Remove Node From Adj. List
          8) Compute Tree Hierarchy

 */

#include "config.h"
#include "operations.h"
#include "uGraph.h"
#include "chain.h"
#include "tree.h"

namespace Solver
{
    template<class graphType, class T>
    class ST2Ops
    {
        enum OPT{HM};
        enum MSG_DIR{FWD,BWD,NONE};

        public:
            std::shared_ptr<graphType> grPtr;
            std::shared_ptr<std::vector<tree>> treesPtr;
            // std::map<size_t,size_t> edgeHash2edgeID;

            size_t m_nV;

            std::vector<Operation<T>> operations;
            ST2Ops(graphType& gr, std::vector<Solver::tree>& trees, OPT ops=HM);
            std::vector<Operation<T>> computeSpanTreeOperations(const size_t idx);
            std::map<size_t,std::vector<size_t>> computeAdjList(tree& tr);
            std::map<size_t,bool> initializeCentroidMap(tree& tr);
    
            void initialize(const size_t idx);

            void computeOperations();

            std::vector<Operation<T>> computeTreeHierarchy(const size_t idx);

            template<typename T2>
            std::map<size_t, T2> initMapForNodes(tree& tr);    

            void DFS(const size_t src, std::map<size_t,bool>& visited,
                    std::map<size_t,size_t>& subtreeSize,
                    size_t* n, mapList& treeMap,
                    std::map<size_t,bool>& centroidMarkedMap);

            size_t getTreeCentroid(mapList& treeMap, 
                        std::map<size_t,bool>& centroidMarkedMap);


            void splitTrees(mapList& aList, const size_t removedVtx, tree& tr);

            size_t getTreeCentroid(const size_t src,
                    std::map<size_t,bool>& visited, 
                    std::map<size_t,size_t> subtreeSize,
                    size_t n, mapList& treeMap,
                    std::map<size_t,bool>& centroidMarkedMap);

            size_t findUnvisitedNode(const std::map<size_t,bool>& visited);

            void printStack(std::stack<size_t> st);


            OPT opts;
    };

    template<class graphType, class T>
    ST2Ops<graphType,T>::ST2Ops(graphType& gr, 
                                std::vector<tree>& trees, 
                                OPT ops): opts(ops)
    {
        grPtr=std::make_shared<graphType>(gr);
        treesPtr=std::make_shared<std::vector<tree>>(trees);
    }

    /*!
        @brief Fills up the eHash2eID. 
     */
    template<class graphType, class T>
    void ST2Ops<graphType,T>::initialize(const size_t idx)
    {

        tree& tr=(*treesPtr)[idx];
        size_t nV=tr.m_nodes.size();
        std::map<size_t,size_t> edgeHash2eID;

        for(size_t e=0;e<tr.m_edges.size();++e)
        {
            /*
            size_t from=tr.m_edges[e].s;
            size_t to=tr.m_edges[e].t;
            size_t eHash=hashEdge(from,to,nV);
            eHash2eID[eHash]=tr.m_edges[e].id;
            */
        }
        
    }
 

    template<class graphType, class T>
    inline mapList ST2Ops<graphType,T>::computeAdjList(tree& tr)
    {
        mapList treeMap;
        for(size_t e=0;e<tr.m_edges.size();++e)
        {
            size_t from=tr.m_edges[e].s();
            size_t to=tr.m_edges[e].t();
            treeMap[from].push_back(to);
            treeMap[to].push_back(from);
        }
        return treeMap;
    }

    template<class graphType, class T>
    std::map<size_t,bool> ST2Ops<graphType,T>::initializeCentroidMap(tree& tr)
    {
        std::map<size_t,bool> centroidMarkedMap;
        for(size_t i=0;i<tr.m_nodes.size();++i)
        {
            size_t nID=tr.m_nodes[i].id();
            centroidMarkedMap[nID]=false;
        }
        return centroidMarkedMap;
    }

    template<class graphType, class T>
    void ST2Ops<graphType,T>::computeOperations()
    {
        // std::vector<Operation<T>> TrOps=computeSpanTreeOperations(0);
        std::vector<Operation<T>> TrOps=computeTreeHierarchy(0);
        // operations.push_back(TrOps);
    }


    template<class graphType, class T>
    std::vector<Operation<T>> ST2Ops<graphType,T>::computeSpanTreeOperations(const size_t idx)
    {
        assert((idx>=0) && (idx<treesPtr->size()));
        /*
        std::map<size_t,size_t> edgeHash2edgeID;

        tree& tr=(*treesPtr)[e];
        size_t nV=tr.m_nodes.size();

        for(size_t e=0;e<tr.m_edges.size();++e)
        {
            size_t from=tr.m_edges[e].s;
            size_t to=tr.m_edges[e].t;
            size_t hashKey=hashEdge(from,to,nV);

            // std::cout << "nV: " << tr.m_nodes.size() << " nE: " << tr.m_edges.size() << "\n";
        }

        */
        std::vector<Operation<T>> ops;



        return ops;

    }


    template<class graphType, class T>
    template<typename T2>
    std::map<size_t, T2> ST2Ops<graphType,T>::initMapForNodes(tree& tr)
    {
        typename std::map<size_t,T2> mapL;
        for(size_t i=0;i<tr.m_nodes.size();++i)
        {
            mapL[tr.m_nodes[i].id()]=0;
        }
        return mapL;
    }

    
    template<class graphType,class T>
    std::vector<Operation<T>> ST2Ops<graphType,T>::computeTreeHierarchy(const size_t idx)
    {
        //! map of hash_edge to MSG_DIR. All messages are initially set to 0
        //! container will be used somewhere to check if the message ...
        tree& trIDX=(*treesPtr)[idx];
        size_t nV=trIDX.m_nodes.size();
        std::map<size_t,MSG_DIR> msgChk;
        for(size_t e=0;e<trIDX.m_edges.size();++e)
        {
            size_t from=trIDX.m_edges[e].s();
            size_t to=trIDX.m_edges[e].t();
            size_t hashK=hashEdge(from,to,nV);
            msgChk[hashK]=NONE;                 //  
        }

        //! set up trees at the current level
        size_t nodeID=trIDX.m_nodes[0].id();
        std::vector<tree> treesAtCurrLevel;
        std::vector<tree> treesAtNextLevel;

        std::cout << "nodeID: " << nodeID << "\n";
        
        //! Push back the main tree
        treesAtCurrLevel.push_back(trIDX);

        //! To begin with treesAtCurrLevel contains the initial tree
        size_t numNodesRemaining=nV;

        //! Sets all the nodes to false.
        //! Since I am using spanning trees, all nodes will anyhow be present.
        std::map<size_t,bool> centroidMarkedMap=initMapForNodes<bool>(trIDX);
        std::map<size_t,bool>::iterator it;

        //! 
        std::vector<std::vector<std::vector<Operation<T>>>> operationsHierarchy;
        std::vector<std::vector<Operation<T>>> opsAtPrevLevel;
        std::vector<std::vector<Operation<T>>> opsAtCurrLevel;

        //! Compute the hierarchical minorant of the tree.
        size_t iteration=0;
        size_t level=0;


        // while(numNodesRemaining>0)
        {

            iteration++;

            treesAtNextLevel.clear();
            opsAtCurrLevel.clear();


            std::cout << "Num trees at current level: " << treesAtCurrLevel.size() << "\n";


            for(size_t i=0;i<treesAtCurrLevel.size();++i)
            {
                tree& tr=treesAtCurrLevel[i];

                line();
                std::cout << BOLDRED << "Printing Trees\n" << RESET;
                std::cout << tr << "\n";
                line();

                //! Is this logic correct
                if(tr.m_nodes.size()==1)
                {
                    numNodesRemaining--;
                    centroidMarkedMap[tr.m_nodes[0].id()]=true;
                    continue;
                }
                else if(tr.m_nodes.size()==2)
                {
                    tree& tr=treesAtCurrLevel[i];
                    std::vector<Operation<T>> operations;
                    size_t from=tr.m_edges[0].s();
                    size_t to=tr.m_edges[0].t();
                    size_t eID=tr.m_edges[0].id();
                    Operation<T> op(from,to,eID,Operation<T>::HANDSHAKE);
                    operations.push_back(op);

                    opsAtCurrLevel.push_back(operations);
                    if(!centroidMarkedMap[from])
                    {
                        centroidMarkedMap[from]=true;
                        numNodesRemaining--;

                    }

                    if(!centroidMarkedMap[to])
                    {
                        centroidMarkedMap[to]=true;
                        numNodesRemaining--;
                    }
                }
                else
                {
                    mapList treeMap=computeAdjList(tr);
                    size_t centroid=getTreeCentroid(treeMap,centroidMarkedMap);
                    
                    
                    // centroidMarkedMap[centroid]=true;
                    // numNodesRemaining--;

                    /*
                    std::vector<Operation<T>> ops=computeOperations(
                                                    treesAtCurrLevel,
                                                    centroid,
                                                    treeMap,
                                                    msgChk);

                    opsAtCurrLevel.push_back(ops);

                    //  Split the trees into sub-trees
                    treesAtNextLevel=splitTrees(treeMap,centroid,tr);
                    */   


                }
            }

            // if(opsAtCurrLevel.size()>0)
            // {
            //     operationsHierarchy.push_back(opsAtCurrLevel);
            // }

            // opsAtPrevLevel=opsAtCurrLevel;
            // treesAtCurrLevel=treesAtNextLevel;
            // level++;

        }


        // std::vector<Operation<T>> flattenedOps=flattenOperations(operationsHierarchy);
        // return flattenedOps;
        
       std::vector<Operation<T>> nullOps;
       return nullOps;
    }
 

    /*
    template<class graphType, class T>
    size_t ST2Ops<graphType,T>::hashEdge(const size_t from, const size_t to, const size_t nV)
    {
        if(from<to)
        {
            return (from+1)*(nV*nV+1)+(to+1);
        }
        else if(to<from)
        {
            return (to+1)*(nV*nV+1)+(from+1);
        }
        return -1;
    }

    template<class graphType, class T>
    std::pair<int,int> ST2Ops<graphType,T>::reverseHash(const size_t key, const size_t nV)
    {
        size_t to=key%(nV*nV+1)-1;
        size_t from=key/(nV*nV+1)-1;
        std::pair<int,int> pr(from,to);
        return pr;
    }
    */
    //  getCentroidMAP()
    //  initializeMapForNodes()
    //  findUnvisitedNode()
    //  computeAdjacencyList()
    //  initCentroidMarked()
    //  initializeCentroidMarked()
    //  initialize()
    //  removeDuplicateNodes()

    template<class graphType, class T>
    void ST2Ops<graphType,T>::printStack(std::stack<size_t> st)
    {
        std::stack<size_t> revStack;
        printf("stack: ");
        while(!st.empty())
        {
            size_t num=st.top();
            st.pop();
            revStack.push(num);
            printf("%d ", num);
        }
        printf("\n");
        printf("rev Stack: ");
        while(!revStack.empty())
        {
            size_t num=revStack.top();
            revStack.pop();
            printf("%d ", num);
        }
        printf("\n");
    }

    template<class graphType, class T>
    size_t ST2Ops<graphType,T>::findUnvisitedNode(const std::map<size_t,bool>& visited)
    {
        size_t desiredID;
        std::map<size_t,bool>::const_iterator it;

        for(it=visited.begin();it!=visited.end();++it)
        {
            if(it->second==false)
            {
                desiredID=it->first;
                break;
            }
        }
        return desiredID;
    }

    /*!
        @brief 
     */
    template<class graphType, class T>
    size_t ST2Ops<graphType,T>::getTreeCentroid(
                                mapList& treeMap, 
                                std::map<size_t,bool>& centroidMarkedMap)
    {
        mapList::iterator it=treeMap.begin();
        size_t src=it->first;
        std::map<size_t,bool> visited;
        std::map<size_t,size_t> subtreeSize;
        mapList::iterator nodesIt;

        /*!
            Mark the visited node false
            Mark subtree Size to 0
         */
        for(nodesIt=treeMap.begin();nodesIt!=treeMap.end();++nodesIt)
        {
            visited[nodesIt->first]=false;
            subtreeSize[nodesIt->first]=0;
        }

        /*!
            Do a depth first search
         */
        size_t n=0;
        DFS(src,visited,subtreeSize,&n, treeMap,centroidMarkedMap);

        std::map<size_t,size_t>::iterator It;

        //  sets this to false again
        std::map<size_t,bool>::iterator bIt;
        for(bIt=visited.begin();bIt!=visited.end();++bIt)
        {
            visited[bIt->first]=false;
        }

        //  this is the call of the recursive function
        size_t centroid=getTreeCentroid(src,visited,subtreeSize,n,treeMap,centroidMarkedMap);
        return centroid;
    }

    template<class graphType, class T>
    size_t ST2Ops<graphType,T>::getTreeCentroid(const size_t src,
                                    std::map<size_t,bool>& visited, 
                                    std::map<size_t,size_t> subtreeSize,
                                    size_t n, mapList& treeMap, 
                                    std::map<size_t,bool>& centroidMarkedMap)
    {
        bool isCentroid=true;
        visited[src]=true;
        size_t heaviestChild=0;
        std::vector<size_t>::iterator it;

        for(it=treeMap[src].begin();it!=treeMap[src].end();++it)
        {
            if(!visited[*it] && !centroidMarkedMap[*it])
            {
                if(subtreeSize[*it]>n/2)
                {
                    isCentroid=false;
                }
                if(heaviestChild==0 || 
                    subtreeSize[*it]>subtreeSize[heaviestChild])
                {
                        heaviestChild=*it;
                }
            }
        }

        if(isCentroid && n-subtreeSize[src]<=n/2)
        {
            return src;
        }

        return getTreeCentroid(heaviestChild,visited,subtreeSize,n,treeMap,centroidMarkedMap);
    }

    template<class graphType, class T>
    void ST2Ops<graphType,T>::splitTrees(mapList& aList, const size_t removedVtx, tree& tr)
    {
        std::vector<tree> trees;

        std::map<size_t,bool> visited;
        std::map<size_t,size_t> treeID;
        std::map<size_t,bool>::iterator mapIt;
        std::map<size_t,size_t> treeIdMapIt;

        size_t numTrees=0;
        std::stack<size_t> nodesStack;
        mapList::iterator it;
        for(it=aList.begin();it!=aList.end();++it)
        {
            if((it->first)!=(removedVtx))
            {
                visited[it->first]=false;
                treeID[it->first]=-1;
            }
        }

        size_t currTreeID=0;
        bool begun=false;
        size_t numNodesRemaining=aList.size()-1;
        size_t currNode;
        it=aList.begin();

        /*
        while(numNodesRemaining>0)
        {
            if(begun&& nodesStack.empty())
            {
                currTreeID++;
                size_t id=findUnvisitedNode(visited);
                nodesStack.push(id);
            }

            if(!begun)
            {
                begun=true;
                currNode=findUnvisitedNode(visited);
                nodesStack.push(currNode);
            }

            while(!nodesStack.empty())
            {
                currNode=nodesStack.top();
                nodesStack.pop();
                visited[currNode]=true;
                treeID[currNode]=currTreeID;
                numNodesRemaining--;

                std::vector<size_t> nbrs=aList[currNode];
                for(size_t i=0;i<nbrs.size();++i)
                {
                    if(nbrs[i]==removedVtx) continue;
                    if(!visited[nbrs[i]]) nodesStack.push(nbrs[i]);
                }

            }
        }
        currTreeID++;

        std::map<size_t,size_t>::iterator trIt;

        for(size_t t=0;t<currTreeID;++t)
        {
            
        }
        */
    }

    /* method to setup subtree sizes and nodes in current tree */
    template<class graphType, class T>
    void ST2Ops<graphType,T>::DFS(
                            const size_t src, 
                            std::map<size_t,bool>& visited,
                            std::map<size_t,size_t>& subtreeSize,
                            size_t* n,
                            mapList& treeMap,   //  this is used to navigate the tree.
                            std::map<size_t,bool>& centroidMarkedMap)
    {
        /* mark node visited */
        visited[src]=true;                  //  mark the src is true.
        /* increase count of nodes visited */
        *n+=1;                              //  one more node transvered.
        /* initialize subtree size for current node*/
        subtreeSize[src]=1;                 //  Size of the current subtree is 1
        
        std::vector<size_t>::iterator it;   //  

        /* recur on non-visited and non-centroid neighbours */
        for(it=treeMap[src].begin();
            it!=treeMap[src].end(); 
            ++it)
        {
            if(!visited[*it] && !centroidMarkedMap[*it])
            {
                DFS(*it,visited,subtreeSize,n,treeMap,centroidMarkedMap);
                subtreeSize[src]+=subtreeSize[*it];
            }
        }

    }
}