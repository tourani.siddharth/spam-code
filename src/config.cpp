#include "config.h"



void printSampling(const Solver::sampType st)
{
    switch(st)
    {
        case Solver::SPANNING_TREES:
            std::cout << "Spanning Trees\n";
            break;
        
        case Solver::ARC_INCONSISTENT:
            std::cout << "Arc Inconsistent\n";
            break;
        
        case Solver::MAX_MONO_CHAINS:
            std::cout << "Maximal Monotonic Chains\n";
            break;
        
        case Solver::EDGE:
            std::cout << "Edge Decomposition\n";
            break;
        
        case Solver::INDUCED_CHAINS:
            std::cout << "Induced Chains\n";
            break;

        case Solver::ROW_COL:
            std::cout << "Row-Col Decomposition\n";
            break;

        case Solver::ROW_EDGE:
            std::cout << "Row-Edge Decomposition\n";
            break;

        case Solver::SHORTEST_PATH:
            std::cout << "Shortest Path Decomposition\n";
            break;

        default:
            break;
    }
}

file_parts ExtractFileParts(std::string filename)
{
    int idx0 = filename.rfind("/");
    int idx1 = filename.rfind(".");
    file_parts fp;
    fp.path = filename.substr(0,idx0+1);
    fp.name = filename.substr(idx0+1,idx1-idx0-1);
    fp.ext  = filename.substr(idx1);
    return fp;
};