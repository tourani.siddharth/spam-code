#pragma once
#include "config.h"

namespace MsgPassing
{
	struct vec1f
	{
	public:
		typedef float scalar;
		typedef float alignedVec;


		static int V()
		{
			return sizeof(alignedVec)/sizeof(scalar);
		}

	};

	struct vec4f
	{
	public:
		typedef float scalar;
		typedef vec4f alignedVec;
		__m128 a;
	};


	// struct vec8f
	// {
	// public:
	// 	typedef float scalar;
	// 	typedef vec4f alignedVec;
	// 	__m256 a;
	// };


	struct vec2d
	{
		public:
		typedef double scalar;
		typedef vec2d alignedVec;
		__m128 a;
	};

	// struct vec4d
	// {
	// public:
	// 	typedef double scalar;
	// 	typedef vec4d alignedVec;
	// 	__m256 a;
	// };

};