#pragma once
/*!
	@file trws.h
	@author Siddharth Tourani
	@brief The trws class.
*/

#include "msgAlgo.h"
#include "alignedAllocator.h"
#include "simd.h"
// #include "graphSampling.h"
#include "utils.h"
#include "qpbo/qpbo.h"
#include "uGraph.h"
#include "timer.h"
#include "testGraphs2.h"

struct Assignment
{
	int id;
	int left;
	int right;
	double cost;
};

struct EdgeCost
{
	int assID0;
	int assID1;
	double cost;
};

static constexpr double INFTY = 1e20;
enum GRAPH_TYPE{GRID_GRAPH,GENERAL_GRAPH};

// template<class vecType>
template<class T>
struct OpTriplet
{
	// using T=typename vecType::scalar;
	size_t e;
	size_t dir;
	T multiplier;

	OpTriplet(size_t E,size_t D,T m): e(E),dir(D),multiplier(m){}


	friend std::ostream& operator<<(std::ostream& os, const OpTriplet<T>& oT);

};

// using chain=typename std::vector<OpTriplet<class T>>;
using chain= std::vector<size_t>;

namespace Solver{
	template<class vecType>
	class trws 
	{
		using uGraph=Graph::uGraph;

		public:
			using vectorizer=typename vecType::alignedVec;
			using T=typename vecType::scalar;
			using vector=typename vecType::scalar;

		struct options
		{
			size_t options;
			Solver::sampType sampling;
			Solver::GraphType grType;
			Solver::RoundingType roundType;
			size_t numGenerationsForPrimalRounding;
			size_t numIters;
		};

		size_t computePadding(size_t l)
		{
			return ((l/sizeof(vector))+1)*sizeof(vector);
		}
		
		size_t flattenIndex(size_t r, size_t c, size_t Cols)
		{
			return r*Cols+c;
		}

		struct node
		{
			size_t K;
			size_t vK;
			T gamma;
			// T reverseGamma;
			T* data;
		};

		struct edge
		{
			size_t s;
			size_t t;

			size_t K1;
			size_t vK2;
			size_t K2;
			
			node* head;
			node* tail;

			T* data;
		};

		alignedAllocator mAllocator;
		std::vector<node> mNodes;
		std::vector<edge> mEdges;

		std::map<std::pair<size_t,size_t>,size_t> nodePair2Edges;
		// std::vector<OpTriplet<T>> mChains;
		std::vector<size_t> mLabelling;
		options mOpts;
		T primalE;
		size_t numMessagesPassed;
		size_t nV;
		size_t nE;
		dirAdjList adjList;
		std::random_device dev;
	    std::mt19937 rng;	

		//	read the file fill up the unary and pairwise terms.
		trws(const Graph::graphStorage& gu,const trws<vecType>::options& opts);
		void allocateMemory();
		void sampleGraph();
		void infer();
		void runIter();
		void computeAdjacencyList();
		void connectGraph(const Graph::graphStorage& gu);

		//	DUAL UPDATES
		void push(size_t e, size_t dir);
		void pull(size_t eID, size_t dir);

		void processNode(const size_t n_id, const size_t dir);

		T dual();


		//	DEBUG FUNCTIONS
		void printPairwise(const size_t eID);
		void printPairwise(const size_t i, const size_t j);
		void printUnary(const size_t i);
		void printGraph();

		void computeGammas();

		//	PRIMAL UPDATES
		void messagePrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom);

		void messageTPrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom);

		size_t pickRandomVertexPrimal(const std::vector<size_t>& unprocessedVertices);
		size_t  pickRandomNeighbor(size_t vtxID, std::vector<bool>& processedVertices);
		T primalGreedy();
		T primal();

		T primalFusionMoves(const size_t numGens=1);
		
		std::vector<size_t> buildAndSolveAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2);

		std::vector<size_t> computeLabellingOrdered();
		std::vector<size_t> computeLabellingGreedyRandom();
		T computeEnergy(std::vector<size_t>& labelling);

		std::vector<std::vector<T>> unaries;
		std::vector<std::vector<T>> pairwise;
	};

	template<class vecType>
	std::vector<size_t> trws<vecType>::computeLabellingGreedyRandom()
	{
		using T=typename vecType::scalar;

		std::vector<size_t> labelling(nV);	
		std::vector<bool> processedVertices(nV,false);
		std::vector<size_t> unprocessedVertices;
		std::vector<size_t> unprocessedNeighbors;
		size_t i=0;
		std::generate_n(std::inserter(unprocessedVertices,
									unprocessedVertices.begin()),
									nV,[&i](){ return i++; });
		
		//	Current vertex to process
		size_t currentVertex;

		//	Pick a vertex to start at from the set of unprocessed vertices
		currentVertex=pickRandomVertexPrimal(unprocessedVertices);
		// currentVertex=0;

		// Num Processed Vertices
		size_t numProcessedVertices=0;

		bool doneFlag=false;
		while(!doneFlag)		{

			std::vector<size_t>::iterator vIt=std::find(unprocessedNeighbors.begin(),
														unprocessedNeighbors.end(),
														currentVertex);
			if(vIt!=unprocessedNeighbors.end())
			{
				unprocessedNeighbors.erase(vIt);
			}

			std::vector<T> unaryVec;
			
			// copy unary vector for the currentVtx  into unaryVec
			std::copy(mNodes[currentVertex].data, 
					mNodes[currentVertex].data+(mNodes[currentVertex].K), 
					std::back_inserter(unaryVec));

			const dirAdjVec& nbrList=adjList[currentVertex];
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					(nbrList[i].ndID<currentVertex) ? 
						messagePrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]);
				}
				else
				{	
					vIt=std::find(unprocessedNeighbors.begin(),unprocessedNeighbors.end(),nbrList[i].ndID);
					if(vIt==unprocessedNeighbors.end())
					{
						unprocessedNeighbors.push_back(nbrList[i].ndID);
					}
				}
			}

			//	Get the min element	
			labelling[currentVertex]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			// erase the element from unprocessed vertices 
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVertex));


			//	update the processed vertices
			numProcessedVertices++;
			processedVertices[currentVertex]=true;

			size_t newVtx=sizetInf;

			// find the new vertex
			if(numProcessedVertices==nV)
			{
				doneFlag=true;
				break;
			}
			else
			{
				// newVtx=pickRandomNeighbor(currentVertex,processedVertices);
				size_t randInd=rand()%unprocessedNeighbors.size();
				newVtx=unprocessedNeighbors[randInd];

				// if no valid neighbor exists, pick a random vertex
				if(newVtx==sizetInf)	{
					// std::cout << "THIS IS THE FUCK UP\n";
					newVtx=pickRandomVertexPrimal(unprocessedVertices);	
				}
			}
			
			//  update the currentVtx 
			currentVertex=newVtx;
		}

		return labelling;
	}

	template<class vecType>
	typename vecType::scalar trws<vecType>::computeEnergy(std::vector<size_t>& labelling)
	{
		using T=typename vecType::scalar;

		T energy=0;

		for(size_t nd=0;nd<nV;++nd)
		{
			energy+=mNodes[nd].data[labelling[nd]];
		}
		for(size_t e=0;e<nE;++e)
		{
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
			size_t vK2=mEdges[e].vK2;
			energy+=mEdges[e].data[labelling[s]*vK2+labelling[t]];
		}
		return energy;
	}

	template<class vecType>
	size_t trws<vecType>::pickRandomVertexPrimal(const std::vector<size_t>& unprocessedVertices)
	{
		using T=typename vecType::scalar;
		size_t vtx=std::numeric_limits<T>::max();
		if(unprocessedVertices.size()>0)
		{
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprocessedVertices.size()-1); // distribution in range [1, 6]
			auto it = unprocessedVertices.cbegin();
    		std::advance(it,dist6(rng));
    		vtx=(*it);	
		}
		return vtx;
	}

	template<class vecType>
	size_t  trws<vecType>::pickRandomNeighbor(size_t vtxID, std::vector<bool>& processedVertices)
	{
		size_t pickedNbr=sizetInf;

		/*!	get the adjacency list of the graph */
		const adjacencyVector& adVec=adjList[vtxID];

		/*!	store the processed neighbors in a vector */
		std::vector<size_t> unprNbrs;
		for(size_t i=0;i<adVec.size();++i)
		{
			if(!processedVertices[adVec[i].ndID])	{
				unprNbrs.push_back(adVec[i].ndID);
			}
		}
		
		// std::cout << "UnprocessedNeighbors: ";
		// printVectorBasic(unprNbrs);

		/*! pick a random neighbor from the vector of processed neighbors	*/
		if(unprNbrs.size()>0)
		{
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprNbrs.size()-1); // distribution in range [1, 6]
			auto it = unprNbrs.cbegin();
    		std::advance(it,dist6(rng));
    		pickedNbr=(*it);	
		}
		return pickedNbr;
	}


	template<class vecType>
	void trws<vecType>::messagePrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  const size_t eID, 
										const size_t winLabelFrom)
	{
		assert(mEdges[eID].t==unID);

		/*!	K is the length of the unary */
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].vK;
		size_t vK2=mEdges[eID].vK2;

		/*!			*/
		for(size_t j=0;j<K;++j)		{
			un[j]+=(mEdges[eID].data[winLabelFrom*vK+j]);		
		}
	}

	template<class vecType>
	void trws<vecType>::messageTPrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom)
	{
		assert(mEdges[eID].s==unID);

		//	K is the length of the unary
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].vK;
		size_t vK2=mEdges[eID].vK2;


		for(size_t j=0;j<K;++j)
		{
			un[j]+=mEdges[eID].data[j*vK2+winLabelFrom];
		}
	}


	template<class vecType>
	std::vector<size_t> trws<vecType>::buildAndSolveAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2)
	{
		using T=typename vecType::scalar;
		qpbo::QPBO<T> q(nV,nE);
		q.AddNode(nV);

		for(size_t i=0;i<nV;++i)
		{	
			T C0=mNodes[i].data[labelling1[i]];
			T C1=mNodes[i].data[labelling2[i]];
			if(labelling1[i]!=labelling2[i])
			{
				q.AddUnaryTerm(i,C0,C1);
			}
			else
			{
				q.AddUnaryTerm(i,C0,INFTY);
			}
			
		}
		
		for(size_t e=0;e<nE;++e)
		{
			auto& mE=mEdges[e];
			size_t vK2=mE.vK2;
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
			size_t l1s=labelling1[s];
			size_t l1t=labelling1[t];
			size_t l2s=labelling2[s];
			size_t l2t=labelling2[t];
			T C00=mE.data[l1s*vK2+l1t];
			T C01=mE.data[l1s*vK2+l2t];
			T C10=mE.data[l2s*vK2+l1t];
			T C11=mE.data[l2s*vK2+l2t];
			q.AddPairwiseTerm(s,t,C00,C01,C10,C11);
		}

		q.MergeParallelEdges();
		q.Solve();
		// q.ComputeWeakPersistencies();

		std::vector<size_t> labelling(nV,0);
		for(size_t i=0;i<nV;++i){
			size_t v=q.GetLabel(i);
			if(v==0)
			{
				labelling[i]=labelling1[i];
			}
			else
			{
				labelling[i]=labelling2[i];
			}	
		}	
		return labelling;
	}



	template<class vecType>
	typename vecType::scalar trws<vecType>::primalFusionMoves(const size_t numGenerations)
	{
		using T=typename vecType::scalar;

		//	For each of the num generations. 
		//	Build the auxiliary problem
		//	Solve the QPBO Problem
		//	Set the labelling.
		// auto l1=computeLabellingGreedyRandom();
		std::vector<size_t> l1=mLabelling;
		T bestEnergy=computeEnergy(l1);

		for(size_t i=0;i<numGenerations;++i)
		{
			auto l2=computeLabellingGreedyRandom();
			l1=buildAndSolveAuxiliaryProblem(l1,l2);

			T currEnergy=computeEnergy(l1);
			if(bestEnergy>currEnergy)
			{
                mLabelling=l1;
				bestEnergy=currEnergy;
			}
		}
		
		return bestEnergy;
	}

	template<class vecType>
	std::vector<size_t> trws<vecType>::computeLabellingOrdered()
	{
		using T=typename vecType::scalar;
		std::vector<size_t> labelling(nV);
		std::vector<bool> processedVertices(nV,false);
		std::vector<size_t> unprocessedVertices;
		size_t i=0;
		std::generate_n(std::inserter(unprocessedVertices,
						unprocessedVertices.begin()),nV,[&i](){ return i++; });
		
		//	Keeps a list of prospective neighbors to select from
		std::vector<size_t> unprocessedNeighbors;

		//	Current vertex to process
		size_t currentVertex;

		//	Pick a vertex to start at from the set of unprocessed vertices
		currentVertex=0;

		// Num Processed Vertices
		size_t numProcessedVertices=0;


		bool doneFlag=false;
		while(!doneFlag)		
		{

			// std::cout << "CURRENT VERTEX: " << currentVertex << "\n";

			std::vector<size_t>::iterator vIt=std::find(unprocessedNeighbors.begin(),
														unprocessedNeighbors.end(),
														currentVertex);
			if(vIt!=unprocessedNeighbors.end())
			{
				unprocessedNeighbors.erase(vIt);
			}

			std::vector<T> unaryVec;
			
			// copy unary vector for the currentVtx  into unaryVec
			std::copy(mNodes[currentVertex].data, 
					mNodes[currentVertex].data+(mNodes[currentVertex].K), 
					std::back_inserter(unaryVec));

			// std::cout << "BEFORE: ";
			// printVectorBasic(unaryVec);

			const dirAdjVec& nbrList=adjList[currentVertex];
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					// std::cout << "Passing message from "<<  nbrList[i].ndID  << " to: " << currentVertex << "\n";
					(nbrList[i].ndID<currentVertex) ? 
						messagePrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]);
				}
				else
				{
					vIt=std::find(unprocessedNeighbors.begin(),unprocessedNeighbors.end(),nbrList[i].ndID);
					if(vIt==unprocessedNeighbors.end())
					{
						unprocessedNeighbors.push_back(nbrList[i].ndID);
					}
				}
				
			}

			// std::cout << "AFTER: ";
			// printVectorBasic(unaryVec);

			//	Get the min element	
			labelling[currentVertex]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			// std::cout << "min labelling: " << labelling[currentVertex] << "\n";

			// erase the element from unprocessed vertices 
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVertex));

			//	update the processed vertices
			numProcessedVertices++;
			processedVertices[currentVertex]=true;

			size_t newVtx=sizetInf;

			// std::cout << "numProcessedVertices: " << numProcessedVertices << "\n";

			// find the new vertex
			if(numProcessedVertices==nV)
			{
				doneFlag=true;
				break;
			}
			else
			{
				// newVtx=pickRandomNeighbor(currentVertex,processedVertices);

				size_t randInd=rand()%unprocessedNeighbors.size();
				newVtx=unprocessedNeighbors[randInd];

				// std::cout << "newVtx: " << newVtx << "\n";
				// if no valid neighbor exists, pick a random vertex
				if(newVtx==sizetInf)	{
					// std::cout << "THIS IS FUCKED UP\n";
					newVtx=pickRandomVertexPrimal(unprocessedVertices);	
				}
			}		

			//  update the currentVtx 
			currentVertex=newVtx;
			// getchar();
		}


		return labelling;
	}

	template<class vecType>
	void trws<vecType>::printGraph()
	{
		size_t ii=0;
		for(auto& nd:mNodes)
		{
			std::cout << "UNARY " << ii<< ": ";
			for(size_t k=0;k<nd.K;++k)
			{
				std::cout << nd.data[k] << " ";
			}
			std::cout << "\n";
			ii++;
		}
	}
	
	template<class vecType>
	void trws<vecType>::printUnary(const size_t i)
	{
		for(size_t j=0;j<mNodes[i].K;++j)
		{
			std::cout << mNodes[i].data[j] << " ";
		}
		std::cout << "\n";
	}

	template<class vecType>
	void trws<vecType>::printPairwise(const size_t eID)
	{
		size_t K1=mEdges[eID].K1;
		size_t K2=mEdges[eID].K2;
		size_t vK2=mEdges[eID].vK2;
		std::cout << "Printing Pairwise: " << mEdges[eID].s << " -- " << mEdges[eID].t << "\n";
		for(size_t i=0;i<K1;++i)
		{
			for(size_t j=0;j<K2;++j)
			{
				std::cout << mEdges[eID].data[i*vK2+j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	template<class vecType>
	void trws<vecType>::printPairwise(const size_t i,const size_t j)
	{
		size_t eID=nodePair2Edges[std::make_pair(i,j)];
		std::cout << "Printing Pairwise: " << mEdges[eID].s << " -- " << mEdges[eID].t << "\n";
		size_t K1=mEdges[eID].K1;
		size_t K2=mEdges[eID].K2;
		size_t vK2=mEdges[eID].vK2;
		for(size_t i=0;i<K1;++i)
		{
			for(size_t j=0;j<K2;++j)
			{
				std::cout << mEdges[eID].data[i*vK2+j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	template<class vecType>
	typename vecType::scalar trws<vecType>::primal()
	{
		using T=typename vecType::scalar;
		/*!	Fill up the labelling.	*/
		auto mLabelling2=computeLabellingOrdered();
		// mLabelling=computeLabellingGreedyRandom();
        T energy1=computeEnergy(mLabelling);
		T energy2=computeEnergy(mLabelling2);
        if(energy2<energy1)
        {
            mLabelling=mLabelling2;
            energy1=energy2;
        }
		return energy1;
	}

	template<class vecType>
	void trws<vecType>::infer()
	{
		using T=typename vecType::scalar;
		// mLabelling=computeLabellingGreedyRandom();
		mLabelling=computeLabellingOrdered();
		T P=computeEnergy(mLabelling);
		for(size_t i=1;i<=mOpts.numIters;++i)
        {
            runIter();

            if(mOpts.roundType==Solver::FUSIONMOVES)
            {
                P=primalFusionMoves();
                
            }
            else if(mOpts.roundType==Solver::GREEDYBEST)
            {
                P=primal();
            }
            T D=dual();
            
            std::cout << numMessagesPassed << " " << D << " " << P <<  "\n";
        }
	}

	template<class vecType>
	void trws<vecType>::runIter()
	{ 	
		using T=typename vecType::scalar;
        T P, D;
		for(int i=0;i<mNodes.size();++i)
		{
            processNode(i,0);   
		}
        for(int i=mNodes.size()-1;i>=0;i--)
        {
            // std::cout << BOLDRED << "AT NODE: " << i << "\n" << RESET;
            processNode(i,1);
        }
	}

	template<class vecType>
	typename vecType::scalar trws<vecType>::dual()
	{
		std::vector<T> dualVec(nV);
		T dualE=0;
		for(auto& nd: mNodes)
		{
			T val=nd.data[0];
			for(size_t l=1;l<nd.K;++l)
			{
				if(val>nd.data[l]){
					val=nd.data[l];
				}
			}
			dualE+=val;
		}
		return dualE;
	}
	
	template<class vecType>
	trws<vecType>::trws(const Graph::graphStorage& gu,const trws<vecType>::options& opts):mOpts(opts),
																			mLabelling(gu.unaries.size()),
																			nV(gu.unaries.size()),
																			nE(gu.pairwise.size()),
																			numMessagesPassed(0)																	
	{
		Timer timer;

		nV=gu.unaries.size();
		nE=gu.pairwise.size();
		mNodes.resize(nV);
		mEdges.resize(nE);
		// pairwise.resize(nE);
		unaries=gu.unaries;
		pairwise=gu.pairwise;

		for(size_t n=0;n<unaries.size();++n)
		{
			mNodes[n].vK=gu.unaries[n].size();
			mNodes[n].K=gu.unaries[n].size();
		}

		connectGraph(gu);
		allocateMemory();
		rng=std::mt19937(dev());
		computeAdjacencyList();
		computeGammas();

	}


	template<class vecType>
	void trws<vecType>::connectGraph(const Graph::graphStorage& gu)
	{
		std::map<std::pair<size_t,size_t>,size_t> nodePair2OriginalEdgeID;
		for(size_t e=0;e<gu.G.m_E.size();++e)
		{
			size_t s=gu.G.m_E[e].s;
			size_t t=gu.G.m_E[e].t;
			nodePair2OriginalEdgeID[std::make_pair(s,t)]=e;
			mEdges[e].s=s;
			mEdges[e].t=t;
			mEdges[e].K1=gu.pwSz[e].l1;
			mEdges[e].K2=gu.pwSz[e].l2;
			mEdges[e].vK2=gu.pwSz[e].l2;
		}
			
	}


	template<class vecType>
	void trws<vecType>::computeAdjacencyList()
	{
		adjList.resize(nV);
		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
		    dirEdgeHalf eH1;
            eH1.eID=e;  eH1.ndID=t;  eH1.dir=0;
		    dirEdgeHalf eH2;
            eH2.eID=e;  eH2.ndID=s;	eH2.dir=1;
			adjList[s].push_back(eH1);
			adjList[t].push_back(eH2);

		}
	}


	template<class vecType>
	void trws<vecType>::computeGammas()
	{
		using T=typename vecType::scalar;
		for(size_t i=0;i<adjList.size();++i)
		{
			int numFwd=0;
			int numBwd=0;

			for(size_t j=0;j<adjList[i].size();++j)
			{
				if(adjList[i][j].dir==0)
				{
					numFwd++;
				}
				else
				{
					numBwd++;
				}
			}
			int ni = (numFwd > numBwd) ? numFwd : numBwd;
			mNodes[i].gamma=((T)1/ni);
		}
	}
	
	template<class vecType>
	void trws<vecType>::processNode(const size_t n_id, const size_t dir)
	{
		assert(n_id>=0 && n_id<adjList.size());
		node& nd=mNodes[n_id];
		T gamma=nd.gamma;

		//	Compute the pushVector, to be pushed to every neighboring node in a certain direction.
        //  First copy the node vector, then multiply each element with gamma.
		std::vector<T> pushVec;
		std::copy(&(mNodes[n_id].data[0]),&(mNodes[n_id].data[mNodes[n_id].K]),std::back_inserter(pushVec));
		std::transform(pushVec.begin(), pushVec.end(), pushVec.begin(), [gamma](auto& c){return c*gamma;});

		//	Pass messages along forward or reverse edges.
		const dirAdjVec& adjVecN=adjList[n_id];
		for(size_t i=0;i<adjVecN.size();++i)
		{

            if(dir==0)
            {
                if( (dir==adjVecN[i].dir) )
                {
                    numMessagesPassed++;
                    size_t eID=adjVecN[i].eID;
                    edge& e=mEdges[eID];
                    size_t K1=e.K1;
                    size_t K2=e.K2;
                    size_t vK2=e.vK2;
                    assert(pushVec.size()==K1);

                    for(size_t j=0;j<K1;++j)
                    {
                        for(size_t k=0;k<K2;++k)
                        {
                            e.data[j*vK2+k]+=pushVec[j];
                        }
                        nd.data[j]-=pushVec[j];
                    }

                    node* toNode=e.head;

                    for(size_t k=0;k<K2;++k)
                    {
                        //	Compute the min value per coordinate
                        T val=e.data[0*vK2+k];
                        for(size_t j=1;j<K1;++j)	
                        {
                            if(val>e.data[j*vK2+k])
                            {
                                val=e.data[j*vK2+k];
                            }
                        }

                        //	Add to the node
                        toNode->data[k]+=val;

                        for(size_t j=0;j<K1;++j)	
                        {
                            e.data[j*vK2+k]-=val;
                        }
                    }
                }
                // else{printf("NO MESSAGE PASSED AS EDGE IS IN WRONG DIRECTION\n");}
            }
            else if(dir==1)
            {
                if( (dir==adjVecN[i].dir) )
			    {
                    numMessagesPassed++;
                    size_t eID=adjVecN[i].eID;
                    edge& e=mEdges[eID];
                    size_t K1=e.K1;
                    size_t K2=e.K2;
                    size_t vK2=e.vK2;
                    assert(pushVec.size()==K2);

                    //  Push from the head into the edge.
                    for(size_t k=0;k<K2;++k)
                    {
                        for(size_t j=0;j<K1;++j)
                        {
                            e.data[j*vK2+k]+=pushVec[k];
                        }
                        nd.data[k]-=pushVec[k];
                    }

                    node* frNode=e.tail;

                    //  Pull from the edge into the head.
                    for(size_t j=0;j<K1;++j)
                    {
                        T val=e.data[j*vK2+0];
                        //	Compute the min value per element
                        for(size_t k=1;k<K2;++k)
                        {
                            if(val>e.data[j*vK2+k])
                            {
                                val=e.data[j*vK2+k];
                            }
                        }

                        //	Add to the node
                        frNode->data[j]+=val;
                        for(size_t k=0;k<K2;++k)
                        {
                            e.data[j*vK2+k]-=val;
                        }
                    }
                }
                // else{printf("NO MESSAGE PASSED AS EDGE IS IN WRONG DIRECTION\n");}
            }
			//	(dir==0) If we are pushing along forward edges 
			//  check if (dir==adjVecN[i].dir) relative to the current 
			//	 node, the edge is indeed a forward edge.
			
			//	(dir==1) If we are pushing along backward edges 
			//  (dir==adjVecN[i].dir) relative to the current node, 
			//	the edge is indeed a backward edge.
			

            // std::cout << "NODE[" << n_id << "]: ";	printUnary(n_id);
			// std::cout << "NODE[" << adjVecN[i].ndID << "]: ";	printUnary(adjVecN[i].ndID);
			// std::cout << "EDGE[" << adjVecN[i].eID << "]\n";
			// printPairwise(adjVecN[i].eID);
            // std::cout << "---------------------------------------------------------------------------------------\n";
            // getchar();
		}

	}


	template<class vecType>
	void trws<vecType>::push(size_t eID, size_t dir)
	{
		/*
		
		
		if(dir==0)
		{
			node* nd=e.tail;

			for(size_t i=0;i<K1;++i)
			{
				for(size_t j=0;j<K2;++j)
				{
					e.data[i*vK2+j]+=;
				}
				nd->data[i]-=(nd->gamma)*(nd->data[i]);
			}
		}
		else
		{
			node* nd=e.head;
			for(size_t j=0;j<K2;++j)
			{
				for(size_t i=0;i<K1;++i)
				{
					e.data[i*vK2+j]+=(nd->gamma)*(nd->data[j]);
				}
				nd->data[j]-=(nd->gamma)*(nd->data[j]);
			}
		}
		*/
	}

	template<class vecType>
	void trws<vecType>::pull(size_t eID, size_t dir)
	{
		edge& e=mEdges[eID];
		size_t K1=e.K1;
		size_t K2=e.K2;
		size_t vK2=e.vK2;
		if(dir==0)
		{
			node* nd=e.head;
			for(size_t j=0;j<K2;++j)
			{
				T val=e.data[0*vK2+j];
				for(size_t i=1;i<K1;++i)
				{
					if(val>e.data[i*vK2+j]){
						val=e.data[i*vK2+j];
					}
				}
				nd->data[j]+=val;
				for(size_t i=0;i<K1;++i)
				{
					e.data[i*vK2+j]-=val;
				}
			}
		}
		else
		{
			node* nd=e.tail;
			for(size_t i=0;i<K1;++i)
			{
				T val=e.data[i*vK2+0];
				for(size_t j=1;j<K2;++j)	
				{
					if(val>e.data[i*vK2+j]){
						val=e.data[i*vK2+j];
					}
				}
				nd->data[i]+=val;
				for(size_t j=0;j<K2;++j)	
				{
					e.data[i*vK2+j]-=val;
				}
			}
		}
	}


	template<class vecType>
	void trws<vecType>::allocateMemory()
	{
		using T=typename vecType::scalar;
		using vectorizer=typename vecType::alignedVec;
		size_t V=sizeof(vectorizer)/sizeof(T);

		for(auto& un:unaries)
		{	
			size_t K=un.size();
			size_t vK=(K+V-1)/V;
			mAllocator.allocate_a<vectorizer>(vK);
		}

		assert(mEdges.size()==pairwise.size());
		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t l1=mEdges[e].K1;
			size_t l2=mEdges[e].K2;
			int vK=l1*((V+l2-1)/V);		//	size in vectorizers.
			// std::cout << "vK: " << vK << "\n";
			mAllocator.allocate_a<vectorizer>(vK);
		}

		mAllocator.init();

		for(size_t v=0;v<mNodes.size();++v)
		{	
			size_t K=unaries[v].size();
			size_t vK=(K+V-1)/V;
			mNodes[v].data=mAllocator.allocate_a<T>(vK);
			mNodes[v].K=K;
			//	Copy into the 
			std::copy (unaries[v].begin(), unaries[v].end(), mNodes[v].data );
		}

		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t K1=mEdges[e].K1;
			size_t K2=mEdges[e].K2;
			size_t vK=((V+K2-1)/V);
			mEdges[e].data=mAllocator.allocate_a<T>(K1*vK);
			// mEdges[e].head=mNodes[mEdges[e].s].data;
			// mEdges[e].tail=mNodes[mEdges[e].t].data;
			mEdges[e].head=&mNodes[mEdges[e].t];
			mEdges[e].tail=&mNodes[mEdges[e].s];

			//	Copy into the 
			std::copy(pairwise[e].begin(),pairwise[e].end(),mEdges[e].data);
		}

		unaries.clear();
		pairwise.clear();
	}

}
