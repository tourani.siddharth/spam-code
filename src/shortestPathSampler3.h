#pragma once
/*!
    @brief An animated embedding. 
    @author 
    @date
*/

#include "config.h"
#include "chain.h"

namespace Solver
{

    bool comparator(std::pair<size_t,size_t>& pr1, std::pair<size_t,size_t>& pr2)
    {
        return pr1.first>pr2.first;
    }

    template<class graphType, class vecType>
    class shortestPathSampler3
    {
        public:
            shortestPathSampler3(graphType& gr);
            std::vector<chain<vecType>> sampleGraph();
            std::map<size_t,std::vector<edgeHalf>> createAdjMap();
            size_t getNextID(const size_t id, std::vector<int>& numNbrsOfNodes);
            void BFS(size_t currentID,std::vector<int>& distances, std::vector<int>& numPaths,
                            std::vector<int>& previousNodes, std::vector<bool>& visited);

            std::vector<chain<vecType>> computeChains(size_t currentID,       
                                        std::vector<int>& distances,
                                        std::vector<int>& previousNodes,
                                        std::vector<int>& numPaths,
                                        std::vector<bool>& nodesDone,
                                        std::vector<bool>& edgesDone,
                                        std::vector<int>& numNbrsOfNodes, 
                                        size_t* numEdgesRemaining);

            std::vector<chain<vecType>> computeChains2(size_t currentID,
                                        std::vector<int>& distances,
                                        std::vector<int>& previousNodes,
                                        std::vector<int>& numPaths,
                                        std::vector<bool>& nodesDone,
                                        std::vector<bool>& edgesDone,
                                        std::vector<int>& numNbrsOfNodes,
                                        size_t* numEdgesRemaining);

            size_t findStartNode(std::vector<int>& numNbrsOfNode, const size_t curID);
            edgeHalf findEdge(const size_t nodeFrom, const size_t nodeTo);

            std::vector<size_t> distancesForChainComputation;            
            std::vector<size_t> nodeIDsForChainComputation;

            std::map<size_t,std::vector<edgeHalf>> adjMap;
            std::shared_ptr<graphType> grPtr;
    };

    template<class graphType, class vecType>
    shortestPathSampler3<graphType,vecType>::shortestPathSampler3(graphType& gr)
    {
        grPtr=std::make_shared<graphType>(gr);
        adjMap=createAdjMap();
    }   

    template<class graphType, class vecType>
    std::vector<chain<vecType>> shortestPathSampler3<graphType,vecType>::sampleGraph()
    {
        std::vector<chain<vecType>> chains;                     //  
        bool graphSamplingComplete=false;                       //  
        bool nodeFresh=false;                                   //  

        //  These have to be refreshed every BFS.
        std::vector<int> distances(grPtr->m_nV,intInf);         //  
        std::vector<int> previousNodes(grPtr->m_nV,-1);         //  
        std::vector<int> numPaths(grPtr->m_nV,0);               //  
        std::vector<bool> visited(grPtr->m_nV,false);

        distancesForChainComputation=std::vector<size_t>(grPtr->m_nV,uintInf);        
        nodeIDsForChainComputation=std::vector<size_t>(grPtr->m_nV,uintInf);

        //  To indicate if a node is blocked or not.
        std::vector<bool> blockedNodes(grPtr->m_nV,false);              //  
        std::vector<int>  numNbrsOfNodes(grPtr->m_nV,0);                //  

        for(size_t i=0;i<numNbrsOfNodes.size();++i)                     //  
        {                                                               //  
            numNbrsOfNodes[i]=grPtr->getNbrsToNode(i).size();           //  
        }                                                               //  

        std::vector<bool> nodesDone(grPtr->m_nV,false);                 //  
        std::vector<bool> edgesDone(grPtr->m_nE,false);                 //  

        //  With each ID, you redo the BFS 
        size_t currentID=1;                                             //  
        size_t numEdgesRemaining=grPtr->m_nE;                           //  
        // std::cout << "numEdgesRemaining: " << numEdgesRemaining << "\n";//  

        bool allEdgesDone=false;
        bool start=true;

        size_t R=4;
        size_t C=5;

        size_t iter=1;
        while(!allEdgesDone)
        {  
            // std::cout << "Iteration " << iter << "\n";
            if(!start)    
            {
                currentID=findStartNode(numNbrsOfNodes,currentID);        //  
            }
            else
            {
                currentID=0;    start=false;
            }

            distances.assign(distances.size(),intInf);
            numPaths.assign(numPaths.size(),0);
            visited.assign(visited.size(),false);
            previousNodes.assign(previousNodes.size(),-1);

/*
            std::cout << "Before BFS Distances\n";
            printArrayInGrid(distances,R,C);
            std::cout << "\n";

            std::cout << "Before BFS numPaths\n";
            printArrayInGrid(numPaths,R,C);
            std::cout << "\n";

            std::cout << "Before previousNodes\n";
            printArrayInGrid(previousNodes,R,C);
            std::cout << "\n";
*/
            
            BFS(currentID,distances,numPaths,previousNodes,visited);        //  

/*
            std::cout << "Distances from BFS: \n";
            printArrayInGrid(distances,R,C);
            std::cout << "\n";
            
            std::cout << "numPaths: \n";
            printArrayInGrid(numPaths,R,C);
            std::cout << "\n";

            std::cout << "After previousNodes\n";
            printArrayInGrid(previousNodes,R,C);
            std::cout << "\n";
*/

            // std::cout << "numNbrs of " << currentID << ": " << numNbrsOfNodes[currentID] << "\n\n";
            // std::cout << "Before numEdgesRemaining: " << numEdgesRemaining << "\n\n";

            std::vector<chain<vecType>> newChains=computeChains(currentID,       
                                                distances, previousNodes,
                                                numPaths, nodesDone, edgesDone,
                                                numNbrsOfNodes, &numEdgesRemaining);

            if(newChains.size()>0)
            {
                chains.insert(std::end(chains), std::begin(newChains), std::end(newChains));
            }

            iter++;

            if(numEdgesRemaining==0)    allEdgesDone=true;
        }

/*
        std::cout << BOLDRED << "\nAFTER CHAIN 2\n" << RESET;
        for(size_t i=0;i<grPtr->m_nV;++i)
        {
            if (i%4==0) std::cout << "\n";
            std::cout << "num nbrs of node[" << i << "]: " << numNbrsOfNodes[i] << "\n";    
        }
        
        for(size_t i=0;i<chains.size();++i)
        {
            std::cout << "Chain  NODE" << i << ": ";
            for(size_t j=0;j<chains[i].nodes.size();++j)
            {
                std::cout << chains[i].nodes[j].id << " ";
            }
            std::cout << "  | ";
            std::cout << "Chain EDGE: ";
            for(size_t j=0;j<chains[i].edges.size();++j)
            {
                std::cout << chains[i].edges[j].id << " ";
            }
            std::cout << " | ";
            std::cout << "HEAD " << chains[i].head << "  TAIL " << chains[i].tail;  
            std::cout << "\n";
        }
*/       

        return chains;
    }

    template<class graphType, class vecType>
    std::vector<chain<vecType>> 
    shortestPathSampler3<graphType,vecType>::computeChains2(size_t currentID,
                                                            std::vector<int>& distances,
                                                            std::vector<int>& previousNodes,
                                                            std::vector<int>& numPaths,
                                                            std::vector<bool>& nodesDone,
                                                            std::vector<bool>& edgesDone,
                                                            std::vector<int>& numNbrsOfNodes,
                                                            size_t* numEdgesRemaining)
    {
        std::vector<chain<vecType>> chains;
        std::vector<std::pair<size_t,size_t>> NodeID2Distance;

        line();
        std::cout << BOLDRED << "Inside Compute Chains\n" << RESET;

        //  compute the greatest distance with numPaths==1
        for(size_t i=0;i<numPaths.size();++i)
        {
            //  what criteria can that be?
            if(i==currentID)    continue;
            if((numPaths[i]==1)&&(numNbrsOfNodes[i]>0))
            {
                std::pair<size_t,size_t> pr(i,distances[i]);
                NodeID2Distance.push_back(pr);
            }
        }
        
        // std::cout << "NodeID2Distance\n";
        // for(size_t i=0;i<NodeID2Distance.size();++i)
        // {
        //     std::cout << "(" << NodeID2Distance[i].first << " " << NodeID2Distance[i].second << ")\n";
        // }

        std::sort(NodeID2Distance.begin(),NodeID2Distance.end(),comparator);

        std::cout << "Before NodeID2Distance\n";
        for(size_t i=0;i<NodeID2Distance.size();++i)
        {
            std::cout << "(" << NodeID2Distance[i].first << " " << NodeID2Distance[i].second << ")\n";
        }


        // while(NodeID2Distance.size()>0)
        {
            std::map<size_t,size_t>::iterator mapIt;

            //  Get the chain at the greatest distance
            auto x = std::max_element(NodeID2Distance.begin(), NodeID2Distance.end(),
                    [](const std::pair<size_t,size_t>& p1, const std::pair<size_t, size_t>& p2) 
                        {return p1.second < p2.second; });

            size_t winID=x->first;
            size_t winDistance=x->second;

            std::cout << "winID: " << winID << " winDistance: " << winDistance << "\n";

            //  Allocate memory for the chain
            std::vector<size_t> chainNodes(winDistance+1);
            std::vector<size_t> chainEdges(winDistance);
            
            size_t ndID=winID;              //   
            size_t chNdID=winDistance;      //  

            chainNodes[chNdID]=winID;       //  
            size_t otherNd;                 //  other node

            //  Sometimes chains overlapping with other chains 
            //  may be constructed. This boolean variable detects this.
            //  If this is the case, the chain is not added to chains, but
            //  the elements are removed from map 

             bool edgeCoveredInPreviousChain=false;
            //  The chain is being constructed here.
            while(previousNodes[ndID]!=-1)
            {
                chNdID--;
                edgeHalf eH;
                {
                if(previousNodes[ndID]<ndID)
                {
                    otherNd=previousNodes[ndID];
                    eH=findEdge(otherNd,ndID);
                }
                else
                    otherNd=ndID;
                    eH=findEdge(ndID,otherNd);
                }

                if(edgesDone[eH.eID]==true)
                {
                    edgeCoveredInPreviousChain=true;
                }

                chainEdges[chNdID]=eH.eID;
                ndID=previousNodes[ndID];

                //  maybe check here if 
                chainNodes[chNdID]=ndID;
            }

            std::cout << "CHAIN NODES ";
            for(size_t i=0;i<chainNodes.size();++i)
            {
                std::cout << chainNodes[i] << " ";
            }
            std::cout << "\n";

            //  erase all elements present in chainNodes from the map.
            std::cout << "Before NodeID2Distance size(): " << NodeID2Distance.size() << "\n";
            for(size_t i=1;i<chainNodes.size();++i)
            {
                size_t winID=std::numeric_limits<size_t>::infinity();
                for(size_t j=0;j<NodeID2Distance.size();++j)
                {
                    if(NodeID2Distance[j].first==chainNodes[i])
                    {
                        winID=j;        //  correct this.
                    }
                    std::cout << "winID: " << winID << "\n";
                    if(winID!=std::numeric_limits<size_t>::infinity())
                    {
                        NodeID2Distance.erase(NodeID2Distance.begin(),NodeID2Distance.begin()+winID);
                    }
                }
            }
            std::cout << "After NodeID2Distance size(): " << NodeID2Distance.size() << "\n";


            std::cout << "After\n";
            std::cout << "NodeID2Distance\n";
            for(size_t i=0;i<NodeID2Distance.size();++i)
            {
                std::cout << "(" << NodeID2Distance[i].first << " " << NodeID2Distance[i].second << ")\n";
            }
    

            if(!edgeCoveredInPreviousChain)
            {
                for(size_t i=0;i<chainNodes.size();++i) 
                {
                    // numNbrsOfNodes[chainNodes[i]]--;        //  update numNbrsOfNodes
                    nodesDone[chainNodes[i]]=true;          //  update nodesDone. Do we need this? 
                }

                //  update edgesDone.
                for(size_t e=0;e<chainEdges.size();++e) 
                {
                    edgesDone[chainEdges[e]]=true;
                    size_t fr=grPtr->m_E[chainEdges[e]].s;
                    size_t to=grPtr->m_E[chainEdges[e]].t;
                    numNbrsOfNodes[fr]--;
                    numNbrsOfNodes[to]--;
                }
                (*numEdgesRemaining)=(*numEdgesRemaining)-chainEdges.size();
                //  
                chain<vecType> cH(chainNodes,chainEdges);
                cH.fixChain();
                chains.push_back(cH);
            }
            getchar();
        }

        line();

        return chains;
    }

    /*!
        By the end of this particular episode, we ought to have 
        numNbrs[currentID]=0.   Without much fuss.
    */
    template<class graphType,class vecType>
    std::vector<chain<vecType>> 
    shortestPathSampler3<graphType,vecType>::computeChains(size_t currentID,       
                                                        std::vector<int>& distances,
                                                        std::vector<int>& previousNodes,
                                                        std::vector<int>& numPaths,
                                                        std::vector<bool>& nodesDone,
                                                        std::vector<bool>& edgesDone,
                                                        std::vector<int>& numNbrsOfNodes, 
                                                        size_t* numEdgesRemaining)
    {
        std::vector<chain<vecType>> chains;
        std::vector<std::pair<size_t,size_t>> uniqueDistanceNodeIDPrs;
        
        //  first ID (Key) is the node ID. second ID is distance.
        std::map<size_t,size_t> NodeID2Distance;

        //  compute the greatest distance with numPaths==1
        for(size_t i=0;i<numPaths.size();++i)
        {
            if(i==currentID)    continue;
            if((numPaths[i]==1)&&(numNbrsOfNodes[i]>0))
            {
                std::pair<size_t,size_t> pr(distances[i],i);
                uniqueDistanceNodeIDPrs.push_back(pr);
                NodeID2Distance[i]=distances[i];
            }
        }
        
        std::sort(uniqueDistanceNodeIDPrs.begin(),uniqueDistanceNodeIDPrs.end(),comparator);

        while(NodeID2Distance.size()>0)
        {
            std::map<size_t,size_t>::iterator mapIt;

            //  Get the chain at the greatest distance
            auto x = std::max_element(NodeID2Distance.begin(), NodeID2Distance.end(),
                    [](const std::pair<size_t,size_t>& p1, const std::pair<size_t, size_t>& p2) 
                        {return p1.second < p2.second; });

            // std::cout << "winID: " << x->first  << " winDistance " << x->second << "\n";

            size_t winID=x->first;
            size_t winDistance=x->second;

            //  Allocate memory for the chain
            std::vector<size_t> chainNodes(winDistance+1);
            std::vector<size_t> chainEdges(winDistance);

            size_t ndID=winID;
            size_t chNdID=winDistance;

            chainNodes[chNdID]=winID;  
            size_t otherNd;             //  other node

            //  Sometimes chains overlapping with other chains 
            //  may be constructed. This boolean variable detects this.
            //  If this is the case, the chain is not added to chains, but
            //  the elements are removed from map

            bool edgeCoveredInPreviousChain=false;
            //  The chain is being constructed here.
            while(previousNodes[ndID]!=-1)
            {
                chNdID--;
                edgeHalf eH;

                if(previousNodes[ndID]<ndID)
                {
                    otherNd=previousNodes[ndID];
                    eH=findEdge(otherNd,ndID);
                }
                else
                {
                    otherNd=ndID;
                    eH=findEdge(ndID,otherNd);
                }

                if(edgesDone[eH.eID]==true)
                {
                    edgeCoveredInPreviousChain=true;
                }

                chainEdges[chNdID]=eH.eID;
                ndID=previousNodes[ndID];

                chainNodes[chNdID]=ndID;
            }

            //  erase all elements present in chainNodes from the map.
            for(size_t i=1;i<chainNodes.size();++i)
            {
                if(NodeID2Distance[chainNodes[i]])
                    NodeID2Distance.erase(chainNodes[i]);
            }

            //  Mark the nodesDone, edgesDone, 
            //  decrement the numNbrsOfNode
            //  create the chain, add it to the chains Vector.
            if(!edgeCoveredInPreviousChain)
            {
                for(size_t i=0;i<chainNodes.size();++i) 
                {
                    // numNbrsOfNodes[chainNodes[i]]--;        //  update numNbrsOfNodes
                    nodesDone[chainNodes[i]]=true;          //  update nodesDone. Do we need this? 
                }

                //  update edgesDone.
                for(size_t e=0;e<chainEdges.size();++e) 
                {
                    edgesDone[chainEdges[e]]=true;
                    size_t fr=grPtr->m_E[chainEdges[e]].s;
                    size_t to=grPtr->m_E[chainEdges[e]].t;
                    numNbrsOfNodes[fr]--;
                    numNbrsOfNodes[to]--;
                }
                (*numEdgesRemaining)=(*numEdgesRemaining)-chainEdges.size();
                //  
                chain<vecType> cH(chainNodes,chainEdges);
                cH.fixChain();
                chains.push_back(cH);
            }
        }

        return chains;
    }

    template<class graphType, class vecType>
    edgeHalf shortestPathSampler3<graphType,vecType>::findEdge(
                                                    const size_t nodeFrom, 
                                                    const size_t nodeTo)
    {
        edgeHalf eH;
        size_t winID=std::numeric_limits<size_t>::max();
        assert((nodeFrom>=0)&& (nodeFrom<(grPtr->m_nV)));
        assert((nodeTo>=0)&& (nodeTo<(grPtr->m_nV)));
        for(size_t i=0;i<adjMap[nodeFrom].size();++i)
        {
            if(adjMap[nodeFrom][i].ndID==nodeTo)
            {
                winID=i;
                break;
            }
        }
        if(winID!=std::numeric_limits<size_t>::max())
        {
            eH=adjMap[nodeFrom][winID];
        }
        return eH;
    }

    template<class graphType, class vecType>
    size_t shortestPathSampler3<graphType,vecType>::findStartNode(std::vector<int>& numNbrsOfNode, const size_t curID)
    {
        size_t winID=curID;
        for(size_t id=curID+1;id<grPtr->m_nV;++id)
        {
            if(numNbrsOfNode[id]>0)
            {
                winID=id;
                break;
            }
        }
        return winID;
    }

    template<class graphType, class vecType>
    std::map<size_t,std::vector<edgeHalf>> shortestPathSampler3<graphType,vecType>::createAdjMap()
    {
        std::map<size_t,std::vector<edgeHalf>> adjMap;
        for(size_t i=0;i<grPtr->m_nV;++i)
        {
            std::vector<edgeHalf> eVector;
            adjMap[i]=grPtr->getNbrsToNode(i);
        }
        return adjMap;
    }

    template<class graphType, class vecType>
    size_t shortestPathSampler3<graphType,vecType>::getNextID(const size_t id, std::vector<int>& numNbrsOfNodes)
    {
        for(size_t i=id+1;i<numNbrsOfNodes.size();++i)
        {
            if(numNbrsOfNodes[i]>0) return i;
        }
    }

    template<class graphType, class vecType>
    void shortestPathSampler3<graphType,vecType>::BFS(size_t currentID,
                                                      std::vector<int>& distances,
                                                      std::vector<int>& numPaths,
                                                      std::vector<int>& previousNodes,
                                                      std::vector<bool>& visited)
    {
        //  do I need to reset the current ID or 
        //  do I need to reset the distances.
        distances[currentID]=0;                                             //  
        std::priority_queue<intPair,                                        //  
        std::vector<intPair>,                                               //  
        std::greater<intPair> > pq;                                         //  
        pq.push(std::make_pair(distances[currentID],currentID));            //  

        numPaths[currentID]=1;

        while(!pq.empty())
        {
            //  pop the node
            size_t nd=pq.top().second;                  //  
            pq.pop();                                   //  
            visited[nd]=true;                           //  

            //  BFS for all neighboring nodes
            for(size_t e=0;e<adjMap[nd].size();++e)     //  
            {
                size_t nbrNd=adjMap[nd][e].ndID;        //  
                size_t eID=adjMap[nd][e].eID;           //  
                size_t alt=distances[nd]+1;             //  

                if(visited[nbrNd]==false)
                {
                    //  add the pair into the 
                    //  priority queue
                    edgePair eP;                        //  
                    eP.first=distances[nbrNd];          //  
                    eP.second=nbrNd;                    //  
                    pq.push(eP);                        //  
                    visited[nbrNd]=true;
                }

                //  if new distance is less than        //  
                //  old distance, update the distances.
                if(distances[nbrNd]>distances[nd]+1)
                {
                    distances[nbrNd]=alt;               //  
                    previousNodes[nbrNd]=nd;            //  
                    numPaths[nbrNd]=numPaths[nd];       //  
                }
                else if(distances[nd]+1==distances[nbrNd])          //  
                {
                    numPaths[nbrNd]+=numPaths[nd];                 //  
                }
            }
        }
      
    }

};

