#pragma once
/*!
	@file spam.h
	@author Siddharth Tourani
	@brief The SPAM class.
*/

#include "msgAlgo.h"
#include "alignedAllocator.h"
#include "simd.h"
// #include "graphSampling.h"
#include "utils.h"
#include "qpbo/qpbo.h"
#include "uGraph.h"
#include "testGraphs2.h"
#include "timer.h"

struct Assignment
{
	int id;
	int left;
	int right;
	double cost;
};

struct EdgeCost
{
	int assID0;
	int assID1;
	double cost;
};

static constexpr double INFTY = 1e20;
enum GRAPH_TYPE{GRID_GRAPH,GENERAL_GRAPH};

// template<class vecType>
template<class T>
struct OpTriplet
{
	// using T=typename vecType::scalar;
	size_t e;
	size_t dir;
	T multiplier;

	OpTriplet(size_t E,size_t D,T m): e(E),dir(D),multiplier(m){}


	friend std::ostream& operator<<(std::ostream& os, const OpTriplet<T>& oT);

};

using index2D=std::pair<size_t,size_t>;

// template<class T>
// std::ostream& operator<<(std::ostream& os, const OpTriplet<T>& oT)
// {
// 	os << "e: " <<
// }

// using chain=typename std::vector<OpTriplet<class T>>;
using chain= std::vector<size_t>;

namespace Solver{
	template<class vecType>
	class spam 
	{
		using uGraph=Graph::uGraph;

		public:
			using vectorizer=typename vecType::alignedVec;
			using T=typename vecType::scalar;
			using vector=typename vecType::scalar;

		struct options
		{
			size_t options;
			Solver::sampType sampling;
			Solver::GraphType grType;
			Solver::RoundingType roundType;
			size_t numGenerationsForPrimalRounding;
			size_t numIters;
			size_t nRows;		//	Used if the graph is a grid	
			size_t nCols;		//	Used if the graph is a grid
            bool ICM;
            size_t numICMIters;
		};

		size_t computePadding(size_t l)
		{
			return ((l/sizeof(vector))+1)*sizeof(vector);
		}
		
		size_t flattenIndex(size_t r, size_t c, size_t Cols)
		{
			return r*Cols+c;
		}

		struct node
		{
			size_t K;
			size_t vK;
			T* data;

            T &operator[](int i) {
                assert(i>=0 && i< K);
                return data[i];
            }
            T & val(int i) {
                assert(i>=0 && i< K);
                return data[i];
            }
		};

		struct edge
		{
			

			size_t s;
			size_t t;

			size_t K1;
			size_t vK2;
			size_t K2;
			
			T* head;
			T* tail;

			T* data;

            T & val(const size_t id1, const size_t id2) {
                assert(id1>=0 && id1< K1);
                assert(id2>=0 && id2< K2);
                return data[id1*vK2+id2];
            }
		};

		alignedAllocator mAllocator;
		std::vector<node> mNodes;
		std::vector<edge> mEdges;
		std::vector<chain> mChains;
		std::map<std::pair<size_t,size_t>,size_t> nodePair2Edges;
		std::vector<std::vector<OpTriplet<T>>> mChainOperations;
		// std::vector<OpTriplet<T>> mChains;
		std::vector<size_t> mLabelling;
		options mOpts;
		T primalE;
		size_t numMessagesPassed;
		size_t nV;
		size_t nE;
		std::random_device dev;
	    std::mt19937 rng;	
		adjacencyList adjList;

		//	read the file fill up the unary and pairwise terms.
		spam(const Graph::graphStorage& gu,const spam<vecType>::options& opts);
		void allocateMemory();
		void sampleGraph();
		// void runIteration();
		void computeOperations();
		// void computePrimalNormal();
		// void computePrimalQPBO();

		void infer();
		void runIter();
		
		void computeAdjacencyList();


		void computeRowOperations(const size_t r);
		void computeColOperations(const  chain& chn);


		void connectGraph(const Graph::graphStorage& gu);
		void connectGridGraph(const Graph::graphStorage& gu);
		void connectCompleteGraph(const Graph::graphStorage& gu);

		//	DUAL UPDATES
		void Msg(size_t);
		void MsgT(size_t);
		void Handshake(size_t);
		void HandshakeT(size_t);
		T dual();

		//	DEBUG FUNCTIONS
		void printPairwise(const size_t eID);
		void printPairwise(const size_t i, const size_t j);
		void printUnary(const size_t i);
		void printGraph();




		//	PRIMAL UPDATES
		void messagePrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom);

		void messageTPrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom);


		size_t pickRandomVertexPrimal(const std::vector<size_t>& unprocessedVertices);
		
		size_t  pickRandomNeighbor(size_t vtxID, std::vector<bool>& processedVertices);

		T primalGreedy();
		
		T primal();

		void sampleGridGraph();

		void sampleCompleteGraph();

        T computeUpdatedEnergy(T E, size_t v, size_t newLbl, std::vector<size_t>& bestLabelling);

        T icm();

		T primalFusionMoves(const size_t numGens=1);
        T primalReducedLabelsFusionMoves(const size_t numGenerations);

        std::vector<size_t> buildAndSolveReducedLabelAuxiliaryProblem(std::vector<size_t>& labelling1,
												                        std::vector<size_t>& labelling2);


		std::vector<size_t> buildAndSolveAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2);

		std::vector<size_t> computeLabellingOrdered();
		
		std::vector<size_t> computeLabellingGreedyRandom();
		
		T computeEnergy(std::vector<size_t>& labelling);

		std::vector<std::vector<T>> unaries;
		
		std::vector<std::vector<T>> pairwise;
	};

	template<class vecType>
	std::vector<size_t> spam<vecType>::computeLabellingGreedyRandom()
	{
		using T=typename vecType::scalar;

		std::vector<size_t> labelling(nV);	
		std::vector<bool> processedVertices(nV,false);
		std::vector<size_t> unprocessedVertices;
		std::vector<size_t> unprocessedNeighbors;
		size_t i=0;
		std::generate_n(std::inserter(unprocessedVertices,
									unprocessedVertices.begin()),
									nV,[&i](){ return i++; });
		
		//	Current vertex to process
		size_t currentVertex;

		//	Pick a vertex to start at from the set of unprocessed vertices
		currentVertex=pickRandomVertexPrimal(unprocessedVertices);
		// currentVertex=0;

		// Num Processed Vertices
		size_t numProcessedVertices=0;

		// for(size_t i=0;i<mNodes.size();++i){
		// 	std::cout << "at node " << i << ": ";
		// 	for(size_t j=0;j<mNodes[i].vK;++j)
		// 	{
		// 		std::cout << mNodes[i].data[j]<<  " ";
		// 	}
		// 	std::cout << "\n";
		// }
		// std::cout << "\n";
		// getchar();


		bool doneFlag=false;
		while(!doneFlag)		{

			std::vector<size_t>::iterator vIt=std::find(unprocessedNeighbors.begin(),
														unprocessedNeighbors.end(),
														currentVertex);
			if(vIt!=unprocessedNeighbors.end())
			{
				unprocessedNeighbors.erase(vIt);
			}

			// std::cout << "unprocessedNeighbors size: " << unprocessedNeighbors.size() << "\n";
			// std::cout << "unprocessedNeighbors: "; printVectorBasic(unprocessedNeighbors);
			// std::cout << "currentVertex: " << currentVertex << "\n";

			std::vector<T> unaryVec;
			
			// copy unary vector for the currentVtx  into unaryVec
			std::copy(mNodes[currentVertex].data, 
					mNodes[currentVertex].data+(mNodes[currentVertex].K), 
					std::back_inserter(unaryVec));

			const adjacencyVector& nbrList=adjList[currentVertex];
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					(nbrList[i].ndID<currentVertex) ? 
						messagePrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]);
				}
				else
				{	
					vIt=std::find(unprocessedNeighbors.begin(),unprocessedNeighbors.end(),nbrList[i].ndID);
					if(vIt==unprocessedNeighbors.end())
					{
						// std::cout << BOLDBLUE << "adding unprocessed neighbor: " << nbrList[i].ndID << "\n" << RESET;
						unprocessedNeighbors.push_back(nbrList[i].ndID);
					}
				}
			}


			//	Get the min element	
			labelling[currentVertex]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			// erase the element from unprocessed vertices 
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVertex));


			//	update the processed vertices
			numProcessedVertices++;
			processedVertices[currentVertex]=true;

			size_t newVtx=sizetInf;

			// find the new vertex
			if(numProcessedVertices==nV)
			{
				doneFlag=true;
				break;
			}
			else
			{
				// newVtx=pickRandomNeighbor(currentVertex,processedVertices);
				size_t randInd=rand()%unprocessedNeighbors.size();
				newVtx=unprocessedNeighbors[randInd];

				// if no valid neighbor exists, pick a random vertex
				if(newVtx==sizetInf)	{
					// std::cout << "THIS IS THE FUCK UP\n";
					newVtx=pickRandomVertexPrimal(unprocessedVertices);	
				}
			}
			
			//  update the currentVtx 
			currentVertex=newVtx;
			// getchar();
		}

		return labelling;
	}

	template<class vecType>
	typename vecType::scalar spam<vecType>::computeEnergy(std::vector<size_t>& labelling)
	{
		using T=typename vecType::scalar;

		T energy=0;

		for(size_t nd=0;nd<nV;++nd)
		{
			energy+=mNodes[nd].data[labelling[nd]];
		}
		for(size_t e=0;e<nE;++e)
		{
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
			size_t vK2=mEdges[e].vK2;
			energy+=mEdges[e].data[labelling[s]*vK2+labelling[t]];
		}
		return energy;
	}

	template<class vecType>
	size_t spam<vecType>::pickRandomVertexPrimal(const std::vector<size_t>& unprocessedVertices)
	{
		using T=typename vecType::scalar;
		size_t vtx=std::numeric_limits<T>::max();
		if(unprocessedVertices.size()>0)
		{
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprocessedVertices.size()-1); // distribution in range [1, 6]
			auto it = unprocessedVertices.cbegin();
    		std::advance(it,dist6(rng));
    		vtx=(*it);	
		}
		return vtx;
	}

	template<class vecType>
	size_t  spam<vecType>::pickRandomNeighbor(size_t vtxID, std::vector<bool>& processedVertices)
	{
		size_t pickedNbr=sizetInf;

		/*!	get the adjacency list of the graph */
		const adjacencyVector& adVec=adjList[vtxID];

		/*!	store the processed neighbors in a vector */
		std::vector<size_t> unprNbrs;
		for(size_t i=0;i<adVec.size();++i)
		{
			if(!processedVertices[adVec[i].ndID])	{
				unprNbrs.push_back(adVec[i].ndID);
			}
		}

		/*! pick a random neighbor from the vector of processed neighbors	*/
		if(unprNbrs.size()>0)
		{
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprNbrs.size()-1); // distribution in range [1, 6]
			auto it = unprNbrs.cbegin();
    		std::advance(it,dist6(rng));
    		pickedNbr=(*it);	
		}
		return pickedNbr;
	}


	template<class vecType>
	void spam<vecType>::messagePrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  const size_t eID, 
										const size_t winLabelFrom)
	{
		assert(mEdges[eID].t==unID);

		/*!	K is the length of the unary */
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].vK;
		size_t vK2=mEdges[eID].vK2;

		/*!			*/
		for(size_t j=0;j<K;++j)		{
			un[j]+=(mEdges[eID].data[winLabelFrom*vK+j]);		
		}
	}

	template<class vecType>
	void spam<vecType>::messageTPrimal(std::vector<typename vecType::scalar>& un,
										const size_t unID,  
										const size_t eID, 
										const size_t winLabelFrom)
	{
		assert(mEdges[eID].s==unID);

		//	K is the length of the unary
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].vK;
		size_t vK2=mEdges[eID].vK2;


		for(size_t j=0;j<K;++j)
		{
			un[j]+=mEdges[eID].data[j*vK2+winLabelFrom];
		}
	}


	template<class vecType>
	std::vector<size_t> spam<vecType>::buildAndSolveReducedLabelAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2)
	{
		using T=typename vecType::scalar;

        std::vector<size_t> validNodes;
        for(size_t i=0;i<labelling1.size();++i)
        {
            if(labelling1[i]!=labelling2[i])
            {
                validNodes.push_back(i);
            }
        }

        std::vector<size_t> validEdges;
        for(size_t e=0;e<nE;++e)
        {
            auto& mE=mEdges[e];
			size_t vK2=mE.vK2;
			size_t s=mE.s;
			size_t t=mE.t;

            auto s_it=std::find(validNodes.begin(),validNodes.end(),s);
            auto t_it=std::find(validNodes.begin(),validNodes.end(),t);
            if(s_it!=validNodes.end()&& t_it!=validNodes.end())
            {
                validEdges.push_back(e);
            }
        }

		qpbo::QPBO<T> q(validNodes.size(),validEdges.size());
		q.AddNode(validNodes.size());

        for(size_t i=0;i<validNodes.size();++i)
        {
	        T C0=mNodes[validNodes[i]].data[labelling1[validNodes[i]]];
			T C1=mNodes[validNodes[i]].data[labelling2[validNodes[i]]];
			q.AddUnaryTerm(i,C0,C1);
        }

        for(size_t e=0;e<validEdges.size();++e)
        {
            auto& mE=mEdges[validEdges[e]];
			size_t vK2=mE.vK2;

            //  s and t ought to be valid edges
			size_t s=mE.s;
			size_t t=mE.t;

            size_t l1s=labelling1[s];
			size_t l1t=labelling1[t];
			size_t l2s=labelling2[s];
			size_t l2t=labelling2[t];

			T C00=mE.val(l1s,l1t);
			T C01=mE.val(l1s,l2t);
			T C10=mE.val(l2s,l1t);
			T C11=mE.val(l2s,l2t);

			q.AddPairwiseTerm(s,t,C00,C01,C10,C11);
        }

		q.MergeParallelEdges();
        q.Solve();

		// q.ComputeWeakPersistencies();

        std::vector<size_t> labelling=labelling1;
		for(size_t v=0;v<validNodes.size();++v){

            size_t val=q.GetLabel(v);

            if(val==1)
            {
               labelling[validNodes[v]]=labelling2[validNodes[v]];
            }
		}	
		return labelling;
	}


	template<class vecType>
	std::vector<size_t> spam<vecType>::buildAndSolveAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2)
	{
		using T=typename vecType::scalar;
		qpbo::QPBO<T> q(nV,nE);
		q.AddNode(nV);

		for(size_t i=0;i<nV;++i)
		{	
			T C0=mNodes[i].data[labelling1[i]];
			T C1=mNodes[i].data[labelling2[i]];
			if(labelling1[i]!=labelling2[i])
			{
				q.AddUnaryTerm(i,C0,C1);
			}
			else
			{
				q.AddUnaryTerm(i,C0,INFTY);
			}
			
		}
		
		for(size_t e=0;e<nE;++e)
		{
			auto& mE=mEdges[e];
			size_t vK2=mE.vK2;
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
			size_t l1s=labelling1[s];
			size_t l1t=labelling1[t];
			size_t l2s=labelling2[s];
			size_t l2t=labelling2[t];
			T C00=mE.data[l1s*vK2+l1t];
			T C01=mE.data[l1s*vK2+l2t];
			T C10=mE.data[l2s*vK2+l1t];
			T C11=mE.data[l2s*vK2+l2t];
			q.AddPairwiseTerm(s,t,C00,C01,C10,C11);
		}

		q.MergeParallelEdges();
		q.Solve();
		// q.ComputeWeakPersistencies();

		std::vector<size_t> labelling(nV,0);
		for(size_t i=0;i<nV;++i){
			size_t v=q.GetLabel(i);
			if(v==0)
			{
				labelling[i]=labelling1[i];
			}
			else
			{
				labelling[i]=labelling2[i];
			}	
		}	
		return labelling;
	}

	template<class vecType>
	typename vecType::scalar spam<vecType>::primalFusionMoves(const size_t numGenerations)
	{
		using T=typename vecType::scalar;

		//	For each of the num generations. 
		//	Build the auxiliary problem
		//	Solve the QPBO Problem
		//	Set the labelling.
		// auto l1=computeLabellingGreedyRandom();
		std::vector<size_t> l1=mLabelling;
		T bestEnergy=computeEnergy(l1);

		for(size_t i=0;i<numGenerations;++i)
		{
			auto l2=computeLabellingGreedyRandom();
			l1=buildAndSolveAuxiliaryProblem(l1,l2);

			T currEnergy=computeEnergy(l1);
			// std::cout << "currEnergy: " << currEnergy << " bestEnergy: " << bestEnergy << "\n";
			if(bestEnergy>currEnergy)
			{
				bestEnergy=currEnergy;
				mLabelling=l1;
			}
		}
		
		return bestEnergy;
	}


    template<class vecType>
    typename vecType::scalar spam<vecType>::primalReducedLabelsFusionMoves(const size_t numGenerations)
	{
		using T=typename vecType::scalar;

		//	For each of the num generations. 
		//	Build the auxiliary problem
		//	Solve the QPBO Problem
		//	Set the labelling.
		// auto l1=computeLabellingGreedyRandom();
		std::vector<size_t> l1=mLabelling;
		T primalE=std::numeric_limits<T>::max();
		T bestEnergy=computeEnergy(l1);

		for(size_t i=0;i<numGenerations;++i)
		{
			auto l2=computeLabellingGreedyRandom();
			l1=buildAndSolveReducedLabelAuxiliaryProblem(l1,l2);

			T currEnergy=computeEnergy(l1);
			if(bestEnergy>currEnergy)
			{
				bestEnergy=currEnergy;
			}
		}
		
		if(primalE>bestEnergy)
		{
			mLabelling=l1;
			primalE=bestEnergy;
		}
		return bestEnergy;
	}


	template<class vecType>
	std::vector<size_t> spam<vecType>::computeLabellingOrdered()
	{
		using T=typename vecType::scalar;
		std::vector<size_t> labelling(nV);
		std::vector<bool> processedVertices(nV,false);
		std::vector<size_t> unprocessedVertices;
		size_t i=0;
		std::generate_n(std::inserter(unprocessedVertices,
						unprocessedVertices.begin()),nV,[&i](){ return i++; });
		
		//	Keeps a list of prospective neighbors to select from
		std::vector<size_t> unprocessedNeighbors;

		//	Current vertex to process
		size_t currentVertex;

		//	Pick a vertex to start at from the set of unprocessed vertices
		currentVertex=0;

		// Num Processed Vertices
		size_t numProcessedVertices=0;


		bool doneFlag=false;
		while(!doneFlag)		
		{

			// std::cout << "CURRENT VERTEX: " << currentVertex << "\n";

			std::vector<size_t>::iterator vIt=std::find(unprocessedNeighbors.begin(),
														unprocessedNeighbors.end(),
														currentVertex);
			if(vIt!=unprocessedNeighbors.end())
			{
				unprocessedNeighbors.erase(vIt);
			}

			std::vector<T> unaryVec;
			
			// copy unary vector for the currentVtx  into unaryVec
			std::copy(mNodes[currentVertex].data, 
					mNodes[currentVertex].data+(mNodes[currentVertex].K), 
					std::back_inserter(unaryVec));

			// std::cout << "BEFORE: ";
			// printVectorBasic(unaryVec);

			const adjacencyVector& nbrList=adjList[currentVertex];
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					// std::cout << "Passing message from "<<  nbrList[i].ndID  << " to: " << currentVertex << "\n";
					(nbrList[i].ndID<currentVertex) ? 
						messagePrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVertex,nbrList[i].eID,labelling[nbrList[i].ndID]);
				}
				else
				{
					vIt=std::find(unprocessedNeighbors.begin(),unprocessedNeighbors.end(),nbrList[i].ndID);
					if(vIt==unprocessedNeighbors.end())
					{
						unprocessedNeighbors.push_back(nbrList[i].ndID);
					}
				}
				
			}

			// std::cout << "AFTER: ";
			// printVectorBasic(unaryVec);

			//	Get the min element	
			labelling[currentVertex]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			// std::cout << "min labelling: " << labelling[currentVertex] << "\n";

			// erase the element from unprocessed vertices 
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVertex));

			//	update the processed vertices
			numProcessedVertices++;
			processedVertices[currentVertex]=true;

			size_t newVtx=sizetInf;

			// std::cout << "numProcessedVertices: " << numProcessedVertices << "\n";

			// find the new vertex
			if(numProcessedVertices==nV)
			{
				doneFlag=true;
				break;
			}
			else
			{
				// newVtx=pickRandomNeighbor(currentVertex,processedVertices);

				size_t randInd=rand()%unprocessedNeighbors.size();
				newVtx=unprocessedNeighbors[randInd];

				// std::cout << "newVtx: " << newVtx << "\n";
				// if no valid neighbor exists, pick a random vertex
				if(newVtx==sizetInf)	{
					// std::cout << "THIS IS FUCKED UP\n";
					newVtx=pickRandomVertexPrimal(unprocessedVertices);	
				}
			}		

			//  update the currentVtx 
			currentVertex=newVtx;
			// getchar();
		}


		return labelling;
	}

	template<class vecType>
	void spam<vecType>::printGraph()
	{
		size_t ii=0;
		for(auto& nd:mNodes)
		{
			std::cout << "UNARY " << ii<< ": ";
			for(size_t k=0;k<nd.K;++k)
			{
				std::cout << nd.data[k] << " ";
			}
			std::cout << "\n";
			ii++;
		}
	}
	
	template<class vecType>
	void spam<vecType>::printUnary(const size_t i)
	{
		for(size_t j=0;j<mNodes[i].K;++j)
		{
			std::cout << mNodes[i].data[j] << " ";
		}
		std::cout << "\n";
	}

	template<class vecType>
	void spam<vecType>::printPairwise(const size_t eID)
	{
		size_t K1=mEdges[eID].K1;
		size_t K2=mEdges[eID].K2;
		size_t vK2=mEdges[eID].vK2;
		std::cout << "Printing Pairwise: " << mEdges[eID].s << " -- " << mEdges[eID].t << "\n";
		for(size_t i=0;i<K1;++i)
		{
			for(size_t j=0;j<K2;++j)
			{
				std::cout << mEdges[eID].data[i*vK2+j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	template<class vecType>
	void spam<vecType>::printPairwise(const size_t i,const size_t j)
	{
		size_t eID=nodePair2Edges[std::make_pair(i,j)];
		std::cout << "Printing Pairwise: " << mEdges[eID].s << " -- " << mEdges[eID].t << "\n";
		size_t K1=mEdges[eID].K1;
		size_t K2=mEdges[eID].K2;
		size_t vK2=mEdges[eID].vK2;
		for(size_t i=0;i<K1;++i)
		{
			for(size_t j=0;j<K2;++j)
			{
				std::cout << mEdges[eID].data[i*vK2+j] << " ";
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}

	template<class vecType>
	typename vecType::scalar spam<vecType>::primal()
	{
		using T=typename vecType::scalar;
		/*!	Fill up the labelling.	*/
		mLabelling=computeLabellingOrdered();
		// mLabelling=computeLabellingGreedyRandom();
		T energy=computeEnergy(mLabelling);
		return energy;
	}

    template<class vecType>
    typename vecType::scalar spam<vecType>::computeUpdatedEnergy(typename vecType::scalar E, 
                                                                size_t v, size_t newLbl, 
                                                                std::vector<size_t>& bestLabelling)
    {
        using T=typename vecType::scalar;

        T updatedE=E-mNodes[v].val(bestLabelling[v])+mNodes[v].val(newLbl);

        for(auto&nbr : adjList[v])
        {
            if(v<nbr.ndID)
            {
                updatedE=updatedE
                            -mEdges[nbr.eID].val(bestLabelling[v],bestLabelling[nbr.ndID])
                            +mEdges[nbr.eID].val(newLbl,bestLabelling[nbr.ndID]);
            }
            else
            {
                updatedE=updatedE
                -mEdges[nbr.eID].val(bestLabelling[nbr.ndID],bestLabelling[v])
                +mEdges[nbr.eID].val(bestLabelling[nbr.ndID],newLbl);
            }
        }
        return updatedE;
    }


    template<class vecType>
    typename vecType::scalar spam<vecType>::icm()
    {
        using T=typename vecType::scalar;

        auto bestLabelling=mLabelling;
        auto bestE=computeEnergy(bestLabelling);
        // std::cout << "Before ICM Energy: " << bestE << "\n";
        for(size_t iter=0;iter<mOpts.numICMIters;++iter)
        {
            for(size_t v=0;v<nV;++v)
            {
                // pick random label. check if 
                
				for(size_t lbl=0;lbl<mNodes[v].K;++lbl)
				{
                	if(lbl==bestLabelling[v])
                    	continue;
					T newE=computeUpdatedEnergy(bestE,v,lbl,bestLabelling);

					if(newE<bestE)
                	{
                    	// std::cout << "label changed at " << v << "\n";
                    	bestE=newE;
	                    bestLabelling[v]=lbl;
                	}

				}
            }
        }
        mLabelling=bestLabelling;
        return bestE;
    }

	template<class vecType>
	void spam<vecType>::infer()
	{
		using T=typename vecType::scalar;
		// mLabelling=computeLabellingGreedyRandom();
		mLabelling=computeLabellingOrdered();
		T pE=computeEnergy(mLabelling);
		T pEBefore;
		for(size_t i=1;i<=mOpts.numIters;++i)
		{
			runIter();
			T dE=dual();
			if(mOpts.roundType==Solver::NORMAL)
			{
				pE=primal();
			}
			else if(mOpts.roundType==Solver::FUSIONMOVES)
			{
				pEBefore=primalFusionMoves();
			}

			// std::cout << "pE: " << pE << "\n";
			std::cout << i << " " << numMessagesPassed << " " << dE << " " << pEBefore << "\n";
		}
	}


	template<class vecType>
	void spam<vecType>::runIter()
	{ 
		for(size_t i=0;i<mChainOperations.size();++i)
		{
			for(size_t j=0;j<mChainOperations[i].size();++j)
			{
				// size_t e=mChainOperations[i][j].e;
				// size_t s=mEdges[e].s;
				// size_t t=mEdges[e].t;


				if(mChainOperations[i][j].dir==0){
					numMessagesPassed++;
					Msg(mChainOperations[i][j].e);
				}
				else if(mChainOperations[i][j].dir==1){
					numMessagesPassed++;
					MsgT(mChainOperations[i][j].e);
				}
				else if(mChainOperations[i][j].dir==2){
					numMessagesPassed+=3;
					Handshake(mChainOperations[i][j].e);
				}
			}
			
		}
	}

	template<class vecType>
	typename vecType::scalar spam<vecType>::dual()
	{
		std::vector<T> dualVec(nV);
		T dualE=0;
		for(auto& nd: mNodes)
		{
			T val=nd.data[0];
			for(size_t l=1;l<nd.K;++l)
			{
				if(val>nd.data[l]){
					val=nd.data[l];
				}
			}
			dualE+=val;
		}
		return dualE;
	}

	template<class vecType>
	void spam<vecType>::Msg(size_t e)
	{
		assert(e>=0 && e<nE);
		T* fr=mEdges[e].head;
		T* pw=mEdges[e].data;
		T* to=mEdges[e].tail;
		size_t vK2=mEdges[e].vK2;
		size_t K1=mEdges[e].K1;
		size_t K2=mEdges[e].K2;

		for(size_t i=0;i<K1;++i)	{
			for(size_t j=0;j<K2;++j)	{
				*(pw+j)+=*(fr);
			}
			pw+=mEdges[e].vK2;
			(*fr)=0;	fr++;
		}

		pw=mEdges[e].data;
		for(size_t j=0;j<K2;++j)	{	
			T val=*pw;

			//	Compute min value
			for(size_t i=1;i<K1;++i) {
				if(val>*(pw+i*vK2))	{
					val=*(pw+i*vK2);
				}
			}
			for(size_t i=0;i<K1;++i){
				*(pw+i*vK2)-=val;
			}
			(*to)+=val;
			pw++;	to++;
		}
	}


	template<class vecType>
	void spam<vecType>::MsgT(size_t e)
	{
		T* fr=mEdges[e].head;
		T* pw=mEdges[e].data;
		T* to=mEdges[e].tail;
		size_t vK2=mEdges[e].vK2;
		size_t K1=mEdges[e].K1;
		size_t K2=mEdges[e].K2;

		for(size_t j=0;j<K2;++j){
			for(size_t i=0;i<K1;++i){
				*(pw+i*vK2)+=*(to);
			}
			pw++;
			*to=0; 
			to++;
		}

		pw=mEdges[e].data;
		for(size_t i=0;i<K1;++i)	{	
			T val=*pw;
			//	Compute min value
			for(size_t j=1;j<K2;++j)	{
				if(val>*(pw+j))	{
					val=(*(pw+j));
				}
			}
			for(size_t j=0;j<K2;++j){
				(*(pw+j))-=val;
			}
			(*fr)+=val;
			pw+=vK2;	fr++;
		}
	}

	template<class vecType>
	void spam<vecType>::Handshake(size_t e)
	{
		T* fr=mEdges[e].head;
		T* pw=mEdges[e].data;
		T* to=mEdges[e].tail;
		size_t vK2=mEdges[e].vK2;
		size_t K1=mEdges[e].K1;
		size_t K2=mEdges[e].K2;

		for(size_t i=0;i<K1;++i)	{
			for(size_t j=0;j<K2;++j)	{
				*(pw+j)+=*(fr)+*(to+j);
			}
			pw+=mEdges[e].vK2;
			*fr=0;	fr++;
		}

		fr=mEdges[e].head;
		// to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t i=0;i<K1;++i)	{	
			T val=*pw;
			//	Compute min value
			for(size_t j=1;j<K2;++j)	{
				if(val>*(pw+j))	{
					val=*(pw+j);
				}
			}
			for(size_t j=0;j<K2;++j){
				*(pw+j)-=0.5*val;
			}
			*(fr)=0.5*val;
			pw+=vK2;	fr++;
		}

		fr=mEdges[e].head;
		to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t j=0;j<K2;++j)	{	
			T val=*pw;

			//	Compute min value
			for(size_t i=1;i<K1;++i) {
				if(val>*(pw+i*vK2))	{
					val=*(pw+i*vK2);
				}
			}
			for(size_t i=0;i<K1;++i){
				*(pw+i*vK2)-=val;
			}
			(*to)=val;
			pw++;	to++;
		}

		fr=mEdges[e].head;
		to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t i=0;i<K1;++i)	{	
			T val=*pw;
			//	Compute min value
			for(size_t j=1;j<K2;++j)	{
				if(val>*(pw+j))	{
					val=*(pw+j);
				}
			}
			for(size_t j=0;j<K2;++j){
				*(pw+j)-=val;
			}
			*(fr)+=val;
			pw+=vK2;	fr++;
		}
	}

	template<class vecType>
	void spam<vecType>::HandshakeT(size_t e)
	{
		T* fr=mEdges[e].head;
		T* pw=mEdges[e].data;
		T* to=mEdges[e].tail;
		size_t vK2=mEdges[e].vK2;
		size_t K1=mEdges[e].K1;
		size_t K2=mEdges[e].K2;

		for(size_t i=0;i<K1;++i)	{
			for(size_t j=0;j<K2;++j)	{
				*(pw+j)+=*(fr)+*(to+j);
			}
			pw+=mEdges[e].vK2;
			*fr=0;	fr++;
		}

		fr=mEdges[e].head;
		to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t j=0;j<K2;++j)	{	
			T val=*pw;

			//	Compute min value
			for(size_t i=1;i<K1;++i) {
				if(val>*(pw+i*vK2))	{
					val=*(pw+i*vK2);
				}
			}
			for(size_t i=0;i<K1;++i){
				*(pw+i*vK2)-=0.5*val;
			}
			(*to)=0.5*val;
			pw++;	to++;
		}


		fr=mEdges[e].head;
		// to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t i=0;i<K1;++i)	{	
			T val=*pw;
			//	Compute min value
			for(size_t j=1;j<K2;++j)	{
				if(val>*(pw+j))	{
					val=*(pw+j);
				}
			}
			for(size_t j=0;j<K2;++j){
				*(pw+j)-=val;
			}
			*(fr)=val;
			pw+=vK2;	fr++;
		}

		fr=mEdges[e].head;
		to=mEdges[e].tail;
		pw=mEdges[e].data;
		for(size_t j=0;j<K2;++j)	{	
			T val=(*pw);

			//	Compute min value
			for(size_t i=1;i<K1;++i) {
				if(val>*(pw+i*vK2))	{
					val=*(pw+i*vK2);
				}
			}
			for(size_t i=0;i<K1;++i){
				*(pw+i*vK2)-=val;
			}
			(*to)+=val;
			pw++;	to++;
		}

		

		
	}


	
	template<class vecType>
	spam<vecType>::spam(const Graph::graphStorage& gu,const spam<vecType>::options& opts):mOpts(opts),
																			mLabelling(gu.unaries.size()),
																			nV(gu.unaries.size()),
																			nE(gu.pairwise.size()),
																			numMessagesPassed(0)																	
	{
		Timer timer;
		// timer.start();

		// unaries=gu.unaries;
		// pairwise=gu.pairwise;
		nV=gu.unaries.size();
		nE=gu.pairwise.size();
		mNodes.resize(nV);
		mEdges.resize(nE);
		pairwise.resize(nE);
		unaries=gu.unaries;

		for(size_t n=0;n<unaries.size();++n)
		{
			// mNodes[n].vK=computePadding(unaries[n].size());
			mNodes[n].vK=gu.unaries[n].size();
			mNodes[n].K=gu.unaries[n].size();
		}

		connectGraph(gu);

		// timer.stop();
		// std::cout << "Time taken to fill up nodes and edges: " << timer.elapsedSeconds() << "\n";

		// timer.start();
		allocateMemory();
		// timer.stop();
		// std::cout << "Time taken to allocateMemory: " << timer.elapsedSeconds() << "\n";

		// timer.start();
		sampleGraph();
		// timer.stop();
		// std::cout << "Time taken to sampleGraph: " << timer.elapsedSeconds() << "\n";
	

		// // timer.start();
		for(size_t i=0;i<mChains.size();++i)
		{
			computeColOperations(mChains[i]);
		}
		// // timer.stop();
		// // std::cout << "Time taken to compute operations: " << timer.elapsedSeconds() << "\n";

		rng=std::mt19937(dev());

		// timer.start();
		computeAdjacencyList();
		// timer.stop();
		// std::cout << "Time taken to compute operations: " << timer.elapsedSeconds() << "\n";
		
	}

	template<class vecType>
	void spam<vecType>::computeAdjacencyList()
	{
		adjList.resize(nV);
		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
		    edgeHalf eH1;
            eH1.eID=e;  eH1.ndID=t;
		    edgeHalf eH2;
            eH2.eID=e;  eH2.ndID=s;
			adjList[s].push_back(eH1);
			adjList[t].push_back(eH2);

		}
	}


	template<class vecType>
	void spam<vecType>::allocateMemory()
	{
		using T=typename vecType::scalar;
		using vectorizer=typename vecType::alignedVec;
		// std::cout << "sizeof(vectorizer): " << sizeof(vectorizer) << "\n";
		size_t V=sizeof(vectorizer)/sizeof(T);

		for(auto& un:unaries)
		{	
			size_t K=un.size();
			size_t vK=(K+V-1)/V;
			mAllocator.allocate_a<vectorizer>(vK);
		}

		assert(mEdges.size()==pairwise.size());
		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t l1=mEdges[e].K1;
			size_t l2=mEdges[e].K2;
			int vK=l1*((V+l2-1)/V);		//	size in vectorizers.
			// std::cout << "vK: " << vK << "\n";
			mAllocator.allocate_a<vectorizer>(vK);
		}

		mAllocator.init();

		for(size_t v=0;v<mNodes.size();++v)
		{	
			size_t K=unaries[v].size();
			size_t vK=(K+V-1)/V;
			mNodes[v].data=mAllocator.allocate_a<T>(vK);
			mNodes[v].K=K;
			//	Copy into the 
			std::copy (unaries[v].begin(), unaries[v].end(), mNodes[v].data );
		}

		for(size_t e=0;e<mEdges.size();++e)
		{
			size_t K1=mEdges[e].K1;
			size_t K2=mEdges[e].K2;
			size_t vK=((V+K2-1)/V);
			mEdges[e].data=mAllocator.allocate_a<T>(K1*vK);
			mEdges[e].head=mNodes[mEdges[e].s].data;
			mEdges[e].tail=mNodes[mEdges[e].t].data;
			//	Copy into the 
			std::copy(pairwise[e].begin(),pairwise[e].end(),mEdges[e].data);
		}

		unaries.clear();
		pairwise.clear();
	}

	template<class vecType>
	void spam<vecType>::connectGraph(const Graph::graphStorage& gu)
	{
		if(mOpts.grType==Solver::COMPLETE)
		{
			connectCompleteGraph( gu);
		}
		else if(mOpts.grType==Solver::GRID)
		{
			connectGridGraph( gu);
		}
	}


	template<class vecType>
	void spam<vecType>::connectGridGraph(const Graph::graphStorage& gu)
	{
		std::map<std::pair<size_t,size_t>,size_t> nodePair2OriginalEdgeID;

		for(size_t e=0;e<gu.G.m_E.size();++e)
		{
			size_t s=gu.G.m_E[e].s;
			size_t t=gu.G.m_E[e].t;
			nodePair2OriginalEdgeID[std::make_pair(s,t)]=e;
		}

		size_t newEID=0;
		// std::cout << "nR: " << mOpts.nRows << " nC: " << mOpts.nCols << "\n";
		for(size_t r=0;r<mOpts.nRows;++r)
		{
			for(size_t c=0;c<mOpts.nCols-1;++c)
			{
				// std::cout << "in here:\n";
				size_t s=flattenIndex(r,c,mOpts.nCols);
				size_t t=flattenIndex(r,c+1,mOpts.nCols);
				std::map<std::pair<size_t,size_t>,size_t>::iterator it;
				it=nodePair2OriginalEdgeID.find(std::make_pair(s,t));
				size_t eID;
				// std::cout << "s: " << s << " t: " << t << "\n";
				if(it==nodePair2OriginalEdgeID.end())
				{
					throw std::runtime_error("Can't find edges.");
				}
				else
				{
					eID=it->second;
				}
				// std::cout << "eID: " << eID << "\n";
				mEdges[newEID].s=s;
				mEdges[newEID].t=t;
				nodePair2Edges[std::make_pair(s,t)]=newEID;
				mEdges[newEID].K1=gu.pwSz[eID].l1;
				mEdges[newEID].K2=gu.pwSz[eID].l2;
				mEdges[newEID].vK2=gu.pwSz[eID].l2;
				pairwise[newEID]=gu.pairwise[eID];
				newEID++;
			}
		}

		for(size_t c=0;c<mOpts.nCols;++c)
		{
			for(size_t r=0;r<mOpts.nRows-1;++r)	
			{
				size_t s=flattenIndex(r,c,mOpts.nCols);
				size_t t=flattenIndex(r+1,c,mOpts.nCols);
				// size_t eID=nodePair2OriginalEdgeID[std::make_pair(s,t)];
				std::map<std::pair<size_t,size_t>,size_t>::iterator it;
				it=nodePair2OriginalEdgeID.find(std::make_pair(s,t));
				size_t eID;
				if(it==nodePair2OriginalEdgeID.end())
				{
					throw std::runtime_error("Can't find edges.");
				}
				else
				{
					eID=it->second;
				}

				mEdges[newEID].s=s;
				mEdges[newEID].t=t;
				nodePair2Edges[std::make_pair(s,t)]=newEID;
				mEdges[newEID].K1=gu.pwSz[eID].l1;
				mEdges[newEID].K2=gu.pwSz[eID].l2;
				mEdges[newEID].vK2=gu.pwSz[eID].l2;
				pairwise[newEID]=gu.pairwise[eID];
				newEID++;
			}
		}

	}

	template<class vecType>
	void spam<vecType>::connectCompleteGraph(const Graph::graphStorage& gu)
	{
		for(size_t e=0;e<gu.G.m_E.size();++e)
		{
			size_t s=gu.G.m_E[e].s;
			size_t t=gu.G.m_E[e].t;
			mEdges[e].s=s;
			mEdges[e].t=t;
			mEdges[e].K1=gu.pwSz[e].l1;
			mEdges[e].K2=gu.pwSz[e].l2;
			mEdges[e].vK2=gu.pwSz[e].l2;
			pairwise[e]=gu.pairwise[e];
			nodePair2Edges[std::make_pair(s,t)]=e;
		}
	}

	template<class vecType>
	void spam<vecType>::sampleGridGraph()
	{
		assert(mOpts.nRows!=0 && mOpts.nCols!=0);

		mChains.resize(mOpts.nRows+mOpts.nCols);
		// size_t r=0;
		

		for(size_t c=0;c<mOpts.nCols;++c)
		{	
			mChains[mOpts.nRows+c].resize(mOpts.nRows-1);
			for(size_t r=0;r<mOpts.nRows-1;++r)	
			{
				size_t s=flattenIndex(r,c,mOpts.nCols);
				size_t t=flattenIndex(r+1,c,mOpts.nCols);
				size_t eID=nodePair2Edges[std::make_pair(s,t)];
				mChains[mOpts.nRows+c][r]=eID;
			}
		}


		for(size_t r=0;r<mOpts.nRows;++r)
		{	
			mChains[r].resize(mOpts.nCols-1);
			for(size_t c=0;c<mOpts.nCols-1;++c)
			{
				size_t s=flattenIndex(r,c,mOpts.nCols);
				size_t t=flattenIndex(r,c+1,mOpts.nCols);
				size_t eID=nodePair2Edges[std::make_pair(s,t)];
				mChains[r][c]=eID;
			}
		}
	}


	template<class vecType>
	void spam<vecType>::sampleCompleteGraph()
	{
		mChains.resize(nE);
		for(size_t e=0;e<mEdges.size();++e)	{
			mChains[e].push_back(e);
		}
	}

	template<class vecType>
	void spam<vecType>::sampleGraph()
	{
		if(mOpts.grType==Solver::GRID)
		{
			sampleGridGraph();
		}
		else if(mOpts.grType==Solver::COMPLETE)
		{
			// std::cout << BOLDBLUE << "Sample Complete Graph\n" << RESET;
			sampleCompleteGraph();
		}

	}

	template<class vecType>
	void spam<vecType>::computeRowOperations(const size_t r)
	{
		using T=typename vecType::scalar;			//	
		using chainEnds=std::pair<size_t,size_t>;	//	
		using chainOps=std::vector<OpTriplet<T>>;	//	

		std::vector<chainEnds> prevLevel;
		std::vector<chainEnds> currLevel;
		std::vector<std::vector<OpTriplet<T>>> operations;

		bool uninitialized=true;

		size_t numEdgesUnshook=mOpts.nCols-1;

		// std::cout << "numEdgesUnshook: " << numEdgesUnshook << "\n";

		bool done=false;
		chainEnds cE=std::make_pair<size_t,size_t>(0,mOpts.nCols+mOpts.nCols-1);
		currLevel.push_back(cE);

		size_t numMessagesPassed=0;

		size_t out=0;
		// while(!done)
		{
			std::vector<chainEnds> nextLevel;
			std::vector<OpTriplet<T>> opsAtCurrLevel;

			for(size_t ch=0;ch<currLevel.size();++ch)
			{
				std::vector<OpTriplet<T>> chainOperations;

				auto& currChainEnds=currLevel[ch];
				chainEnds leftChild, rightChild;
				size_t currStart=currChainEnds.first;
				size_t currStop=currChainEnds.second;

				size_t currMidLeft=static_cast<size_t>(static_cast<float>((static_cast<float>(currStart)+static_cast<float>(currStop))/2));
				size_t currMidRight=currMidLeft+1;

				// std::cout << "start: " << currStart << " currMidLeft: " << currMidLeft << " currMidRight: " << currMidRight << " currStop: " << currStop  << "\n";

				if(currStart==currStop)
				{
					continue;
				}

				if(prevLevel.size()==0)
				{
					//	Forward Message Passes
					for(size_t i=currStart;i<currMidLeft;++i)
					{
						size_t eID=nodePair2Edges[std::make_pair(i,i+1)];
						OpTriplet<T> op(eID,0,1);
						// std::cout << "s: " << i << " t: " << i+1 << "\n";
						chainOperations.push_back(op);
						numMessagesPassed++;
					}

					//	Backward Message Passes
					for(size_t i=currStop;i>currMidRight;--i)
					{
						size_t eID=nodePair2Edges[std::make_pair(i-1,i)];
						OpTriplet<T> op(eID,1,1);
						// std::cout << "s: " << i-1 << " t: " << i << "\n";
						chainOperations.push_back(op);
						numMessagesPassed++;
					}

					chainOperations.push_back(OpTriplet<T>(nodePair2Edges[std::make_pair(currMidLeft,currMidRight)],2,0));
					numMessagesPassed+=3;
				}
				else
				{
					size_t previousIndex=static_cast<size_t>(std::floor(ch/2));
					size_t previousStart=prevLevel[previousIndex].first;
					size_t previousStop=prevLevel[previousIndex].second;

					// std::cout << "prevIdx: " << previousIndex << " start: " << previousStart << " stop: " << previousStop << "\n";

					if(previousStart==currStart)
					{
						// std::cout << "prevStart: "<< previousStart << " currStart " << currStart << "\n";
						for(size_t i=currStop;i>currMidRight;--i)
						{
							OpTriplet<T> op(nodePair2Edges[std::make_pair(i-1,i)],1,1);
							chainOperations.push_back(op);
							numMessagesPassed++;
						}
					}
					
					if(previousStop==currStop)
					{
						// std::cout << "prevStop: "<< previousStop << " currStop " << currStop << "\n";
						for(size_t i=currStart;i<currMidLeft;++i)
						{
							OpTriplet<T> op(nodePair2Edges[std::make_pair(i,i+1)],0,1);
							chainOperations.push_back(op);
							numMessagesPassed++;
						}
					}

					chainOperations.push_back(
						OpTriplet<T>(nodePair2Edges[std::make_pair(currMidLeft,currMidRight)],2,0));
					numMessagesPassed+=3;
				}
				
				//! 
				leftChild=std::make_pair(currStart,currMidLeft);
				rightChild=std::make_pair(currMidRight,currStop);

				nextLevel.push_back(leftChild);
				nextLevel.push_back(rightChild);

				// std::cout << "numMessagesPassed: " << numMessagesPassed << " numEdgesUnshook: " << numEdgesUnshook << "\n";

				numEdgesUnshook--;
				if(numEdgesUnshook==0)
				{
					done=true;
				}
				opsAtCurrLevel.insert(opsAtCurrLevel.end(),chainOperations.begin(),chainOperations.end());
			}

			operations.push_back(opsAtCurrLevel);	
			prevLevel=currLevel;
			currLevel=nextLevel;
		}
		
		std::vector<OpTriplet<T>> flattenOperations;
		for(size_t i=0;i<operations.size();++i)
		{
			flattenOperations.insert(flattenOperations.end(),operations[i].begin(),operations[i].end());
		}
		// std::cout << flattenOperations.size() << "\n";
	}
	
	template<class vecType>
	void spam<vecType>::computeColOperations(const  chain& chn)
	{

		using T=typename vecType::scalar;			//	
		using chainEnds=std::pair<size_t,size_t>;	//	
		using chainOps=std::vector<OpTriplet<T>>;	//	

		std::vector<chainEnds> prevLevel;
		std::vector<chainEnds> currLevel;
		std::vector<std::vector<OpTriplet<T>>> operations;

		bool uninitialized=true;

		size_t numEdgesUnshook=chn.size();

		bool done=false;
		chainEnds cE=std::make_pair<size_t,size_t>(0,chn.size());
		currLevel.push_back(cE);

		size_t numMessagesPassed=0;

		size_t out=0;
		while(!done)
		{
			std::vector<chainEnds> nextLevel;
			std::vector<OpTriplet<T>> opsAtCurrLevel;

			for(size_t ch=0;ch<currLevel.size();++ch)
			{
				std::vector<OpTriplet<T>> chainOperations;

				auto& currChainEnds=currLevel[ch];
				chainEnds leftChild, rightChild;
				size_t currStart=currChainEnds.first;
				size_t currStop=currChainEnds.second;

				size_t currMidLeft=static_cast<size_t>(static_cast<float>((static_cast<float>(currStart)+static_cast<float>(currStop))/2));
				size_t currMidRight=currMidLeft+1;

				// std::cout << "start: " << currStart << " currMidLeft: " << currMidLeft << " currMidRight: " << currMidRight << " currStop: " << currStop << " numMessages Passed: " << numMessagesPassed   << " numEdgesUnshook: " << numEdgesUnshook << "\n";

				if(currStart==currStop) {
					continue;
				}

				if(prevLevel.size()==0){
					//	Forward Message Passes
					for(size_t i=currStart;i<currMidLeft;++i)
					{
						size_t eID=chn[i];
						OpTriplet<T> op(eID,0,1);
						// std::cout << mEdges[eID].s << " -> " << mEdges[eID].t << "\n";
						chainOperations.push_back(op);
						numMessagesPassed++;
					}

					//	Backward Message Passes
					for(size_t i=currStop;i>currMidRight;--i)
					{
						size_t eID=chn[i-1];
						// std::cout << mEdges[eID].s << " <- " << mEdges[eID].t << "\n";
						OpTriplet<T> op(eID,1,1);
						chainOperations.push_back(op);
						numMessagesPassed++;
					}

					// std::cout << mEdges[chn[currMidLeft]].s << "~" << mEdges[chn[currMidLeft]].t << "\n";
					chainOperations.push_back(OpTriplet<T>(chn[currMidLeft],2,0));
					numMessagesPassed+=3;
				}
				else
				{

					size_t previousIndex=static_cast<size_t>(std::floor(ch/2));
					size_t previousStart=prevLevel[previousIndex].first;
					size_t previousStop=prevLevel[previousIndex].second;
					
					if(previousStop==currStop)
					{
						// std::cout << "prevStop: "<< previousStop << " currStop " << currStop << "\n";
						for(size_t i=currStart;i<currMidLeft;++i)
						{
							size_t eID=chn[i];
							OpTriplet<T> op(eID,0,1);
							// std::cout << mEdges[eID].s << " -> " << mEdges[eID].t << "\n";
							chainOperations.push_back(op);
							numMessagesPassed++;
						}
					}

					// std::cout << "prevIdx: " << previousIndex << " start: " << previousStart << " stop: " << previousStop << "\n";
					if(previousStart==currStart)
					{
						// std::cout << "prevStart: "<< previousStart << " currStart " << currStart << "\n";
						for(size_t i=currStop;i>currMidRight;--i)
						{
							size_t eID=chn[i-1];
							OpTriplet<T> op(eID,1,1);
							// std::cout << mEdges[eID].s << " <- " << mEdges[eID].t << "\n";
							chainOperations.push_back(op);
							numMessagesPassed++;
						}
					}

					// std::cout << mEdges[chn[currMidLeft]].s << "~" << mEdges[chn[currMidLeft]].t << "\n";
					chainOperations.push_back(OpTriplet<T>(chn[currMidLeft],2,0));
					numMessagesPassed+=3;
				}



				//! 
				leftChild=std::make_pair(currStart,currMidLeft);
				rightChild=std::make_pair(currMidRight,currStop);

				nextLevel.push_back(leftChild);
				nextLevel.push_back(rightChild);
				numEdgesUnshook--;
				

				opsAtCurrLevel.insert(opsAtCurrLevel.end(),chainOperations.begin(),chainOperations.end());
				// getchar();
				// std::cout << "numMessagesPassed: " << numMessagesPassed << " numEdgesUnshook: " << numEdgesUnshook << "\n";
				
				if(numEdgesUnshook==0)
				{
					done=true;
					break;
				}
				
			}

			operations.push_back(opsAtCurrLevel);	
			prevLevel=currLevel;
			currLevel=nextLevel;
		}
		// std::cout << "numMessagesPassed: " << numMessagesPassed << "\n";
		
		std::vector<OpTriplet<T>> flattenOperations;
		for(size_t i=0;i<operations.size();++i)
		{
			flattenOperations.insert(flattenOperations.end(),operations[i].begin(),operations[i].end());
		}
		// std::cout << flattenOperations.size() << "\n";
		mChainOperations.push_back(flattenOperations);
	}

}
