#pragma once
/*!
    @brief  My aim is to compute the strict shortest paths of graphs.
            This is also something that adjusts with graph density.
            It will involve using Djikstras' algorithm and keeping
            track of multiple paths. 

    @author 
    @date   
*/

#include "config.h"
#include "chain.h"

namespace Solver
{

template<class graphType, class vecType>
class shortestPathSampler
{
    public:
        shortestPathSampler(graphType& gr);
        std::vector<chain<vecType>> sampleGraph();

    //  All of these can be private
        chain<vecType> sampleChain(const size_t src);

        std::map<size_t,std::vector<edgeHalf>> adjMap;

        std::map<size_t,std::vector<edgeHalf>> createAdjMap();

        chain<vecType> createChainFromPath(std::vector<size_t>& path);
        void removeChainFromAdjMap(const chain<vecType>& ch);

        std::vector<size_t> getPath(std::vector<bool>& blockedVertices, 
                                    std::vector<size_t>& distances,
                                    std::vector<size_t>& previousNeighbor);

        //  DEBUG FUNCTIONS
        void printNodeOnlyAdjMap();
        void printEdgeOnlyAdjMap();
        std::vector<bool> extinguishedNodes;

        size_t computeSourceNode();
        std::shared_ptr<graphType> grPtr;
};

template<class graphType, class vecType>
shortestPathSampler<graphType,vecType>::shortestPathSampler(graphType& gr)
{
    grPtr=std::make_shared<graphType>(gr);
    extinguishedNodes.resize(grPtr->m_nV,false);
    adjMap=createAdjMap();
}

template<class graphType, class vecType>
inline size_t shortestPathSampler<graphType,vecType>::computeSourceNode()
{
    assert(adjMap.size()>0);
    return adjMap.begin()->first;
}


template<class graphType, class vecType>
std::map<size_t,std::vector<edgeHalf>> shortestPathSampler<graphType,vecType>::createAdjMap()
{
    std::map<size_t,std::vector<edgeHalf>> adjMap;    

    for(size_t i=0;i<grPtr->m_nV;++i)
    {
        std::vector<edgeHalf> eVector;
        adjMap[i]=grPtr->getNbrsToNode(i);
    }

    return adjMap;
}


template<typename T>
void print_queue(T& q)
{
    while(!q.empty())
    {
        std::cout << q.top().first << " ";
        q.pop();
    }
    std::cout << "\n";
}

void print_PQ(std::priority_queue<std::pair<size_t,size_t>, std::vector<std::pair<size_t,size_t>>, std::greater<std::pair<size_t,size_t>> >& pq)
{
    std::priority_queue<std::pair<size_t,size_t>, std::vector<std::pair<size_t,size_t>>, std::greater<std::pair<size_t,size_t>> > p=pq;
    while(!p.empty())
    {
        std::pair<size_t,size_t> pr=p.top();
        std::cout << "(w: " << pr.first << " id: " << pr.second << ") ";
        p.pop();
    }    
    std::cout << "\n";
}


template<class graphType, class vecType>
std::vector<size_t> shortestPathSampler<graphType, vecType>::getPath(std::vector<bool>& blockedVertices,
                                                            std::vector<size_t>& distances,
                                                            std::vector<size_t>& previousNeighbor)
{
    size_t maxDist=0;
    size_t idx=0;
    for(size_t i=0;i<blockedVertices.size();++i)
    {
        if(!blockedVertices[i])
        {
            if(maxDist<=distances[i]&& distances[i]!=uintInf)
            {
                idx=i;
                maxDist=distances[i];
            }
        }   
    }

    std::vector<size_t> path;
    path.push_back(idx);

    while(1)
    {
        idx=previousNeighbor[idx];
        if(idx==uintInf)
        {
            break;
        }
        path.push_back(idx);
    }
    return path;
}

template<class graphType, class vecType>
std::vector<chain<vecType>> shortestPathSampler<graphType,vecType>::sampleGraph()
{
    std::vector<bool> edgesDone(grPtr->m_nE,false);
    size_t numEdgesDone=0;
    std::vector<chain<vecType>> chains;
    while(numEdgesDone<grPtr->m_nE)
    {
        size_t srcNode= computeSourceNode();
        chain<vecType> ch=sampleChain(srcNode);
        chains.push_back(ch);
        removeChainFromAdjMap(ch);          //  This should probably be removed. 
        // std::cout << ch;
        for(auto& e:ch.edges)
        {
            if(!edgesDone[e.id])
            {
                edgesDone[e.id]=true;
                numEdgesDone++;
            }
        }
    }
    return chains;
}


/*!
    This has the process of sampling one chain.
 */
template<class graphType, class vecType>
chain<vecType> shortestPathSampler<graphType,vecType>::sampleChain(
                                                const size_t srcNode)
{
    //  This is done per chain. This is not good.
    std::vector<size_t> distances(grPtr->m_nV,uintInf);
    //  This is also done per chain?
    std::vector<bool> blockedVertices(grPtr->m_nV,false);
    
    std::vector<size_t> previousNeighbor(grPtr->m_nV,uintInf);
    
    std::vector<bool> visited(grPtr->m_nV,false);

    //  distances are the priority
    std::priority_queue<std::pair<size_t,size_t>,
                    std::vector<std::pair<size_t,size_t> >,
                    std::greater<std::pair<size_t,size_t>> > pq;

    //  
    pq.push(std::make_pair(0,srcNode));
    distances[srcNode]=0;
    std::vector<size_t>::iterator vIt;


    bool growthPossible=true;
    
    //  for all the chains for which growth is possible.
    while(growthPossible)
    {
        if(pq.size()==0)
        {
            growthPossible=false;
            break;
        }

        size_t currNode=pq.top().second;
        pq.pop();
        
        //  if current node is already visited continue.
        if(visited[currNode]) continue;
        // if(blockedVertices[currNode])continue;
        visited[currNode]=true;

        //  go through the neighbors of the node
        for(auto x: adjMap[currNode])
        {
            size_t nbrID=x.ndID;
            //  If the node has been visited
            //  or the vertices are blocked.
            //  A node is blocked if 
            //  
            if(visited[nbrID] || blockedVertices[nbrID])
            {
                continue;
            }

            size_t weight=1;
            //  this statement checks if the current path is unique
            //  there may be another path to the neighboring node.
            //  
            if(distances[nbrID]>distances[currNode]+1)
            {
                distances[nbrID]=distances[currNode]+weight;
                previousNeighbor[nbrID]=currNode;
                pq.push(std::make_pair(distances[nbrID],nbrID));
            }
            else if(distances[nbrID]==distances[currNode]+1)
            {
                blockedVertices[nbrID]=true;
            }
        }
    }

    std::vector<size_t> path=getPath(blockedVertices,
                                     distances,
                                     previousNeighbor);

    chain<vecType> ch=createChainFromPath(path);

    return ch;

}

template<class graphType, class vecType>
chain<vecType> shortestPathSampler<graphType,vecType>::createChainFromPath(std::vector<size_t>& path)
{
    chain<vecType> ch;
    std::reverse(path.begin(),path.end());

    ch.setHead(path[0]);
    
    for(size_t i=1;i<path.size();++i)
    {
        const size_t eID=grPtr->getEdgeID(path[i-1],path[i]);
        ch.addToTail(path[i],eID);
    }
    
    // std::cout << "Chain\n";
    // std::cout << ch;
    return ch;
}

/*!
    for each edge in the chain remove the element from both node adjacenct lists.
    if a node completely empty, then remove the node.
 */
template<class graphType, class vecType>
inline void shortestPathSampler<graphType,vecType>::removeChainFromAdjMap(const chain<vecType>& ch)
{
    std::vector<edgeHalf>::iterator vIt;
    for(size_t e=0;e<ch.edges.size();++e)
    {
        // std::cout << "s: " << ch.edges[e].s << " t: " << ch.edges[e].t << "\n";
        size_t s=ch.edges[e].s;
        size_t t=ch.edges[e].t;
        adjacencyVector& from_aV=adjMap[ch.edges[e].s];

        size_t idx=0;
        for(size_t j=0;j<from_aV.size();++j)
        {
            if(from_aV[j].ndID==t)
            {
                idx=j;
                break;
            }
        }

        vIt=from_aV.begin()+idx;
        from_aV.erase(vIt);
        
        adjacencyVector& to_aV=adjMap[ch.edges[e].t];

        idx=0;
        for(size_t j=0;j<to_aV.size();++j)
        {
            if(to_aV[j].ndID==s)
            {
                idx=j;
                break;
            }
        }
        vIt=to_aV.begin()+idx;
        to_aV.erase(vIt);

        if(to_aV.size()==0)
        {
            adjMap.erase(t);
            extinguishedNodes[t]=true;
        }

        if(from_aV.size()==0)
        {
            adjMap.erase(s);
            extinguishedNodes[s]=true;
        }

    }

}

template<class graphType, class vecType>
void shortestPathSampler<graphType, vecType>::printNodeOnlyAdjMap()
{
    std::vector<edgeHalf> eHvec;
    std::map<size_t,adjacencyVector>::iterator mIt;
    for(mIt=adjMap.begin();mIt!=adjMap.end();++mIt)
    {
        std::cout << mIt->first << " (";
        eHvec=mIt->second;
        for(size_t j=0;j<eHvec.size();++j)
        {
            std::cout << eHvec[j].ndID << " ";
        }
        std::cout << ")\n";
    }
}

template<class graphType, class vecType>
void shortestPathSampler<graphType, vecType>::printEdgeOnlyAdjMap()
{
    std::vector<edgeHalf> eHvec;
    std::map<size_t,adjacencyVector>::iterator mIt;
    for(mIt=adjMap.begin();mIt!=adjMap.end();++mIt)
    {
        std::cout << mIt->first << " (";
        eHvec=mIt->second;
        for(size_t j=0;j<eHvec.size();++j)
        {
            std::cout << eHvec[j].eID << " ";
        }
        std::cout << ")\n";
    }
}

};