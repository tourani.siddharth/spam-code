#pragma once

#include "config.h"
#include "subGraph.h"

namespace Solver
{
		//	some node class here
	/*!
		Alternative structure 
	*/
	template<class vecType>
	class chainNode
	{
		using vectorizer=typename  vecType::alignedVec;     //	
        using T=typename  vecType::scalar;	                //	

		public:
		size_t id;
		size_t inEdgeID;		//!	incoming edge Id
		size_t outEdgeID;		//!	outgoing edge Id

		size_t inNodeID;		//!	incoming node id
		size_t outNodeID;		//!	outgoing node id

		vectorizer* data;

		chainNode(): id(0), inNodeID(0),
					outNodeID(0),inEdgeID(0),
					outEdgeID(0)
		{};

		chainNode(size_t ID): id(ID), inNodeID(0),
							outNodeID(0), inEdgeID(0), 
							outEdgeID(0)
		{

		}


		chainNode(const chainNode& cN): 
			id(cN.id), inNodeID(cN.inNodeID), outNodeID(cN.outNodeID),
			inEdgeID(cN.inEdgeID), outEdgeID(cN.outEdgeID)
		{

		}

		chainNode& operator=(const chainNode& cN)
		{
			id=cN.id;
			inNodeID=cN.inNodeID;
			outNodeID=cN.outNodeID;
			inEdgeID=cN.inEdgeID;
			outEdgeID=cN.outEdgeID;
			return *this;
		}

		friend std::ostream& operator<<(std::ostream& os, chainNode& cN)
		{
			os << "id: " << cN.id << " inNodeID: " << cN.inNodeID;
			os << " outNodeID: " << cN.outNodeID;
			os << " inEdgeID: " << cN.inEdgeID;
			os << " outEdgeID: " << cN.outEdgeID << "\n";
			return os;
		}
	};

	//	should this be part of the aligned memory?
	template<class vecType>
	class chainEdge
	{
		// using vectorizer=typename  vecType::alignedVec;     //	
        using T=typename  vecType::scalar;	                //	
		
		public:
		size_t id;
		size_t s;
		size_t t;
		// size_t subs;
		// size_t subt;

		size_t otherNode(const size_t id)
		{
			if(id==s)	return t;
			else return s;
		}


		chainEdge():id(0),s(0),t(0){};

		chainEdge(const size_t ID):id(ID), s(0), t(0)
		{

		} 

		chainEdge(const size_t ID, 
				  const size_t from,
				  const size_t to): 
				  id(ID), s(from), t(to)
		{

		}

		chainEdge(const chainEdge& cE): 
			id(cE.id), s(cE.s), t(cE.t)
		{

		}

		chainEdge& operator=(const chainEdge& cE)
		{
			id=cE.id;
			s=cE.s;
			t=cE.t;
			return *this;
		}

		friend std::ostream& operator<<(std::ostream& os, chainEdge& cE)
		{
			os << "id: " << cE.id << " s: " << cE.s << " t: " << cE.t << "\n";
			return os;
		}
	};


	//	should this be part of the aligned memory?
	template<typename vecType>
	class chain: public subGraph
	{
		public:
		std::vector<chainNode<vecType>> nodes;
		std::vector<chainEdge<vecType>> edges;

		size_t head;
		size_t tail;
		

		chain(): head(std::numeric_limits<size_t>::infinity()), 
				 tail(std::numeric_limits<size_t>::infinity())
		{

		};

		chain(const std::vector<size_t>& ndIDs, 
			  const std::vector<size_t>& eIDs)
		{
			for(size_t i=0;i<ndIDs.size();++i)
			{
				chainNode<vecType> cN(ndIDs[i]);
				nodes.push_back(cN);
			}
			for(size_t e=0;e<eIDs.size();++e)
			{
				chainEdge<vecType> cE(eIDs[e],ndIDs[e],ndIDs[e+1]);
				edges.push_back(cE);
			}
			head=ndIDs[0];
			tail=ndIDs[ndIDs.size()-1];
		}



		chain(const std::vector<int>& ndIDs, 
			  const std::vector<int>& eIDs)
		{
			for(size_t i=0;i<ndIDs.size();++i)
			{
				chainNode<vecType> cN(ndIDs[i]);
				nodes.push_back(cN);
			}
			for(size_t e=0;e<eIDs.size();++e)
			{
				chainEdge<vecType> cE(eIDs[e],ndIDs[e],ndIDs[e+1]);
				edges.push_back(cE);
			}
			head=ndIDs[0];
			tail=ndIDs[ndIDs.size()-1];
		}


		chain(const chain<vecType>& ch)
		{
			nodes=(ch.nodes);
			edges=(ch.edges);
			head=ch.head;
			tail=ch.tail;
		}

		chain(chain<vecType>&& other)
		{
			nodes=other.nodes;
			edges=other.edges;
			head=other.head;
			tail=other.tail;
		}


		chain<vecType>& operator=(chain<vecType>& ch)
		{
			this->nodes=ch.nodes;
			this->edges=ch.edges;
			this->head=ch.head;
			this->tail=ch.tail;
			return *this;
		}

		//	Move Constructor

		chain<vecType>& operator=(chain<vecType>&& other)
		{
			this->nodes=std::move(other.nodes);
			this->edges=std::move(other.edges);
			this->head=std::move(other.head);
			this->tail=std::move(other.tail);
			return *this;
		}

		void addNode(const chainNode<vecType>& cN);
		void addEdge(const chainEdge<vecType>& cE);

		void addToHead(const size_t ndID, const size_t eID);

		void addToTail(const size_t ndID, const size_t eID);

		void fixChain()
		{
			for(size_t i=0;i<nodes.size();++i)
			{
				if(i==0)
				{
					nodes[i].inNodeID=std::numeric_limits<size_t>::max();
					// nodes[i].outNodeID=nodes[i].id;
				}
				else if(i==nodes.size()-1)
				{
					// nodes[i].inNodeID=nodes[i-1].id;
					nodes[i].outNodeID=std::numeric_limits<size_t>::max();
				}
				else
				{
					// nodes[i].inNodeID=nodes[i-1].id;
					// nodes[i].outNodeID=nodes[i+1].id;
				}
			}

			for(size_t e=0;e<edges.size();++e)
			{
				size_t s=edges[e].s;
				size_t t=edges[e].t;
				size_t id=edges[e].id;

				bool foundS=false;
				for(size_t ii=0;ii<nodes.size();++ii)
				{
					if(nodes[ii].id==s)
					{
						nodes[ii].outNodeID=t;
						nodes[ii].outEdgeID=id;
						foundS=true;
					}
				}

				bool foundT=false;
				for(size_t jj=0;jj<nodes.size();++jj)
				{
					if(nodes[jj].id==t)
					{
						nodes[jj].inNodeID=s;
						nodes[jj].inEdgeID=id;
						foundT=true;
					}
				}

			}

		}
		
		void setHead(const size_t hd);

		bool checkInChain(const size_t ndID,const size_t eID);

		friend std::ostream& operator<<(std::ostream& os, chain& c)
		{
			os << "---------------Printing chain---------------\n";
			os << "head: " << c.head << " tail: " << c.tail << "\n";
			for(size_t i=0;i<c.nodes.size();++i)
			{
				os << "NODE " << c.nodes[i];
			}

			for(size_t e=0;e<c.edges.size();++e)
			{
				os << "EDGE " << c.edges[e];
			}
			os << "---------------------------------------------\n";
			return os;
		}
	};

	template<class vecType>
	void chain<vecType>::addNode(const chainNode<vecType>& cN)
	{
		nodes.push_back(cN);
	}

	template<class vecType>
	void chain<vecType>::addEdge(const chainEdge<vecType>& cE)
	{
		edges.push_back(cE);
	}

	template<class vecType>
	void chain<vecType>::addToTail(const size_t ndID, const size_t eID)
	{
		chainNode<vecType> cN(ndID);
		chainEdge<vecType> cE(eID,tail,ndID);
		addNode(cN);
		addEdge(cE);
		tail=ndID;
	}	

	template<class vecType>
	void chain<vecType>::addToHead(const size_t ndID, const size_t eID)
	{
		chainNode<vecType> cN(ndID);
		chainEdge<vecType> cE(eID,ndID,head);
		// addNode(cN);
		// addEdge(cE);
		nodes.insert(nodes.begin(),cN);
		edges.insert(edges.begin(),cE);
		head=ndID;
	}

	template<class vecType>
	void chain<vecType>::setHead(const size_t hd)
	{
		chainNode<vecType> cN(hd);
		nodes.push_back(hd);
		head=hd;
		tail=hd;
	}

	template<class vecType>
	bool chain<vecType>::checkInChain(const size_t ndID, const size_t eID)
	{
		for(auto& i:nodes)
		{
			if(i.id==ndID)
			{
					return true;
			}
		}

		for(auto& e:edges)
		{
			if(e.id==eID)
			{
				return true;
			}
		}
		return false;
	}

};