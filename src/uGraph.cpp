#include "uGraph.h"

namespace Graph
{
/*	----------------	uGraph     ----------------- */
	void uGraph::init(const size_t nV, const std::vector<edgeEnds>& E)
	{
		m_nV=nV;
		m_nE=E.size();
		m_E=E;

		m_nbrs.resize(nV);
		for(size_t i=0;i<m_E.size();++i)
		{
			edgeEnds& st=m_E[i];
			edgeHalf eH;
			eH.ndID=st.t;
			eH.eID=i;
			edgeHalf eH2;
			eH2.ndID=st.s;
			eH2.eID=i;
			m_nbrs[st.s].push_back(eH);
			m_nbrs[st.t].push_back(eH2);
		}
	}

	size_t uGraph::getEdgeID(const size_t s, const size_t t)
	{
		size_t edgeIndex=uintInf;
		for(size_t i=0;i<m_nbrs[s].size();++i)
		{
			if(m_nbrs[s][i].ndID==t)
			{
				edgeIndex=m_nbrs[s][i].eID;
				break;
			}
		}
		if(edgeIndex==uintInf)
		{
			throw std::runtime_error("In function getEdgeID. Unable to find edge in the graph");
		}

		return edgeIndex;
	}

	adjacencyVector& uGraph::getNbrsToNode(const size_t ndID)
	{
		return m_nbrs[ndID];
	}

};