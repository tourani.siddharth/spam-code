#pragma once
/*!
	@file spam.h
	@author Siddharth Tourani
	@brief The ICHM class is derived from the msgAlgo class.
*/

#include "msgAlgo.h"
#include "alignedAllocator.h"
// #include "solverOptions.h"
#include "testGraphs.h"
#include "simd.h"
#include "graphSampling.h"
#include "chain2Ops.h"
#include "shortestPathSampler.h"
#include "timer.h"
#include "utils.h"
#include "qpbo/qpbo.h"

struct Assignment
{
	int id;
	int left;
	int right;
	double cost;
};

struct EdgeCost
{
	int assID0;
	int assID1;
	double cost;
};


static constexpr double INFTY = 1e20;
enum GRAPH_TYPE{GRID_GRAPH,GENERAL_GRAPH};

namespace Solver
{
	template<class vecType, class graphType>
	class spam: public msgAlgo<vecType,graphType>
	{
		public:
		// using msgAlgo<vecType,graphType> mAlgo;
		typedef typename Solver::msgAlgo<vecType,graphType> parent;
		typedef typename  vecType::alignedVec vectorizer;	//	
		using T=typename  vecType::scalar;					//	

		//!	declarations 
		struct edgeCore;
		struct nodeCore;
		struct nodeInfo;
		struct edgeInfo;


		struct options
		{
			size_t numIters;
			Solver::sampType sampling;
			size_t numGenerations;
			
		};

		/*!	nodeCore */
		struct nodeCore
		{
			vectorizer* data;	//	size of the unary
			int vK;				//	size in vectorizers
			edgeCore* nbrEdges;	//	Should these be here? ? ?
			nodeInfo* n;			//	pointer to the node
			nodeCore(): data(0),vK(0),nbrEdges(0){};
			vectorizer* un() const {return data;};
			edgeCore* nbr() const {return nbrEdges;};
		};	

		/*! edgeCore */
		struct edgeCore
		{
			vectorizer* data;
			nodeCore* nHead;
			nodeCore* nTail;
			//	Let K1 and K2 be the lengths of the pairwise term.
			int vK1;		//	vectorized K1
			int vK2;		//	vectorized K2
			int K1;			//	
			int K2;			//

			edgeCore(): data(0),nHead(0),nTail(0){};
			const nodeCore* tail() const
			{
				return nTail;
			}

			const nodeCore* head() const
			{
				return nHead;
			}
		};

		/*! nodeInfo */
		struct nodeInfo
		{
			int K;
			int id;
			nodeCore* core;
			std::vector<edgeInfo*> nbrs;
		};

		/*! edgeInfo */
		struct edgeInfo
		{
			int s;
			int t;
			edgeCore* core;
			nodeInfo* head;
			nodeInfo* tail;
			edgeInfo(): core(0),head(0),tail(0){};
			//	get the data out
		};

		void testFunction()
		{
			std::cout << "ICHM Class\n";
		}

	public:


		alignedAllocator mAl;							//	
		std::vector<nodeInfo> mNodes;					//	
		std::vector<edgeInfo> mEdges;					//	
		std::vector<chain<vecType>> mChains;			//	
		std::vector<std::vector<Operation<T>>> mChainOperations;//	
		std::vector<size_t> mLabelling;
		options mOpts;
		std::shared_ptr<graphType> grPtr;
		T* dualPtr;
		T primalE;
		int numMessagesPassed;
		int m_nV, m_nE;

		spam(Graph::graphStorage<T,graphType>& gS,
			 options& opts): msgAlgo<vecType,graphType>(gS.G),		
				mOpts(opts),
				m_nV(gS.G.m_nV),
				m_nE(gS.G.m_nE),
				mLabelling(gS.G.m_nV,0),
				numMessagesPassed(0)
		{
			grPtr=std::make_shared<graphType>(gS.G);
			std::cout << "nV: " << parent::mG->m_nV << " nE: " << parent::mG->m_nE << "\n";
			mNodes.resize(parent::mG->m_nV);
			mEdges.resize(parent::mG->m_nE);
			allocateMemory(gS);

			std::cout << dual() << "\n";

			Timer timer;
			timer.start();
			sampleGraph();
			timer.stop();
			std::cout << "Time taken to sample Graph: " << timer.elapsedSeconds() << "\n";
			timer.start();
			computeOperations();
			timer.stop();
			std::cout << "Time taken to compute operations: " << timer.elapsedSeconds() << "\n";
		}
		
		//!	Done with the allocating memory
		void allocateMemory(Graph::graphStorage<T,graphType>& gS);	//	
		void sampleGraph();											//	
		void infer();							//	
		void computeOperations();									//	
		void runIteration();										//	
		T dual();													//	
		void computeLabelling();

		T primal();
		T primalGreedy();

		T computeEnergy(std::vector<size_t>& labelling);

		void computeLabelling2();

		std::vector<size_t> computeLabellingByGreedyRounding();

		void messagePrimal(std::vector<typename vecType::scalar>& un,const size_t unID,  const size_t eID, const size_t winLabelFrom);

		void messageTPrimal(std::vector<typename vecType::scalar>& un,const size_t unID,  const size_t eID, const size_t winLabelFrom);

		size_t pickRandomVertexPrimal(const std::set<size_t>& unprocessedVertices);

		size_t pickRandomVertexPrimal2(const std::vector<size_t>& unprocessedVertices);



		std::set<size_t> getNbrsAsSet(const size_t unID);
		std::vector<edgeHalf> getProcessedNeighbors(const std::set<size_t>& processedVertices, const size_t unID);

		std::vector<size_t> buildAndSolveAuxiliaryProblem(std::vector<size_t>& labelling1,std::vector<size_t>& labelling2);
		T primalFusionMoves(const size_t numGenerations);

		void writeToDDFile(std::string& filename);

		//!	Message Passing Operations
		void diffuse(const Operation<T>& op);
		void fractionalMessage(const Operation<T>& op, const T r=1);
		void message(const Operation<T>& op);
		void messageT(const Operation<T>& op);
		void handshake(const Operation<T>& op);
		void computeGraphAdjacencyList();
		size_t pickRandomNeighbor(size_t vtxID,std::set<size_t>& processedVertices);
		size_t  pickRandomNeighbor2(size_t vtxID, std::vector<bool>& processedVertices);

	};

	template<class vecType, class graphType>
	void spam<vecType,graphType>::sampleGraph()
	{
		graphSampling<graphType,vecType> gS(*(parent::mG),mOpts.sampling);
		
		switch(mOpts.sampling)
		{
			// case Solver::SPANNING_TREES:
			// 	std::cout << BOLDBLUE << "Spanning Tree Decomposition\n" << RESET;
			// 	// mTrees=gS.spanningTreeDecomposition();
			// 	break;
			
			// case Solver::ARC_INCONSISTENT:
			// 	std::cout << BOLDBLUE << "Arc Inconsistent Decomposition\n" << RESET;
			// 	// mTrees=gS.arcInconsistentDecomposition();
			// 	break;

			// case Solver:: INDUCED_CHAINS:
			// 	std::cout << BOLDRED << "Haven't implemented induced chains yet\n" << RESET;
			// 	break;

			// case Solver::ROW_COL:	
			// 	std::cout << BOLDBLUE << "Row-Col Decomposition\n" << RESET;
			// 	mChains=gS.rowColDecomposition();
			// 	break;

			case Solver::SHORTEST_PATH:
				std::cout << BOLDBLUE << "Shortest Path Decomposition\n" << RESET;
				mChains=gS.shortestPathDecomposition3();
				break;

			// case Solver::MAX_MONO_CHAINS:
			// 	std::cout << BOLDBLUE << "Maximal Monotonic Chains\n" << RESET;
			// 	mChains=gS.MMCDecomposition();
			// 	break;

			// case Solver::ROW_EDGE:
			// 	std::cout << BOLDBLUE << "Row-Edge\n" << RESET;
			// 	mChains=gS.rowEdgeDecomposition();
			// 	break;

			// case Solver::EDGE:
			// 	mChains=gS.edgeDecomposition();
			// 	break;
		}
		
	}

	/*!
		Allocate memory. first the unaries and then the pairwise terms for a start, 
		then, we will see if something can be gained by different alignments.
	 */
	template<class vecType, class graphType>
	void spam<vecType,graphType>::allocateMemory(Graph::graphStorage<T,graphType>& gS)
	{
		/*!
			Memory is laid out in a very particular manner.
			nodeCore contains information about the node.
			Amount allocated is size of the unary measured in vectorized values.
			Then edgeCore is allocated.
			Then, aligned memory for pw terms are allocated.
		*/
		for(size_t n=0;n<gS.G.m_nV;++n)
		{
			mAl.allocate_a<nodeCore>();
			int V=sizeof(vectorizer)/sizeof(T);
			int K=gS.unaries[n].size();
			//	round to the next multiple of sizeof(vectorizer)//	
			int vK=(K+V-1)/V;
			mAl.allocate_a<vectorizer>(vK);	//	this should change I think
		}

		for(size_t e=0;e<gS.G.m_nE;++e)
		{
			mAl.allocate_a<edgeCore>();
			int V=sizeof(vectorizer)/sizeof(T);			
			int l1=gS.unaries[gS.G.m_E[e].s].size();
			int l2=gS.unaries[gS.G.m_E[e].t].size();
			int vK=l1*((V+l2-1)/V);		//	size in vectorizers.
			mAl.allocate_a<vectorizer>(vK);
		}

		mAl.allocate_a<T>(gS.G.m_nV+gS.G.m_nE);

		mAl.init();

		for(size_t n=0;n<gS.G.m_nV;++n)
		{
			mNodes[n].K=gS.unaries[n].size();
			mNodes[n].core=mAl.allocate_a<nodeCore>();
			mNodes[n].id=n;
			int V=sizeof(vectorizer)/sizeof(T);
			int K=gS.unaries[n].size();
			int vK=(K+V-1)/V;		//	size in vectorizers.

			mNodes[n].core->vK=K;
			mNodes[n].core->data=mAl.allocate_a<vectorizer>(vK);
			mNodes[n].core->n=&mNodes[n];
			
			for(size_t i=0;i<gS.unaries[n].size();++i)
			{
				mNodes[n].core->data[i]=gS.unaries[n][i];
			}
		}

		for(size_t e=0;e<gS.G.m_nE;++e)
		{
			mEdges[e].core=mAl.allocate_a<edgeCore>();
			int s=gS.G.m_E[e].s;
			int t=gS.G.m_E[e].t;
			int V=sizeof(vectorizer)/sizeof(T);
			int l1=gS.unaries[gS.G.m_E[e].s].size();
			int l2=gS.unaries[gS.G.m_E[e].t].size();
			int vK=((V+l2-1)/V);
		
			mEdges[e].core->data=mAl.allocate_a<vectorizer>(l1*vK);
			mEdges[e].s=s;
			mEdges[e].t=t;	
			mEdges[e].core->nHead=mNodes[s].core;
			mEdges[e].core->nTail=mNodes[t].core;
			mEdges[e].core->K1=l1;
			mEdges[e].core->K2=l2;
			mEdges[e].core->vK1=l1;
			mEdges[e].core->vK2=(V+l2-1)/V;
			mEdges[e].head=&mNodes[s];
			mEdges[e].tail=&mNodes[t];

			for(size_t i=0;i<l1;++i)
			{
				for(size_t j=0;j<l2;++j)
				{
					mEdges[e].core->data[i*vK+j]=gS.pairwise[e][i*l2+j];
				}
			}
		
		}

		dualPtr=mAl.allocate_a<T>(gS.G.m_nV+gS.G.m_nE);
	}

	template<class vecType,class graphType>
	void spam<vecType,graphType>::infer()
	{
		using T=typename vecType::scalar;
		primalE=std::numeric_limits<T>::max();
		Timer t;
		t.start();
		T D=dual();
		// primalE=primalFusionMoves(mOpts.numGenerations);
		primalE=primal();
		std::cout << -1  << " " << numMessagesPassed << " " << D << " " << primalE <<  "\n";
		size_t i=0;
		for(size_t i=1;i<=mOpts.numIters;++i)
		{
			runIteration();
			D=dual();
			primalE=primalFusionMoves(mOpts.numGenerations);
			// primalE=primal();
			t.stop();
		 	T time=t.elapsedSeconds();
		 	std::cout << i  << " " << numMessagesPassed << " " << D << " " << primalE <<  "\n";
		}
	}

	template<class vecType, class graphType>
	void spam<vecType,graphType>::writeToDDFile(std::string& filename)
	{
		std::vector<Assignment> assignments;
		std::vector<EdgeCost> edgeCosts;

		std::vector<std::vector<int>> unaries2AssID(m_nV);
		for(size_t i=0;i<unaries2AssID.size();++i)
		{
			unaries2AssID[i].resize(mNodes[i].K);
		}

		int assID=0;
		for(auto& nd:mNodes)
		{
			int id=nd.id;
			for(size_t j=0;j<nd.K;++j)
			{
				Assignment ass;
				ass.id=assID;
				ass.left=id;
				ass.right=j;
				ass.cost=nd.core->data[j];
				assignments.push_back(ass);
				unaries2AssID[id][j]=assID;
				assID++;
			}
		}

		for(auto& e:mEdges)
		{
			int s=e.s;
			int t=e.t;
			for(size_t uu=0;uu<e.core->K1;++uu)
			{
				int vK2=e.core->vK2;
				for(size_t vv=0;vv<e.core->K2;++vv)
				{
					double C=e.core->data[uu*vK2+vv];
					EdgeCost e;
					e.assID0=unaries2AssID[s][uu];
					e.assID1=unaries2AssID[t][vv];
					e.cost=C;
					edgeCosts.push_back(e);
				}
			}
		}


		std::ofstream fid(filename);

		//	First line.
		fid << "p " << mNodes.size() << " " << mNodes[0].K << " ";
		fid  << assignments.size() << " " << edgeCosts.size() << "\n";

		for(auto& ass: assignments)
		{
			fid << "a " << ass.id << " " << ass.left << " " << ass.id << " " << ass.cost << "\n";
		}

		for(auto& eC: edgeCosts)
		{
			fid << "e " << eC.assID0 << " " << eC.assID1 << " " << eC.cost << "\n";
		}

		fid.close();
	}

	template<class vecType, class graphType>
	typename vecType::scalar spam<vecType,graphType>::dual()
	{
		using T=typename vecType::scalar;
		for(size_t i=0;i<mNodes.size();++i)
		{
			nodeInfo& nd=mNodes[i];
			int K=nd.K;
			dualPtr[i]=*std::min_element(nd.core->data,nd.core->data+K);
		}
		for(size_t e=0;e<mEdges.size();++e)
		{
			edgeInfo& ed=mEdges[e];
			int K1=ed.core->K1;
			int K2=ed.core->K2;
			int vK2=ed.core->vK2;
			dualPtr[m_nV+e]=*std::min_element(ed.core->data,ed.core->data+K1*vK2);
		}
		T D=std::accumulate(dualPtr,dualPtr+m_nV+m_nE,0.0,std::plus<T>());
		return D;
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::runIteration()
	{
		for(size_t i=0;i<mChainOperations.size();++i)
		{
			for(size_t j=0;j<mChainOperations[i].size();++j)
			{
				switch(mChainOperations[i][j].type)
				{
					//	Msg Operation
					case Operation<T>::MSG:
						numMessagesPassed++;
						message(mChainOperations[i][j]);

						break;

					//	Inverse Msg Operation
					case Operation<T>::MSG_T:
						numMessagesPassed++;
						messageT(mChainOperations[i][j]);
						break;

					//	Handshake Operation
					case Operation<T>::HANDSHAKE:
						numMessagesPassed+=3;
						handshake(mChainOperations[i][j]);
						break;

					//	Diffuse Operation
					case Operation<T>::DIFFUSE:
						diffuse(mChainOperations[i][j]);
						break;

					//	Fractional Operation
					case Operation<T>::FRACTION:
						numMessagesPassed++;
						// fractionalMessage(chainOperations[i][j],chainOperations[i][j].frac);
						break;

					//	None Operation
					case Operation<T>::NONE:
						break;

					default:
						throw std::runtime_error("unable to find ...\n");
						break;

				}
			}
		}
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::handshake(const Operation<T>& op)
	{
		if(op.base<op.nbrs[0].ndID)
		{
			int id1=op.base;
			int id2=op.nbrs[0].ndID;

			nodeInfo& un1Nd=mNodes[op.base];
			nodeInfo& un2Nd=mNodes[op.nbrs[0].ndID];
			edgeInfo& ed=mEdges[op.nbrs[0].eID];

			vectorizer* un1Data=(un1Nd.core->data);
			vectorizer* un2Data=(un2Nd.core->data);
			vectorizer* eData=(ed.core->data);

			int K1=ed.core->K1;
			int K2=ed.core->K2;
			int vK2=ed.core->vK2;

			for(size_t i=0;i<K1;++i)
			{
				for(size_t j=0;j<K2;++j)
				{
					eData[i*vK2+j]+=un1Data[i]+un2Data[j];
				}
			}

			for(size_t i=0;i<K1;++i)
			{
				T minVal=eData[i*vK2+0];
				for(size_t j=1;j<K2;++j)
				{
					if(minVal>eData[i*vK2+j])
					{
						minVal=eData[i*vK2+j];
					}
				}

				un1Data[i]=minVal*0.5;
				for(size_t j=0;j<K2;++j)
				{
					eData[i*vK2+j]-=minVal*0.5;
				}
			}


			for(size_t j=0;j<K2;++j)
			{
				T minVal=eData[0*vK2+j];
				for(size_t i=1;i<K1;++i)	
				{
					if(minVal>eData[i*vK2+j])
					{
						minVal=eData[i*vK2+j];
					}
				}
				un2Data[j]=minVal;
				for(size_t i=0;i<K1;++i)	
				{
					eData[i*vK2+j]-=minVal;
				}
			}

			for(size_t i=0;i<K1;++i)
			{
				T minVal=eData[i*vK2+0];
				for(size_t j=1;j<K2;++j)
				{
					if(minVal>eData[i*vK2+j])
					{
						minVal=eData[i*vK2+j];
					}
				}
				un1Data[i]+=minVal;
				for(size_t j=0;j<K2;++j)
				{
					eData[i*vK2+j]-=minVal;
				}
			}
		}
		else
		{
			int id1=op.nbrs[0].ndID;
			int id2=op.base;
		}
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::messageT(const Operation<T>& op)
	{
		nodeInfo& un1Nd=mNodes[op.base];
		nodeInfo& un2Nd=mNodes[op.nbrs[0].ndID];
		edgeInfo& ed=mEdges[op.nbrs[0].eID];

		vectorizer* un1Data=un1Nd.core->data;
		vectorizer* un2Data=un2Nd.core->data;
		vectorizer* eData=ed.core->data;
		
		size_t K2=ed.core->K1;
		size_t K1=ed.core->K2;
		size_t vK2=ed.core->vK2;

		for(size_t i=0;i<K1;++i) {
			for(size_t j=0;j<K2;++j) {
				eData[j*vK2+i]+=un1Data[i];
			}
			un1Data[i]=0;
		}

		for(size_t j=0;j<K2;++j)
		{
			T minVal=eData[j*vK2+0];
			for(size_t i=1;i<K1;++i)		
			{
				if(minVal>eData[j*vK2+i])
				{
					minVal=eData[j*vK2+i];
				}
			}
			un2Data[j]+=minVal;
			for(size_t i=0;i<K1;++i)
			{
				eData[j*vK2+i]-=minVal;
			}
		}
	}

	template<class vecType, class graphType>
	void spam<vecType, graphType>::messagePrimal(std::vector<typename vecType::scalar>& un,const size_t unID,  const size_t eID, const size_t winLabelFrom)
	{
		assert(mEdges[eID].t==unID);

		/*!	K is the length of the unary */
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].core->vK;

		/*!			*/
		for(size_t j=0;j<K;++j)		{
			un[j]+=(mEdges[eID].core->data[winLabelFrom*vK+j]);		
		}
	}

	template<class vecType, class graphType>
	void spam<vecType, graphType>::messageTPrimal(std::vector<typename vecType::scalar>& un,const size_t unID,  const size_t eID, const size_t winLabelFrom)
	{
		assert(mEdges[eID].s==unID);

		//	K is the length of the unary
		size_t K=mNodes[unID].K;
		size_t vK=mNodes[unID].core->vK;

		for(size_t j=0;j<K;++j)
		{
			un[j]+=mEdges[eID].core->data[j*vK+winLabelFrom];
		}
	}

	template<class vecType,class graphType>
	inline void spam<vecType,graphType>::message(const Operation<T>& op)
	{	
		int id1=op.base;
		int id2=op.nbrs[0].ndID;
		int eID=op.nbrs[0].eID;
		nodeInfo& un1Nd=mNodes[op.base];
		nodeInfo& un2Nd=mNodes[op.nbrs[0].ndID];
		edgeInfo& ed=mEdges[op.nbrs[0].eID];
		vectorizer* un1Data=un1Nd.core->data;
		vectorizer* un2Data=un2Nd.core->data;
		vectorizer* eData=ed.core->data;
		size_t K1=ed.core->K1,K2=ed.core->K2,vK2=ed.core->vK2;

		for(size_t i=0;i<K1;++i){
			for(size_t j=0;j<K2;++j){
				eData[i*vK2+j]+=un1Data[i];
			}
			un1Data[i]=0;
		}

		for(size_t j=0;j<K2;++j){
			T minVal=eData[0*vK2+j];
			for(size_t i=1;i<K1;++i){
				if(minVal>eData[i*vK2+j]){
					minVal=eData[i*vK2+j];
				}
			}
			for(size_t i=0;i<K1;++i){
				eData[i*vK2+j]-=minVal;
			}
			un2Data[j]+=minVal;
		}
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::fractionalMessage(const Operation<T>& op, const T r)
	{
		// line();
		// std::cout << BOLDRED << "Fractional message from: " << op.base << " and " << op.nbrs[0].ndID << " via " << op.nbrs[0].eID << " ratio: " << r << "\n" << RESET;
		int id1=op.base;
		int id2=op.nbrs[0].ndID;
		int eID=op.nbrs[0].eID;

		if(id1<id2)
		{
			nodeInfo& un1Nd=mNodes[op.base];
			nodeInfo& un2Nd=mNodes[op.nbrs[0].ndID];
			edgeInfo& ed=mEdges[op.nbrs[0].eID];

			vectorizer* un1Data=un1Nd.core->data;
			vectorizer* un2Data=un2Nd.core->data;
			vectorizer* eData=ed.core->data;
		
			size_t K1=ed.core->K1;
			size_t K2=ed.core->K2;
			size_t vK2=ed.core->vK2;

			// std::cout << BOLDRED << "BEFORE\n" << RESET;
			// std::cout << "un1: ";
			// for(size_t i=0;i<K1;++i)	std::cout << un1Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "un2: ";
			// for(size_t i=0;i<K2;++i)	std::cout << un2Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "PW\n";
			// for(size_t i=0;i<K1;++i)
			// {
			// 	for(size_t j=0;j<K2;++j)
			// 	{
			// 		std::cout << eData[i*vK2+j] << " ";
			// 	}
			// 	std::cout << "\n";
			// }
			// std::cout << "\n";


			for(size_t i=0;i<K2;++i)
			{
				for(size_t j=0;j<K1;++j)
				{
					eData[i*vK2+j]+=r*un1Data[i];
				}
				un1Data[i]=(1-r)*un1Data[i];
			}


			for(size_t j=0;j<K2;++j)
			{
				T minVal=eData[0*vK2+j];
				for(size_t i=1;i<K1;++i)
				{
					if(minVal>eData[i*vK2+j])
					{
						minVal=eData[i*vK2+j];
					}
				}
				for(size_t i=0;i<K1;++i)
				{
					eData[i*vK2+j]-=minVal;
				}
				un2Data[j]+=minVal;
			}

			// std::cout << BOLDRED << "AFTER\n" << RESET;
			// std::cout << "un1: ";
			// for(size_t i=0;i<K1;++i)	std::cout << un1Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "un2: ";
			// for(size_t i=0;i<K2;++i)	std::cout << un2Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "PW\n";
			// for(size_t i=0;i<K1;++i)
			// {
			// 	for(size_t j=0;j<K2;++j)
			// 	{
			// 		std::cout << eData[i*vK2+j] << " ";
			// 	}
			// 	std::cout << "\n";
			// }
			// std::cout << "\n";

		}
		else
		{
			nodeInfo& un1Nd=mNodes[op.base];
			nodeInfo& un2Nd=mNodes[op.nbrs[0].ndID];
			edgeInfo& ed=mEdges[op.nbrs[0].eID];

			vectorizer* un1Data=un1Nd.core->data;
			vectorizer* un2Data=un2Nd.core->data;
			vectorizer* eData=ed.core->data;
		
			size_t K2=ed.core->K1;
			size_t K1=ed.core->K2;
			size_t vK2=ed.core->vK2;

			// std::cout << "un1: ";
			// for(size_t i=0;i<K1;++i)	std::cout << un1Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "un2: ";
			// for(size_t i=0;i<K2;++i)	std::cout << un2Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "PW\n";
			// for(size_t j=0;j<K2;++j)
			// {
			// 	for(size_t i=0;i<K1;++i)	
			// 	{
			// 		std::cout << eData[j*vK2+i] << " ";
			// 	}
			// 	std::cout << "\n";
			// }
			// std::cout << "\n";

			for(size_t i=0;i<K1;++i)
			{
				for(size_t j=0;j<K2;++j)
				{
					eData[j*vK2+i]+=r*un1Data[i];
				}
				un1Data[i]=(1-r)*un1Data[i];
			}

			for(size_t j=0;j<K2;++j)
			{
				T minVal=eData[j*vK2+0];
				for(size_t i=1;i<K1;++i)		
				{
					if(minVal>eData[j*vK2+i])
					{
						minVal=eData[j*vK2+i];
					}
				}
				un2Data[j]+=minVal;
				for(size_t i=0;i<K1;++i)
				{
					eData[j*vK2+i]-=minVal;
				}
			}

			// std::cout << "un1: ";
			// for(size_t i=0;i<K1;++i)	std::cout << un1Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "un2: ";
			// for(size_t i=0;i<K2;++i)	std::cout << un2Data[i] << " ";
			// std::cout << "\n";

			// std::cout << "PW\n";
			// for(size_t j=0;j<K2;++j)
			// {
			// 	for(size_t i=0;i<K1;++i)	
			// 	{
			// 		std::cout << eData[j*vK2+i] << " ";
			// 	}
			// 	std::cout << "\n";
			// }
			// std::cout << "\n";

		}
		// line();
	}

	template<class vecType,class graphType>
	inline void spam<vecType,graphType>::diffuse(const Operation<T>& op)
	{
		std::cout << BOLDRED << "Diffusion from: " << op.base << " to: ";
		for(size_t i=0;i<op.nbrs.size();++i)
		{
			std::cout << "(" << op.nbrs[i].ndID << " via " << op.nbrs[i].eID << ") ";
		}
		std::cout << "\n" << RESET;
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::computeOperations()
	{
		if(mChains.size()>0)
		{
			Solver::Chain2Ops<vecType> ch2Ops(mChains);
			mChainOperations=ch2Ops.computeOperationsForAllChains();
		}
	}

	template<class vecType, class graphType>
	inline void spam<vecType,graphType>::computeGraphAdjacencyList()
	{
		for(auto& edge:mEdges)
		{
			std::cout << "s: "<< edge.s << " t: " << edge.t << "\n";
		}
	}

	template<class vecType, class graphType> 
	size_t spam<vecType,graphType>::pickRandomNeighbor(size_t vtxID,std::set<size_t>& processedVertices)
	{
		size_t pickedNbr=std::numeric_limits<size_t>::max();
		std::set<size_t> nbrsSet=getNbrsAsSet(vtxID);
		std::set<size_t> setIntersect;
		std::set_intersection(processedVertices.begin(),processedVertices.end(),
										nbrsSet.begin(),nbrsSet.end(), 
										 std::inserter(setIntersect,setIntersect.begin()));


		/*!	DEBUG CODE
			std::set<size_t>::iterator setIt;
			std::cout << "Intersection of nbrs of " << vtxID << " and processedVertices size: " << setIntersect.size() << "\n";
			std::cout << "Intersection of nbrs of " << vtxID << " and processedVertices: ";
			for(setIt=setIntersect.begin();setIt!=setIntersect.end();++setIt)
			{
				std::cout << (*setIt) << " ";
			}
			std::cout << "\n";
		*/
		
		size_t chosenVtx=std::numeric_limits<size_t>::max();
		if(setIntersect.size()>0)	{
			chosenVtx=pickRandomVertexPrimal(setIntersect);
		}		
		return chosenVtx;
	}

	template<class vecType, class graphType>
	size_t  spam<vecType,graphType>::pickRandomNeighbor2(size_t vtxID, std::vector<bool>& processedVertices)
	{
		size_t pickedNbr=sizetInf;

		/*!	get the adjacency list of the graph */
		const adjacencyVector& adVec=grPtr->getNbrsToNode(vtxID);

		/*!	store the processed neighbors in a vector */
		std::vector<size_t> unprNbrs;
		for(size_t i=0;i<adVec.size();++i)
		{
			if(!processedVertices[adVec[i].ndID])	{
				unprNbrs.push_back(adVec[i].ndID);
			}
		}

		/*! pick a random neighbor from the vector of processed neighbors	*/
		if(unprNbrs.size()>0)
		{
			std::random_device dev;
	    	std::mt19937 rng(dev());	
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprNbrs.size()-1); // distribution in range [1, 6]
			auto it = unprNbrs.cbegin();
    		std::advance(it,dist6(rng));
    		pickedNbr=(*it);	
		}
		return pickedNbr;
	}

	/* */
	template<class vecType, class graphType>
	size_t spam<vecType,graphType>::pickRandomVertexPrimal2(const std::vector<size_t>& unprocessedVertices)
	{
		using T=typename vecType::scalar;
		size_t vtx=std::numeric_limits<T>::max();
		if(unprocessedVertices.size()>0)
		{
			std::random_device dev;
	    	std::mt19937 rng(dev());	
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprocessedVertices.size()-1); // distribution in range [1, 6]
			auto it = unprocessedVertices.cbegin();
    		std::advance(it,dist6(rng));
    		vtx=(*it);	
		}
		return vtx;
	}


	template<class vecType,class graphType>
	size_t spam<vecType, graphType>::pickRandomVertexPrimal(const std::set<size_t>& unprocessedVertices)
	{
		using T=typename vecType::scalar;
		size_t vtx=std::numeric_limits<T>::max();

		if(unprocessedVertices.size()>0)
		{
			std::random_device dev;
	    	std::mt19937 rng(dev());	
    		std::uniform_int_distribution<std::mt19937::result_type> dist6(0,unprocessedVertices.size()-1); // distribution in range [1, 6]
			auto it = unprocessedVertices.cbegin();
    		std::advance(it,dist6(rng));
    		vtx=(*it);	
		}
		return vtx;
	}

	template<class vecType,class graphType>
	std::set<size_t> spam<vecType,graphType>::getNbrsAsSet(const size_t unID)
	{
		std::set<size_t>nbrsAsSet;
		const adjacencyVector& nbrList=grPtr->getNbrsToNode(unID);
		for(size_t i=0;i<nbrList.size();++i)
		{
			nbrsAsSet.insert(nbrList[i].ndID);
		}
		return nbrsAsSet;
	}

	template<class vecType, class graphType>
	std::vector<edgeHalf> spam<vecType,graphType>::getProcessedNeighbors(const std::set<size_t>& processedVertices, const size_t unID)
	{
		std::vector<edgeHalf> processedNeighbors;
		std::set<edgeHalf>::const_iterator it;
		const adjacencyVector& nbrList=grPtr->getNbrsToNode(unID);
		for(size_t i=0;i<nbrList.size();++i)	{

			if(processedVertices.find(nbrList[i].ndID)!=
						processedVertices.end())	{
				processedNeighbors.push_back(nbrList[i]);
			}
		}
		return processedNeighbors;
	}

	template<class vecType, class graphType>
	typename vecType::scalar spam<vecType,graphType>::primalFusionMoves(const size_t numGenerations)
	{
		using T=typename vecType::scalar;

		//	For each of the num generations. 
		//	Build the auxiliary problem
		//	Solve the QPBO Problem
		//	Set the labelling.
		auto l1=computeLabellingByGreedyRounding();
		T bestEnergy=computeEnergy(l1);

		for(size_t i=0;i<numGenerations;++i)
		{
			auto l2=computeLabellingByGreedyRounding();
			l1=buildAndSolveAuxiliaryProblem(l1,l2);

			T currEnergy=computeEnergy(l1);
			if(bestEnergy>currEnergy)
			{
				bestEnergy=currEnergy;
				
			}
		}
		
		if(primalE>bestEnergy)
		{
			mLabelling=l1;
			primalE=bestEnergy;
		}
		return primalE;
	}


	template<class vecType, class graphType>
	typename vecType::scalar spam<vecType,graphType>::computeEnergy(std::vector<size_t>& labelling)
	{
		using T=typename vecType::scalar;

		/*!	Set up energy	*/
		T energy=0;

		/*!		calculate unary energy	*/
		for(auto& nd:mNodes)	{
			energy+=nd.core->data[labelling[nd.id]];
		}

		/*!		calculate pairwise energy	*/
		for(auto& ed:mEdges)	{
			energy+=ed.core->data[labelling[ed.s]*ed.core->vK2+labelling[ed.t]];
		}
		return energy;
	}

	template<class vecType, class graphType>
	typename std::vector<size_t> spam<vecType,graphType>::buildAndSolveAuxiliaryProblem(
												std::vector<size_t>& labelling1,
												std::vector<size_t>& labelling2)
	{
		using T=typename vecType::scalar;
		qpbo::QPBO<T> q(grPtr->m_nV,grPtr->m_nE);
		q.AddNode(grPtr->m_nV);

		for(size_t i=0;i<grPtr->m_nV;++i)
		{	
			T C0=mNodes[i].core->data[labelling1[i]];
			T C1=mNodes[i].core->data[labelling2[i]];
			if(labelling1[i]!=labelling2[i])
			{
				q.AddUnaryTerm(i,C0,C1);
			}
			else
			{
				q.AddUnaryTerm(i,C0,INFTY);
			}
			
		}
		
		for(size_t e=0;e<m_nE;++e)
		{
			auto& mE=mEdges[e];
			size_t vK2=mE.core->vK2;
			size_t s=mEdges[e].s;
			size_t t=mEdges[e].t;
			size_t l1s=labelling1[s];
			size_t l1t=labelling1[t];
			size_t l2s=labelling2[s];
			size_t l2t=labelling2[t];
			T C00=mE.core->data[l1s*vK2+l1t];
			T C01=mE.core->data[l1s*vK2+l2t];
			T C10=mE.core->data[l2s*vK2+l1t];
			T C11=mE.core->data[l2s*vK2+l2t];
			q.AddPairwiseTerm(s,t,C00,C01,C10,C11);
		}

		q.MergeParallelEdges();
		q.Solve();
		// q.ComputeWeakPersistencies();

		std::vector<size_t> labelling(m_nV,0);
		for(size_t i=0;i<m_nV;++i){
			size_t v=q.GetLabel(i);
			if(v==0)
			{
				labelling[i]=labelling1[i];
			}
			else
			{
				labelling[i]=labelling2[i];
			}	
		}	
		return labelling;
	}


	template<class vecType,class graphType>
	typename vecType::scalar spam<vecType,graphType>::primalGreedy()
	{
		using T=typename vecType::scalar;
		auto labelling=computeLabellingByGreedyRounding();
		T energy=computeEnergy(labelling);
		return energy;
	}

	template<class vecType,class graphType>
	typename vecType::scalar spam<vecType,graphType>::primal()	{
		using T=typename vecType::scalar;
		/*!	Fill up the labelling.	*/
		computeLabelling2();
		T energy=computeEnergy(mLabelling);
		return energy;
	}

	template<class vecType, class graphType>
	void spam<vecType, graphType>::computeLabelling2()	{
		
		using T=typename vecType::scalar;

		std::vector<bool> processedVertices(grPtr->m_nV,false);

		std::vector<size_t> unprocessedVertices;

		/*!	Fill up the unprocessed Vertices from 0 to |V|.	  */
		size_t i=0;
    	std::generate_n(std::inserter(unprocessedVertices, unprocessedVertices.begin()), grPtr->m_nV, [&i](){ return i++; });

		// std::cout << "unprocessedVertices: ";	printVectorBasic(unprocessedVertices);	std::cout << "\n";

		/*! Current vertex to process	*/
		size_t currentVtx;

		/*! Pick a vertex to start at from the set of unprocessed vertices	*/	
		currentVtx=pickRandomVertexPrimal2(unprocessedVertices);

		/*! Current vertex	*/
		// std::cout << "current vertex: " << currentVtx << "\n";

		/*! Num Processed Vertices	*/
		size_t numProcessedVertices=0;

		bool doneFlag=false;
		while(!doneFlag)		{

			std::vector<T> unaryVec;
			
			/*! copy unary vector for the currentVtx  into unaryVec	*/
			std::copy(mNodes[currentVtx].core->data, mNodes[currentVtx].core->data+(mNodes[currentVtx].K),std::back_inserter(unaryVec));

			const adjacencyVector& nbrList=grPtr->getNbrsToNode(currentVtx);
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					(nbrList[i].ndID<currentVtx) ? 
						messagePrimal(unaryVec,currentVtx,nbrList[i].eID,mLabelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVtx,nbrList[i].eID,mLabelling[nbrList[i].ndID]);
				}
			}

	
			
			/*!		Get the min element	*/
			mLabelling[currentVtx]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			// std::cout << "AFTER UNARY: ";	printVectorBasic(unaryVec);
			// std::cout << "Winning Label: " << mLabelling[currentVtx] << "\n";

			/*!	erase the element from unprocessed vertices */
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVtx));


			/*!	update the processed vertices	*/
			numProcessedVertices++;
			processedVertices[currentVtx]=true;

			// std::cout << "unprocessedVertices: ";	printVectorBasic(unprocessedVertices);

			size_t newVtx=sizetInf;
			/*! find the new vertex	*/
			if(numProcessedVertices==grPtr->m_nV)
			{
				doneFlag=true;
			}
			else
			{
				newVtx=pickRandomNeighbor2(currentVtx,processedVertices);

				/*! if no valid neighbor exists, pick a random vertex	*/
				if(newVtx==sizetInf)	{
					newVtx=pickRandomVertexPrimal2(unprocessedVertices);	
				}
			}

			/*!  update the currentVtx 	*/
			currentVtx=newVtx;
		}
	}

	template<class vecType, class graphType>
	std::vector<size_t> spam<vecType,graphType>::computeLabellingByGreedyRounding()
	{
		using T=typename vecType::scalar;
		
		std::vector<size_t> labelling(grPtr->m_nV,0);
		std::vector<bool> processedVertices(grPtr->m_nV,false);

		std::vector<size_t> unprocessedVertices;

		/*!	Fill up the unprocessed Vertices from 0 to |V|.	  */
		size_t i=0;
    	std::generate_n(std::inserter(unprocessedVertices, unprocessedVertices.begin()), grPtr->m_nV, [&i](){ return i++; });

		// std::cout << "unprocessedVertices: ";	printVectorBasic(unprocessedVertices);

		/*! Current vertex to process	*/
		size_t currentVtx;

		/*! Pick a vertex to start at from the set of unprocessed vertices	*/	
		currentVtx=pickRandomVertexPrimal2(unprocessedVertices);

		/*! Num Processed Vertices	*/
		size_t numProcessedVertices=0;

		bool doneFlag=false;
		while(!doneFlag)		{

			std::vector<T> unaryVec;
			
			/*! copy unary vector for the currentVtx  into unaryVec	*/
			std::copy(mNodes[currentVtx].core->data, mNodes[currentVtx].core->data+(mNodes[currentVtx].K),std::back_inserter(unaryVec));

			const adjacencyVector& nbrList=grPtr->getNbrsToNode(currentVtx);
			for(size_t i=0;i<nbrList.size();++i)	{

				if(processedVertices[nbrList[i].ndID])	{
					(nbrList[i].ndID<currentVtx) ? 
						messagePrimal(unaryVec,currentVtx,nbrList[i].eID,labelling[nbrList[i].ndID]) : 
						messageTPrimal(unaryVec,currentVtx,nbrList[i].eID,labelling[nbrList[i].ndID]);
				}
			}
			
			/*!		Get the min element	*/
			labelling[currentVtx]=(std::min_element(unaryVec.begin(),unaryVec.end())-unaryVec.begin());

			/*!	erase the element from unprocessed vertices */
			unprocessedVertices.erase(
					std::find(unprocessedVertices.begin(),
					unprocessedVertices.end(),currentVtx));


			/*!	update the processed vertices	*/
			numProcessedVertices++;
			processedVertices[currentVtx]=true;

			size_t newVtx=sizetInf;
			/*! find the new vertex	*/
			if(numProcessedVertices==grPtr->m_nV)
			{
				doneFlag=true;
			}
			else
			{
				newVtx=pickRandomNeighbor2(currentVtx,processedVertices);

				/*! if no valid neighbor exists, pick a random vertex	*/
				if(newVtx==sizetInf)	{
					newVtx=pickRandomVertexPrimal2(unprocessedVertices);	
				}
			}

			/*!  update the currentVtx 	*/
			currentVtx=newVtx;
		}
		return labelling;
	}

};

