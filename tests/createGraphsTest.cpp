#include <iostream>
#include <stdio.h>
#include "gtest/gtest.h"
#include "testGraphs.h"
#include "uGraph.h"

int main()
{
	srand(123432);
	Graph::graphStorage<float,Graph::uGraph> gu;

	size_t R=3;
	size_t C=3;
	size_t K=3;

	CreateGridGraph(R,C,K, true,gu);
	std::cout << gu << "\n";
	
	

	return 0;
}