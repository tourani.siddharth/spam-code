#include <iostream>
#include <stdio.h>
//#include "gtest/gtest.h"
#include "config.h"
#include "utils.h"
#include "uGraph.h"
#include "trws.h"
// #include "spam2.h"
#include "inputOutput.h"

template<class vecType>
void writeToUAIFile(std::string& filename,
	Graph::graphStorage& gu)
{
	std::ofstream fid(filename);
	size_t nV=gu.unaries.size();

	fid << "MARKOV\n";

	fid << nV << "\n";
	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << " ";
	}
	fid << "\n";

	int nF=nV+gu.pairwise.size();

	fid << nF << "\n";

	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << "\n";
		for(size_t j=0;j<gu.unaries[i].size();++j)
		{
			fid << gu.unaries[i][j] << " ";
		}
		fid << "\n";
	}

	for(size_t e=0;e<gu.pairwise.size();++e)
	{
		fid << gu.pwSz[e].l1*gu.pwSz[e].l2 << "\n";
		size_t l1=gu.pwSz[e].l1;
		size_t l2=gu.pwSz[e].l2;
		for(size_t j=0;j<gu.pwSz[e].l1;++j)
		{
			for(size_t k=0;k<gu.pwSz[e].l2;++k)
			{
				fid << gu.pairwise[e][j*l2+k] << " ";
			}
		}
		fid << "\n";
	}

	fid.close();
}

template<typename T>
void writeGraphToTXTFile(std::string filename, std::vector<std::vector<T>>& unaries, 
                        std::vector<std::vector<T>>& pairwise, 
                        std::vector<edgeEnds>& pws, std::vector<pwSize>& pwSz)
{
	std::ofstream fid(filename);
	fid << unaries.size() << " " << pws.size() << "\n";
	for(size_t i=0;i<unaries.size();++i){
		// fid << "UNARY[" << i << "]: ";
		fid << "1\n";
		fid << i << " " << unaries[i].size() << "\n";
		for(size_t l=0;l<unaries[i].size();++l)
		{
			fid << unaries[i][l] << " ";
		}
		fid << "\n";
	}
	fid << "\n";
	for(size_t e=0;e<pairwise.size();++e)
	{
		fid << "2\n";
		fid << pws[e].s << " " << pws[e].t << "\n";
		fid << pwSz[e].l1 << " " << pwSz[e].l2 << "\n";
		// fid << "EDGE " << pws[e].s << " - " << pws[e].t << "\n";
		size_t l1=pwSz[e].l1;
		size_t l2=pwSz[e].l2;
		for(size_t l=0;l<l1;++l)
		{
			for(size_t k=0;k<l2;++k)
			{
				fid << pairwise[e][l*l2+k] << " ";
			}
			fid << "\n\n";
		}
		fid << "\n";
	}
	fid.close();
}


void stereo(Solver::RoundingType rt)
{
    std::vector<std::string> filenames;
    std::string baseDir="/home/sid/Desktop/spam-code/txtFiles/mrf-stereo-txtFiles/";
    std::string tsuFile=baseDir+"tsu-gm.txt"; filenames.push_back(tsuFile);
    // std::string tedFile="/home/sid/Desktop/spam-code/txtFiles/mrf-stereo-txtFiles/ted-gm.txt"; filenames.push_back(tedFile);
    std::string venFile=baseDir+"ven-gm.txt"; filenames.push_back(venFile);
	

	Solver::trws<MsgPassing::vec1f>::options opts;
	opts.numIters=3000;
	opts.roundType=rt;
	
    for(size_t i=0;i<filenames.size();++i)
    {
        Graph::graphStorage gu;
        std::cout << filenames[i] << "\n";
        IO::readFile(filenames[i],gu);
	    Solver::trws<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }

}

void photomontage(Solver::RoundingType rt)
{
    std::vector<std::string> filenames;
    std::string baseDir="/home/sid/Desktop/spam-code/txtFiles/mrf-photomontage-txtFiles/";
    std::string famFile=baseDir+"family-gm.txt"; filenames.push_back(famFile);
    std::string panFile=baseDir+"pano-gm.txt"; filenames.push_back(panFile);
	

	Solver::trws<MsgPassing::vec1f>::options opts;
	opts.numIters=3000;
	opts.roundType=rt;
	
    for(size_t i=0;i<filenames.size();++i)
    {
        Graph::graphStorage gu;
        std::cout << filenames[i] << "\n";
        IO::readFile(filenames[i],gu);
	    Solver::trws<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }

}

void proteinFolding(Solver::RoundingType rt)
{
    std::string baseDir="/home/sid/Desktop/spam-code/txtFiles/protein-folding-txtFiles/";

    std::vector<std::string> filenames;
    std::string f01=baseDir+"1CKK.txt";  filenames.push_back(f01);
    std::string f02=baseDir+"1CM1.txt";  filenames.push_back(f02);
    std::string f03=baseDir+"1SY9.txt";  filenames.push_back(f03);
    std::string f04=baseDir+"2BBN.txt";  filenames.push_back(f04);
    std::string f05=baseDir+"2BCX.txt";  filenames.push_back(f05);
    std::string f06=baseDir+"2BE6.txt";  filenames.push_back(f06);
    std::string f07=baseDir+"2F3Y.txt";  filenames.push_back(f07);
    std::string f08=baseDir+"2FOT.txt";  filenames.push_back(f08);
    std::string f09=baseDir+"2HQW.txt";  filenames.push_back(f09);
    std::string f10=baseDir+"2O60.txt";  filenames.push_back(f10);
    std::string f11=baseDir+"3BXL.txt";  filenames.push_back(f11);
	

    for(size_t i=0;i<filenames.size();++i)
    {
        Graph::graphStorage gu;
        Solver::trws<MsgPassing::vec1f>::options opts;
        opts.numIters=300;
        opts.roundType=rt;

        std::string pfFile=filenames[i];
        
        IO::readFile(pfFile,gu);

		std::cout << pfFile << "\n";
	    Solver::trws<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }
	
    

}



int main(int argc, char** argv)
{
    // Solver::RoundingType rt=Solver::GREEDYBEST;
    Solver::RoundingType rt=Solver::FUSIONMOVES;
	// proteinFolding(rt);
    stereo(rt);

	return 0;
}