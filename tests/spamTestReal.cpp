#include <iostream>
#include <stdio.h>
//#include "gtest/gtest.h"
#include "config.h"
#include "utils.h"
#include "uGraph.h"
#include "spam2.h"
#include "testGraphs2.h"
#include "inputOutput.h"


template<class vecType>
void writeToUAIFile(std::string& filename,
	Graph::graphStorage& gu)
{
	std::ofstream fid(filename);
	size_t nV=gu.unaries.size();

	fid << "MARKOV\n";

	fid << nV << "\n";
	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << " ";
	}
	fid << "\n";

	int nF=nV+gu.pairwise.size();

	fid << nF << "\n";

	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << "\n";
		for(size_t j=0;j<gu.unaries[i].size();++j)
		{
			fid << gu.unaries[i][j] << " ";
		}
		fid << "\n";
	}

	for(size_t e=0;e<gu.pairwise.size();++e)
	{
		fid << gu.pwSz[e].l1*gu.pwSz[e].l2 << "\n";
		size_t l1=gu.pwSz[e].l1;
		size_t l2=gu.pwSz[e].l2;
		for(size_t j=0;j<gu.pwSz[e].l1;++j)
		{
			for(size_t k=0;k<gu.pwSz[e].l2;++k)
			{
				fid << gu.pairwise[e][j*l2+k] << " ";
			}
		}
		fid << "\n";
	}

	fid.close();
}

template<typename T>
void writeGraphToTXTFile(std::string filename, std::vector<std::vector<T>>& unaries, std::vector<std::vector<T>>& pairwise, std::vector<edgeEnds>& pws, std::vector<pwSize>& pwSz)
{
	std::ofstream fid(filename);
	fid << unaries.size() << " " << pws.size() << "\n";
	for(size_t i=0;i<unaries.size();++i){
		// fid << "UNARY[" << i << "]: ";
		fid << "1\n";
		fid << i << " " << unaries[i].size() << "\n";
		for(size_t l=0;l<unaries[i].size();++l)
		{
			fid << unaries[i][l] << " ";
		}
		fid << "\n";
	}
	fid << "\n";
	for(size_t e=0;e<pairwise.size();++e)
	{
		fid << "2\n";
		fid << pws[e].s << " " << pws[e].t << "\n";
		fid << pwSz[e].l1 << " " << pwSz[e].l2 << "\n";
		// fid << "EDGE " << pws[e].s << " - " << pws[e].t << "\n";
		size_t l1=pwSz[e].l1;
		size_t l2=pwSz[e].l2;
		for(size_t l=0;l<l1;++l)
		{
			for(size_t k=0;k<l2;++k)
			{
				fid << pairwise[e][l*l2+k] << " ";
			}
			fid << "\n\n";
		}
		fid << "\n";
	}
	fid.close();
}

void tsuTestNormalRounding(){
	
	std::string tsuFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-stereo/tsu-gm.txt";
	std::cout << "TSUKUBA\n";
	Graph::graphStorage gu;

	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=500;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::NORMAL;
	opts.nRows=288;
	opts.nCols=384;
	IO::readFile(tsuFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void tsuTestFusionMoves(){
	
	std::string tsuFile="/home/sid/Desktop/Greedy-Rounding/dataset/txtFiles/mrf-stereo-txtFiles/tsu-gm.txt";
	std::cout << "TSUKUBA\n";
	Graph::graphStorage gu;

	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=500;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::FUSIONMOVES;
	opts.nRows=288;
	opts.nCols=384;
	opts.ICM=true;
    opts.numICMIters=1;
	IO::readFile(tsuFile,gu);
	
	std::cout << "WITH ICM\n";
    for(size_t run=0;run<5;++run)
    {
		std::cout << "RUN " << run << "\n";
        opts.ICM=true;
	    Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }

	std::cout << "WITHOUT ICM\n";
    for(size_t run=0;run<5;++run)
    {
		std::cout  << "RUN " << run << "\n";
        opts.ICM=false;
	    Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }

}

void venTestNormalRounding(){
	
	std::string venFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-stereo/ven-gm.txt";
	std::cout << "VENUS\n";
	Graph::graphStorage gu;

	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=500;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::NORMAL;
	opts.nRows=383;
	opts.nCols=434;
	IO::readFile(venFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void venTestFusionMoves(){
	
	std::string venFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-stereo/ven-gm.txt";
	std::cout << "VENUS\n";
	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=500;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::FUSIONMOVES;
	opts.nRows=383;
	opts.nCols=434;
	IO::readFile(venFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void panoTestFusionMoves()
{
	std::string venFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-photomontage/pano-gm.txt";
	std::cout << "PANO FUSION\n";
	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=4000;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::FUSIONMOVES;
	opts.nRows=480;
	opts.nCols=1071;
	IO::readFile(venFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void panoTestNormalRounding()
{
	std::string venFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-photomontage/pano-gm.txt";
	std::cout << "PANO NORMAL\n";
	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=4000;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::NORMAL;
	opts.nRows=480;
	opts.nCols=1071;
	IO::readFile(venFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void familyTestFusionMoves()
{
	std::string venFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-photomontage/family-gm.txt";
	std::cout << "FAMILY FUSION\n";
	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=4000;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::FUSIONMOVES;
	opts.nRows=434;
	opts.nCols=383;
	IO::readFile(venFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void familyTestNormalRounding()
{
	std::string famFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-photomontage/family-gm.txt";
	std::cout << "FAMILY NORMAL\n";

	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=4000;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::GRID;
	opts.roundType=Solver::NORMAL;
	opts.nRows=566;
	opts.nCols=752;
	IO::readFile(famFile,gu);
	
	Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	algo.infer();
}

void proteinFoldingNormalRounding()
{
    std::string baseDir="/home/sid/Desktop/spam-code/txtFiles/protein-folding-txtFiles/";

    std::vector<std::string> filenames;
    std::string f01=baseDir+"1CKK.txt";  filenames.push_back(f01);
    std::string f02=baseDir+"1CM1.txt";  filenames.push_back(f02);
    std::string f03=baseDir+"1SY9.txt";  filenames.push_back(f03);
    std::string f04=baseDir+"2BBN.txt";  filenames.push_back(f04);
    std::string f05=baseDir+"2BCX.txt";  filenames.push_back(f05);
    std::string f06=baseDir+"2BE6.txt";  filenames.push_back(f06);
    std::string f07=baseDir+"2F3Y.txt";  filenames.push_back(f07);
    std::string f08=baseDir+"2FOT.txt";  filenames.push_back(f08);
    std::string f09=baseDir+"2HQW.txt";  filenames.push_back(f09);
    std::string f10=baseDir+"2O60.txt";  filenames.push_back(f10);
    std::string f11=baseDir+"3BXL.txt";  filenames.push_back(f11);
	
    size_t i=0;
    for(size_t i=0;i<filenames.size();++i)
    {
        Graph::graphStorage gu;
        Solver::spam<MsgPassing::vec1f>::options opts;
        opts.numIters=300;
        opts.sampling=Solver::SHORTEST_PATH;
        opts.grType=Solver::COMPLETE;
        opts.roundType=Solver::FUSIONMOVES;

        std::string pfFile=filenames[i];
        IO::readFile(pfFile,gu);

        std::cout << pfFile << "\n";
	    Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	    algo.infer();
    }
	
    

}

namespace fs=std::filesystem;


int main(int argc, char** argv)
{
	Graph::graphStorage gu;
	Solver::spam<MsgPassing::vec1f>::options opts;
	opts.numIters=300;
	opts.sampling=Solver::SHORTEST_PATH;
	opts.grType=Solver::COMPLETE;
	opts.roundType=Solver::FUSIONMOVES;


	std::string dirPath="/media/sid/new-data/pose_files/";

    for (const auto & entry : fs::directory_iterator(dirPath))
	{
		std::cout << entry.path() << std::endl;
		IO::readFile(entry.path(),gu);
		getchar();
	}
        


	// std::string pfFile="/home/sid/Desktop/Greedy-Rounding/opengm3/build/src/examples/unsorted-examples/patch_tsukuba_pt:(180,160)_sz:(20,30).txt";
	

	// std::cout << "numUnaries: " << gu.unaries.size() << " numPW: " << gu.pairwise.size() << "\n";
	// std::cout << "numVertices: " << gu.G.nV() << " numEdges: " << gu.G.nE() << "\n";

	// std::cout << pfFile << "\n";
	// Solver::spam<MsgPassing::vec1f> algo(gu,opts);
	// algo.infer();


	// // srand(14235);
	// size_t randNum=21435;
	// srand(randNum);
	// // srand(time(NULL));
	// using uGraph=Graph::uGraph;
	// Graph::graphStorage gu;
	// int R=400;	int C=R;	int K=3;
	// // Graph::createCompleteGraph(30,K);
	// Graph::createGridGraph(R,C,K, true,gu);

	// Solver::spam<MsgPassing::vec1f>::options opts;
	// opts.numIters=100;
	// opts.sampling=Solver::SHORTEST_PATH;
	// opts.grType=Solver::GRID;
	// opts.roundType=Solver::FUSIONMOVES;
	// // opts.roundType=Solver::NORMAL;
	// opts.nRows=R;
	// opts.nCols=C;


	// std::string tsuFile="/home/sid/Desktop/Greedy-Rounding/dataset/mrf-stereo/ven-gm.txt";
	// std::cout << "TSUKUBA\n";
	// Solver::spam<MsgPassing::vec1f>::options opts;
	// opts.numIters=2;
	// opts.sampling=Solver::SHORTEST_PATH;
	// opts.grType=Solver::GRID;
	// opts.roundType=Solver::FUSIONMOVES;
	// opts.nRows=383;
	// opts.nCols=434;
	// IO::readFile(tsuFile,gu);


	
	
	// Solver::spam<MsgPassing::vec1f> algo(gu,opts);

    // algo.primal();
    // algo.icm();
    // return 0;
}
