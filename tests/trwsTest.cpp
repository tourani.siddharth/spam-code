#include <iostream>
#include <stdio.h>
//#include "gtest/gtest.h"
#include "config.h"
#include "utils.h"
#include "uGraph.h"
#include "trws.h"
// #include "spam2.h"
#include "inputOutput.h"

template<class vecType>
void writeToUAIFile(std::string& filename,
	Graph::graphStorage& gu)
{
	std::ofstream fid(filename);
	size_t nV=gu.unaries.size();

	fid << "MARKOV\n";

	fid << nV << "\n";
	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << " ";
	}
	fid << "\n";

	int nF=nV+gu.pairwise.size();

	fid << nF << "\n";

	for(size_t i=0;i<gu.unaries.size();++i)
	{
		fid << gu.unaries[i].size() << "\n";
		for(size_t j=0;j<gu.unaries[i].size();++j)
		{
			fid << gu.unaries[i][j] << " ";
		}
		fid << "\n";
	}

	for(size_t e=0;e<gu.pairwise.size();++e)
	{
		fid << gu.pwSz[e].l1*gu.pwSz[e].l2 << "\n";
		size_t l1=gu.pwSz[e].l1;
		size_t l2=gu.pwSz[e].l2;
		for(size_t j=0;j<gu.pwSz[e].l1;++j)
		{
			for(size_t k=0;k<gu.pwSz[e].l2;++k)
			{
				fid << gu.pairwise[e][j*l2+k] << " ";
			}
		}
		fid << "\n";
	}

	fid.close();
}

template<typename T>
void writeGraphToTXTFile(std::string filename, std::vector<std::vector<T>>& unaries, 
                        std::vector<std::vector<T>>& pairwise, 
                        std::vector<edgeEnds>& pws, std::vector<pwSize>& pwSz)
{
	std::ofstream fid(filename);
	fid << unaries.size() << " " << pws.size() << "\n";
	for(size_t i=0;i<unaries.size();++i){
		// fid << "UNARY[" << i << "]: ";
		fid << "1\n";
		fid << i << " " << unaries[i].size() << "\n";
		for(size_t l=0;l<unaries[i].size();++l)
		{
			fid << unaries[i][l] << " ";
		}
		fid << "\n";
	}
	fid << "\n";
	for(size_t e=0;e<pairwise.size();++e)
	{
		fid << "2\n";
		fid << pws[e].s << " " << pws[e].t << "\n";
		fid << pwSz[e].l1 << " " << pwSz[e].l2 << "\n";
		// fid << "EDGE " << pws[e].s << " - " << pws[e].t << "\n";
		size_t l1=pwSz[e].l1;
		size_t l2=pwSz[e].l2;
		for(size_t l=0;l<l1;++l)
		{
			for(size_t k=0;k<l2;++k)
			{
				fid << pairwise[e][l*l2+k] << " ";
			}
			fid << "\n\n";
		}
		fid << "\n";
	}
	fid.close();
}



int main(int argc, char** argv)
{
	// srand(14235);
	size_t randNum=214335;
	srand(randNum);
	// srand(time(NULL));
	using uGraph=Graph::uGraph;
	Graph::graphStorage gu;
	int R=30;	int C=R;	int K=3;
	// Graph::createCompleteGraph(30,K);
    
	Graph::createGridGraph(R,C,K, true,gu);

    // writeGraphToTXTFile(filename,gu.unaries,gu.pairwise,gu.G.m_E,gu.pwSz);


	Solver::trws<MsgPassing::vec1f>::options opts;
	opts.numIters=100;
	opts.roundType=Solver::GREEDYBEST;
	Solver::trws<MsgPassing::vec1f> trws(gu,opts);
	trws.infer();

    // Solver::spam<MsgPassing::vec1f>::options opts;
	// opts.numIters=100;
	// opts.sampling=Solver::SHORTEST_PATH;
	// opts.grType=Solver::GRID;
	// opts.nRows=R;
	// opts.nCols=C;
	// opts.roundType=Solver::FUSIONMOVES;
	// // opts.roundType=Solver::NORMAL;
	
	// // std::cout << gu << "\n";
	// Solver::spam<MsgPassing::vec1f> spam(gu,opts);
    // spam.infer();


	// Solver::trws<MsgPassing::vec1f> algo(gu,opts);
	// algo.infer();


	return 0;
}